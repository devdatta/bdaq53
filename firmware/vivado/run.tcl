
# ---------------------------------------------------------------
# Copyright (c) SILAB ,  Institute of Physics, University of Bonn
# ---------------------------------------------------------------
#
#   This script creates Vivado projects and bitfiles for the supported hardware platforms.
#
#   Start vivado in tcl mode by executing:
#       vivado -mode tcl -source run.tcl
#
#   NOTE: This will build firmware versions for every supported hardware. See the section "Create projects and bitfiles" below.
#   Alternatively, a list of 6 arguments can be used to build only the specified firmware.
#   The arguments have to be in the correct order. Just copy&paste from the "Create projects and bitfiles" section and remove all but one space in between the args.
#       vivado -mode tcl -source run.tcl -tclargs xc7k160tffg676-2 BDAQ53 "" ../src/bdaq53.xdc 64 _RX640
#


set basil_dir [exec python -c "import basil, os; print(str(os.path.dirname(os.path.dirname(basil.__file__))))"]
set include_dirs [list $basil_dir/basil/firmware/modules $basil_dir/basil/firmware/modules/utils]

file mkdir output reports


proc read_design_files {} {
    read_verilog ../src/bdaq53.v
    read_verilog ../src/bdaq53_core.v

    read_edif ../SiTCP/SiTCP_XC7K_32K_BBT_V110.ngc
    read_verilog ../SiTCP/TIMER.v
    read_verilog ../SiTCP/SiTCP_XC7K_32K_BBT_V110.V
    read_verilog ../SiTCP/WRAP_SiTCP_GMII_XC7K_32K.V
}


proc run_bit { part board connector xdc_file size option} {
    create_project -force -part $part $board$option$connector designs

    read_design_files
    read_xdc $xdc_file

    #read_ip ../src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane.xci
    global include_dirs

    synth_design -top bdaq53 -include_dirs $include_dirs -verilog_define "$board=1" -verilog_define "$connector=1" -verilog_define "SYNTHESIS=1" -verilog_define "$option=1"
    opt_design
    place_design
    phys_opt_design
    route_design
    report_utilization
    report_timing -file "reports/report_timing.$board$option$connector.log"
    write_bitstream -force -file output/$board$option$connector
    write_cfgmem -format mcs -size $size -interface SPIx4 -loadbit "up 0x0 output/$board$option$connector.bit" -force -file output/$board$option$connector
    close_project

    exec tar -C ./output -cvzf output/$board$option$connector.tar.gz $board$option$connector.bit $board$option$connector.mcs
}


#########

#
# Create projects and bitfiles
#

if {$argc == 0} {
# By default, all firmware versions are generated. You can comment the ones you don't need.

# Bitfiles for the 640 Mb/s Aurora ip core configuration
#       FPGA type           board name  connector   constraints file     flash size  option
run_bit xc7k160tffg676-2    BDAQ53      ""          ../src/bdaq53.xdc       64      _RX640
run_bit xc7k160tfbg676-1    USBPIX3     ""          ../src/usbpix3.xdc      64      _RX640
run_bit xc7k325tffg900-2    KC705       _SMA        ../src/kc705_gmii.xdc   16      _RX640
run_bit xc7k325tffg900-2    KC705       _FMC_LPC    ../src/kc705_gmii.xdc   16      _RX640

# Bitfiles for the 1.28 Gb/s Aurora ip core configuration
#       FPGA type           board name	connector  	constraints file     flash size  option
run_bit xc7k160tffg676-2    BDAQ53      ""          ../src/bdaq53.xdc       64        ""
run_bit xc7k160tfbg676-1    USBPIX3     ""          ../src/usbpix3.xdc      64        ""
run_bit xc7k325tffg900-2    KC705       _SMA        ../src/kc705_gmii.xdc   16        ""
run_bit xc7k325tffg900-2    KC705       _FMC_LPC    ../src/kc705_gmii.xdc   16        ""

# In order to build only one specific firmware version, the tun.tcl can be executed with arguments
} else {
    if {$argc == 6} {
        run_bit {*}$argv
    } else {
        puts "ERROR: Invalid args"
    }
}
exit
