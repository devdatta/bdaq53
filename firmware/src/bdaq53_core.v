/**
 * ------------------------------------------------------------
 * Copyright (c) SILAB , Physics Institute of Bonn University
 * ------------------------------------------------------------
 */

`timescale 1ns / 1ps
`default_nettype wire

`include "cmd_rd53/cmd_rd53.v"
`include "cmd_rd53/cmd_rd53_core.v"

`ifdef AURORA_1LANE
    `include "rx_aurora/rx_aurora_64b66b_1lane/rx_aurora_64b66b.v"
    `include "rx_aurora/rx_aurora_64b66b_1lane/rx_aurora_64b66b_core.v"
`elsif AURORA_2LANE
    `include "rx_aurora/rx_aurora_64b66b_2lanes/rx_aurora_64b66b.v"
    `include "rx_aurora/rx_aurora_64b66b_2lanes/rx_aurora_64b66b_core.v"
`elsif AURORA_4LANE
    `include "rx_aurora/rx_aurora_64b66b_4lanes/rx_aurora_64b66b.v"
    `include "rx_aurora/rx_aurora_64b66b_4lanes/rx_aurora_64b66b_core.v"
`else
    `include "rx_aurora/rx_aurora_64b66b_1lane/rx_aurora_64b66b.v"
    `include "rx_aurora/rx_aurora_64b66b_1lane/rx_aurora_64b66b_core.v"
`endif


// include basil modules
`include "i2c/i2c.v"
`include "i2c/i2c_core.v"
`include "gpio/gpio.v"
`include "utils/cdc_pulse_sync.v"
`include "utils/cdc_reset_sync.v"
`include "utils/cdc_syncfifo.v"
`include "utils/generic_fifo.v"
`include "utils/pulse_gen_rising.v"
`include "utils/rgmii_io.v"
`include "utils/rbcp_to_bus.v"
`include "utils/fifo_32_to_8.v"
`include "utils/bus_to_ip.v"
`include "utils/ddr_des.v"
`include "rrp_arbiter/rrp_arbiter.v"
`include "includes/log2func.v"
`include "spi/spi.v"
`include "spi/spi_core.v"
`include "spi/blk_mem_gen_8_to_1_2k.v"
`include "utils/CG_MOD_pos.v"
`include "utils/flag_domain_crossing.v"
`include "utils/3_stage_synchronizer.v"

`include "tlu/tlu_controller.v"
`include "tlu/tlu_controller_core.v"
`include "tlu/tlu_controller_fsm.v"

`include "pulse_gen/pulse_gen.v"
`include "pulse_gen/pulse_gen_core.v"

`include "tdc_s3/tdc_s3.v"
`include "tdc_s3/tdc_s3_core.v"


module bdaq53_core(
    input wire          BUS_CLK,
    input wire          BUS_RST,
    input wire  [31:0]  BUS_ADD,
    inout wire  [31:0]  BUS_DATA,
    input wire          BUS_RD,
    input wire          BUS_WR,
    output wire         BUS_BYTE_ACCESS,

    // Clocks from oscillators and mux select
    input wire  CLK200_P, CLK200_N,
    input wire  RX_CLK_IN_P, RX_CLK_IN_N,
    output wire CLK_SEL,
    output wire REFCLK1_OUT,
    output wire TX_OUT_CLK,
    output wire AURORA_CLK_OUT,

    // PLL
    input wire  CLK_CMD,

    // Aurora lanes
    input wire [3:0] MGT_RX_P, MGT_RX_N,
    output wire TX_P, TX_N,

    // CMD encoder
    input wire EXT_TRIGGER,
    output wire CMD_DATA, CMD_OUT, CMD_WRITING, CMD_OUTPUT_EN,
    output wire BYPASS_MODE,
    output wire CMD_LOOP_START_PULSE,

    // 4 individual HitOr lines from the different pixel locations
    input wire [3:0] HITOR,

    // Displayport control signals
    output wire [3:0] GPIO_RESET,
    input wire [3:0]  GPIO_SENSE,

    // Debug signals
    output wire RX_LANE_UP,
    output wire RX_CHANNEL_UP,
    output wire PLL_LOCKED,
    output wire DEBUG_TX0, DEBUG_TX1,

    // I2C bus
    inout wire I2C_SDA,
    inout wire I2C_SCL,

    // FIFO cpontrol signals (TX FIFO of DAQ)
    output wire [31:0] FIFO_DATA,
    output wire FIFO_WRITE,
    input wire FIFO_EMPTY,
    input wire FIFO_FULL,

    output wire BX_CLK_EN,
    output wire DUT_RESET,
    output wire RESET_TB,
    input wire AURORA_RESET,
    output wire RX_SOFT_ERROR, RX_HARD_ERROR,
    output wire [3:0] PMOD,

    // TLU
    input wire RJ45_TRIGGER, RJ45_RESET,
    output wire RJ45_BUSY,
    output wire RJ45_CLK,

    // TRIGGER
    input wire LEMO_TRIGGER,
    input wire MULTI_PURPOSE,

    // TDC
    input wire CLK320,
    input wire CLK160,
    input wire CLK40

/*
    // DDR3 Memory Interface
    output wire [14:0]ddr3_addr,
    output wire [2:0] ddr3_ba,
    output wire       ddr3_cas_n,
    output wire       ddr3_ras_n,
    output wire       ddr3_ck_n, ddr3_ck_p,
    output wire [0:0] ddr3_cke,
    output wire       ddr3_reset_n,
    inout  wire [7:0] ddr3_dq,
    inout  wire       ddr3_dqs_n, ddr3_dqs_p,
    output wire [0:0] ddr3_dm,
    output wire       ddr3_we_n,
    output wire [0:0] ddr3_cs_n,
    output wire [0:0] ddr3_odt,
*/
);


// FIRMWARE VERSION
parameter VERSION_MAJOR = 8'd0;
parameter VERSION_MINOR = 8'd11;


// BOARD ID
localparam CON_SMA = 16'd0;
localparam CON_FMC_LPC = 16'd1;
localparam CON_FMC_HPC = 16'd2;
localparam CON_DP = 16'd3;

localparam SIM = 16'd0;
localparam BDAQ53 = 16'd1;
localparam USBPIX3 = 16'd2;
localparam KC705 = 16'd3;

`ifdef RTL_SIM
    localparam BOARD = SIM;
    localparam CONNECTOR = CON_SMA;
`elsif BDAQ53
    localparam BOARD = BDAQ53;
    localparam CONNECTOR = CON_DP;
`elsif USBPIX3
    localparam BOARD = USBPIX3;
    localparam CONNECTOR = CON_SMA;
`elsif KC705
    localparam BOARD = KC705;
    `ifdef _FMC_LPC
        localparam CONNECTOR = CON_FMC_LPC;
    `elsif _SMA
        localparam CONNECTOR = CON_SMA;
    `endif
`endif

// BOARD OPTIONS
// bit0: 640Mb/s RX, bit1: ...
`ifdef _RX640
    localparam AURORA_RX_640M = 1;
`else
    localparam AURORA_RX_640M = 0;
`endif

// VERSION/BOARD READBACK
reg [7:0] BUS_DATA_OUT_REG;
always @ (posedge BUS_CLK) begin
    if(BUS_RD) begin
        if(BUS_ADD == 0)
            BUS_DATA_OUT_REG <= VERSION_MINOR;
        else if(BUS_ADD == 1)
            BUS_DATA_OUT_REG <= VERSION_MAJOR;
        else if(BUS_ADD == 2)
            BUS_DATA_OUT_REG <= BOARD[7:0];
        else if(BUS_ADD == 3)
            BUS_DATA_OUT_REG <= BOARD[15:8];
        else if(BUS_ADD == 4)
            BUS_DATA_OUT_REG <= {7'b0000000, AURORA_RX_640M};
        else if(BUS_ADD == 5)
            BUS_DATA_OUT_REG <= CONNECTOR[7:0];
        else if(BUS_ADD == 6)
            BUS_DATA_OUT_REG <= CONNECTOR[15:8];
    end
end

reg READ_VER;
always @ (posedge BUS_CLK)
    if(BUS_RD & BUS_ADD < 7)
        READ_VER <= 1;
    else
        READ_VER <= 0;

assign BUS_DATA[7:0] = READ_VER ? BUS_DATA_OUT_REG : 8'hzz;


// CLOCKS
wire INIT_CLK_IN_P, INIT_CLK_IN_N;  // 50...200 MHz
assign INIT_CLK_IN_P = CLK200_P;
assign INIT_CLK_IN_N = CLK200_N;


// -------  MODULE ADREESSES  ------- //
localparam TLU_BASEADDR = 16'h0600;
localparam TLU_HIGHADDR = 16'h0700-1;

localparam PULSER_TRIG_BASEADDR = 16'h0700;
localparam PULSER_TRIG_HIGHADDR = 16'h0800-1;

localparam PULSER_VETO_BASEADDR = 16'h0800;
localparam PULSER_VETO_HIGHADDR = 16'h0900-1;

localparam I2C_BASEADDR  = 32'h1000;
localparam I2C_HIGHADDR  = 32'h2000-1;

localparam GPIO_BASEADDR = 32'h2000;
localparam GPIO_HIGHADDR = 32'h2100-1;

localparam GPIO_RESET_SENSE_BASEADDR = 32'h2100;
localparam GPIO_RESET_SENSE_HIGHADDR = 32'h2200-1;

localparam SPI_BASEADDR = 32'h2200;
localparam SPI_HIGHADDR = 32'h2300-1;

localparam PULSE_AZ_BASEADDR = 32'h2300;
localparam PULSE_AZ_HIGHADDR = 32'h2400-1;

localparam TDC_BASEADDR = 32'h2400;
localparam TDC_HIGHADDR = 32'h2500-1;

localparam PULSE_CMD_START_LOOP_BASEADDR = 32'h2800;
localparam PULSE_CMD_START_LOOP_HIGHADDR = 32'h2900-1;

localparam AURORA_RX_BASEADDR = 32'h6000;
localparam AURORA_RX_HIGHADDR = 32'h7000-1;

localparam CMD_RD53_BASEADDR = 32'h9000;
localparam CMD_RD53_HIGHADDR = 32'hb000-1;


// -------  USER MODULES  ------- //
wire [7:0] GPIO_IO;
gpio #(
    .BASEADDR(GPIO_BASEADDR),
    .HIGHADDR(GPIO_HIGHADDR),
    .ABUSWIDTH(32),
    .IO_WIDTH(8),
    .IO_DIRECTION(8'hff)
) i_gpio_rx (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .IO(GPIO_IO)
);

assign DUT_RESET = GPIO_IO[0];
assign BX_CLK_EN = GPIO_IO[2];
assign RESET_TB  = GPIO_IO[6];


// SPI module to acces external ADC and DAC with clock divider
wire SPI_CLK;
clock_divider #(
    .DIVISOR(4)
) i_clock_divisor_spi (
    .CLK(BUS_CLK),
    .RESET(1'b0),
    .CE(),
    .CLOCK(SPI_CLK)
);

wire SEN_inverted;
assign PMOD[3] = ~SEN_inverted;
spi #(
    .BASEADDR(SPI_BASEADDR),
    .HIGHADDR(SPI_HIGHADDR),
    .ABUSWIDTH(16),
    .MEM_BYTES(2)
) i_spi (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .SPI_CLK(SPI_CLK),

    .SCLK(PMOD[0]),
    .SDO(),
    .SDI(PMOD[2]),
    .EXT_START(),

    .SEN(SEN_inverted),
    .SLD()
);

// GPIO module to access the chip reset and power sense signals
gpio #(
    .BASEADDR(GPIO_RESET_SENSE_BASEADDR),
    .HIGHADDR(GPIO_RESET_SENSE_HIGHADDR),
    .ABUSWIDTH(32),
    .IO_WIDTH(8),
    .IO_DIRECTION(8'hf0)
) i_gpio_control (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .IO({GPIO_RESET, GPIO_SENSE})
);

// ------ I²C module with clock generator ------ //
(* KEEP = "{TRUE}" *)
wire I2C_CLK;

clock_divider #(
    .DIVISOR(1600)
) i_clock_divisor_i2c (
    .CLK(BUS_CLK),
    .RESET(1'b0),
    .CE(),
    .CLOCK(I2C_CLK)
);


localparam I2C_MEM_BYTES = 32;

i2c #(
    .BASEADDR(I2C_BASEADDR),
    .HIGHADDR(I2C_HIGHADDR),
    .ABUSWIDTH(32),
    .MEM_BYTES(I2C_MEM_BYTES)
)  i_i2c (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .I2C_CLK(I2C_CLK),
    .I2C_SDA(I2C_SDA),
    .I2C_SCL(I2C_SCL)
);


// ----- Pulser for trigger command----- //
wire EXT_TRIGGER_PULSE;
wire EXT_START_VETO;
wire EXT_START_PULSE_TRIG;

pulse_gen #(
    .BASEADDR(PULSER_TRIG_BASEADDR),
    .HIGHADDR(PULSER_TRIG_HIGHADDR),
    .ABUSWIDTH(32)
) i_pulse_gen_trig (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .PULSE_CLK(CLK_CMD),
    .EXT_START(EXT_START_PULSE_TRIG  & ~EXT_START_VETO),   // don't send triggers during the az phase (for sync fe)
    .PULSE(EXT_TRIGGER_PULSE)
);


// ----- Auto-zeroing pulse generator ----- //
wire AZ_PULSE;
pulse_gen
#(
    .BASEADDR(PULSE_AZ_BASEADDR),
    .HIGHADDR(PULSE_AZ_HIGHADDR),
    .ABUSWIDTH(32)
    ) i_pulse_gen_az
(
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .PULSE_CLK(BUS_CLK),
    .EXT_START(1'b0),
    .PULSE(AZ_PULSE)
    );


// ----- Command encoder ----- //
wire EXT_START_PIN;
wire CMD_EXT_START_ENABLED;
wire AZ_VETO_FLAG, AZ_VETO_TLU_PULSE;
assign EXT_START_VETO = AZ_VETO_FLAG;   // don't send triggers during the az phase (for sync fe)
wire CMD_LOOP_START;

cmd_rd53 #(
    .BASEADDR(CMD_RD53_BASEADDR),
    .HIGHADDR(CMD_RD53_HIGHADDR),
    .ABUSWIDTH(32)
) i_cmd_rd53 (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .EXT_START_PIN(EXT_START_PIN),
    .EXT_START_ENABLED(CMD_EXT_START_ENABLED),
    .EXT_TRIGGER(EXT_TRIGGER_PULSE), // length of EXT_TRIGGER_PULSE determines how many frames will be read out

    .AZ_PULSE(AZ_PULSE),
    .AZ_VETO_TLU_PULSE(AZ_VETO_TLU_PULSE),
    .AZ_VETO_FLAG(AZ_VETO_FLAG),

    .CMD_WRITING(CMD_WRITING),
    .CMD_LOOP_START(CMD_LOOP_START),
    .CMD_CLK(CLK_CMD),
    .CMD_OUTPUT_EN(CMD_OUTPUT_EN),
    .CMD_SERIAL_OUT(CMD_DATA),
    .CMD_OUT(CMD_OUT),

    .BYPASS_MODE(BYPASS_MODE)
);


// ----- Arbiter ----- //
wire TLU_FIFO_EMPTY;
wire AURORA_FIFO_EMPTY;
wire TLU_FIFO_PEEMPT_REQ;
wire [3:0] TDC_FIFO_EMPTY;
wire [31:0] AURORA_FIFO_DATA;
wire [31:0] TLU_FIFO_DATA;
wire [31:0] TDC_FIFO_DATA [3:0];
wire ARB_READY_OUT, ARB_WRITE_OUT;
wire ARB_GRANT_AURORA_RX, ARB_GRANT_TLU_FIFO;
wire [3:0] ARB_GRANT_TDC_FIFO;
wire [31:0] ARB_DATA_OUT;


// ----- CMD_START_LOOP -> TDC pulse generator ----- //
pulse_gen
#(
    .BASEADDR(PULSE_CMD_START_LOOP_BASEADDR),
    .HIGHADDR(PULSE_CMD_START_LOOP_HIGHADDR),
    .ABUSWIDTH(32)
    ) i_pulse_gen_cmd_start_loop
(
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .PULSE_CLK(CLK_CMD),
    .EXT_START(CMD_LOOP_START),
    .PULSE(CMD_LOOP_START_PULSE)
    );


rrp_arbiter
#(
    .WIDTH(6)
) i_rrp_arbiter
(
    .RST(BUS_RST),
    .CLK(BUS_CLK),
    .WRITE_REQ({!TDC_FIFO_EMPTY[3], !TDC_FIFO_EMPTY[2], !TDC_FIFO_EMPTY[1], !TDC_FIFO_EMPTY[0], !AURORA_FIFO_EMPTY, !TLU_FIFO_EMPTY}),      // indicate will of writing data
    .HOLD_REQ({5'b0, TLU_FIFO_PEEMPT_REQ}),                    // wait for writing for given stream (priority)
    .DATA_IN({TDC_FIFO_DATA[3], TDC_FIFO_DATA[2], TDC_FIFO_DATA[1], TDC_FIFO_DATA[0], AURORA_FIFO_DATA, TLU_FIFO_DATA}),            // incoming data for arbitration
    .READ_GRANT({ARB_GRANT_TDC_FIFO, ARB_GRANT_AURORA_RX, ARB_GRANT_TLU_FIFO}),    // indicate to stream that data has been accepted

    .READY_OUT(ARB_READY_OUT),              // indicates ready for outgoing stream (input)
    .WRITE_OUT(ARB_WRITE_OUT),              // indicates will of write to outgoing stream
    .DATA_OUT(ARB_DATA_OUT)                 // outgoing data stream
);

assign ARB_READY_OUT = !FIFO_FULL;


// ----- TLU module ----- //
wire TLU_FIFO_READ;
wire TRIGGER_ACKNOWLEDGE_FLAG; // to TLU FSM
wire TRIGGER_ACCEPTED_FLAG;
wire [31:0] TIMESTAMP;
wire TLU_BUSY, TLU_CLOCK;

wire [3:0] LEMO_TRIGGER_FROM_TDC;
wire [3:0] TDC_IN_FROM_TDC;

// Combine all four HITOR lines into one line
wire HITOR_COMBINED;
assign HITOR_COMBINED = | HITOR;

tlu_controller #(
    .BASEADDR(TLU_BASEADDR),
    .HIGHADDR(TLU_HIGHADDR),
    .DIVISOR(32),
    .ABUSWIDTH(32),
    .TLU_TRIGGER_MAX_CLOCK_CYCLES(32)
) i_tlu_controller (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .TRIGGER_CLK(CLK_CMD),  // TLU module operates with 160 MHz command clock

    .FIFO_READ(TLU_FIFO_READ),
    .FIFO_EMPTY(TLU_FIFO_EMPTY),
    .FIFO_DATA(TLU_FIFO_DATA),

    .FIFO_PREEMPT_REQ(TLU_FIFO_PEEMPT_REQ),

    .TRIGGER({1'b0, TDC_IN_FROM_TDC, MULTI_PURPOSE, LEMO_TRIGGER_FROM_TDC[0], HITOR_COMBINED}),  // HITOR (TDC loop-through), RX1, RX0 (TDC loop-through), HITOR
    .TRIGGER_VETO({6'b0, AZ_VETO_FLAG, FIFO_FULL}),

    .TRIGGER_ACKNOWLEDGE(TRIGGER_ACKNOWLEDGE_FLAG),
    .TRIGGER_ACCEPTED_FLAG(TRIGGER_ACCEPTED_FLAG),
    .EXT_TRIGGER_ENABLE(CMD_EXT_START_ENABLED),

    .TLU_TRIGGER(RJ45_TRIGGER),
    .TLU_RESET(RJ45_RESET),
    .TLU_BUSY(TLU_BUSY),
    .TLU_CLOCK(TLU_CLOCK),

    .TIMESTAMP(TIMESTAMP)
);

assign RJ45_BUSY = TLU_BUSY;
assign RJ45_CLK = TLU_CLOCK;

// ----- Pulser for TLU veto----- //
wire EXT_START_PULSE_VETO;
assign EXT_START_PULSE_VETO = TRIGGER_ACCEPTED_FLAG;
assign EXT_START_PULSE_TRIG = TRIGGER_ACCEPTED_FLAG;
wire VETO_TLU_PULSE;
assign AZ_VETO_TLU_PULSE = VETO_TLU_PULSE;

// set acknowledge when veto returns to low
pulse_gen_rising i_pulse_gen_rising_tlu_veto(.clk_in(CLK_CMD), .in(~VETO_TLU_PULSE), .out(TRIGGER_ACKNOWLEDGE_FLAG));

pulse_gen #(
    .BASEADDR(PULSER_VETO_BASEADDR),
    .HIGHADDR(PULSER_VETO_HIGHADDR),
    .ABUSWIDTH(32)
) i_pulse_gen_veto (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .PULSE_CLK(CLK_CMD),
    .EXT_START(EXT_START_PULSE_VETO),
    .PULSE(VETO_TLU_PULSE)
);


// ----- TDC module (individual for all four HitOr lines)----- //
localparam CLKDV = 4;  // division factor from 160 MHz clock to DC_CLK (here 40 MHz)
wire [3:0] TDC_FIFO_READ;
// Do not generate TDC module for KC705.
`ifndef KC705
    wire [CLKDV*4-1:0] FAST_TRIGGER_OUT;
    wire [1:0] TRIG_FAST_OUT;
    // First TDC module: creates fast sampled trigger signal to use it for other TDC modules.
    tdc_s3 #(
        .BASEADDR(TDC_BASEADDR),
        .HIGHADDR(TDC_HIGHADDR),
        .ABUSWIDTH(32),
        .CLKDV(CLKDV),
        .DATA_IDENTIFIER(4'b0001),
        .FAST_TDC(1),
        .FAST_TRIGGER(1),
        .BROADCAST(0)  // generate for first TDC module the 640MHz sampled trigger signal and share it with other TDC mddules (broadcast)
    ) i_tdc (
        .CLK320(CLK320),  // 320 MHz
        .CLK160(CLK160),  // 160 MHz
        .DV_CLK(CLK40),  // 40 MHz
        .TDC_IN(HITOR[0]),
        .TDC_OUT(TDC_IN_FROM_TDC[0]),
        .TRIG_IN(LEMO_TRIGGER),
        .TRIG_OUT(LEMO_TRIGGER_FROM_TDC[0]),

        // input/output trigger signals for broadcasting mode
        .FAST_TRIGGER_IN(16'b0),
        .FAST_TRIGGER_OUT(FAST_TRIGGER_OUT),  // collect 640 MHz sampled trigger signal to pass it to other TDC modules

        .FIFO_READ(TDC_FIFO_READ[0]),
        .FIFO_EMPTY(TDC_FIFO_EMPTY[0]),
        .FIFO_DATA(TDC_FIFO_DATA[0]),

        .BUS_CLK(BUS_CLK),
        .BUS_RST(BUS_RST),
        .BUS_ADD(BUS_ADD),
        .BUS_DATA(BUS_DATA),
        .BUS_RD(BUS_RD),
        .BUS_WR(BUS_WR),

        .ARM_TDC(1'b0),
        .EXT_EN(1'b0),

        .TIMESTAMP(TIMESTAMP[15:0])
    );

    // Additional TDC modules: Use the fast sampled trigger signal from first TDC module.
    genvar i;
    generate
      for (i = 1; i < 4; i = i + 1) begin: tdc_gen
        tdc_s3 #(
            .BASEADDR(TDC_BASEADDR + 32'h0100*i),
            .HIGHADDR(TDC_HIGHADDR + 32'h0100*i),
            .ABUSWIDTH(32),
            .CLKDV(CLKDV),
            .DATA_IDENTIFIER(4'b0001 + i),
            .FAST_TDC(1),
            .FAST_TRIGGER(1),
            .BROADCAST(1)  // generate for first TDC module the 640MHz sampled trigger signal and share it with other TDC mddules (broadcast)
        ) i_tdc (
            .CLK320(CLK320),  // 320 MHz
            .CLK160(CLK160),  // 160 MHz
            .DV_CLK(CLK40),  // 40 MHz
            .TDC_IN(HITOR[i]),
            .TDC_OUT(TDC_IN_FROM_TDC[i]),
            // Use FAST_TRIGGER_IN as input for trigger signal
            .TRIG_IN(1'b0),
            .TRIG_OUT(),

            // input/output trigger signals for broadcasting mode
            .FAST_TRIGGER_IN(FAST_TRIGGER_OUT),  // Use the already existing 640 MHz sampled trigger signal from first module as FAST TRIGGER (broadcast)
            .FAST_TRIGGER_OUT(LEMO_TRIGGER_FROM_TDC[i]),

            .FIFO_READ(TDC_FIFO_READ[i]),
            .FIFO_EMPTY(TDC_FIFO_EMPTY[i]),
            .FIFO_DATA(TDC_FIFO_DATA[i]),

            .BUS_CLK(BUS_CLK),
            .BUS_RST(BUS_RST),
            .BUS_ADD(BUS_ADD),
            .BUS_DATA(BUS_DATA),
            .BUS_RD(BUS_RD),
            .BUS_WR(BUS_WR),

            .ARM_TDC(1'b0),
            .EXT_EN(1'b0),

            .TIMESTAMP(TIMESTAMP[15:0])
        );
      end
    endgenerate
`else
    // TDC module for KC705. Only possible to sample HitOr. Trigger distance measurement is not possbile since have no trigger signal.
    genvar i;
    generate
      for (i = 0; i < 4; i = i + 1) begin: tdc_gen
        tdc_s3 #(
            .BASEADDR(TDC_BASEADDR + 32'h0100*i),
            .HIGHADDR(TDC_HIGHADDR + 32'h0100*i),
            .ABUSWIDTH(32),
            .CLKDV(CLKDV),
            .DATA_IDENTIFIER(4'b0001 + i),
            .FAST_TDC(1),
            .FAST_TRIGGER(0), // Cannot use FAST_TRIGGER mode for KC705, since have no external TRIGGER signal.
            .BROADCAST(0)
        ) i_tdc (
            .CLK320(CLK320),  // 320 MHz
            .CLK160(CLK160),  // 160 MHz
            .DV_CLK(CLK40),  // 40 MHz
            .TDC_IN(HITOR[i]),
            .TDC_OUT(TDC_IN_FROM_TDC[i]),
            .TRIG_IN(LEMO_TRIGGER),
            .TRIG_OUT(LEMO_TRIGGER_FROM_TDC[0]),

            .FIFO_READ(TDC_FIFO_READ[i]),
            .FIFO_EMPTY(TDC_FIFO_EMPTY[i]),
            .FIFO_DATA(TDC_FIFO_DATA[i]),

            .BUS_CLK(BUS_CLK),
            .BUS_RST(BUS_RST),
            .BUS_ADD(BUS_ADD),
            .BUS_DATA(BUS_DATA),
            .BUS_RD(BUS_RD),
            .BUS_WR(BUS_WR),

            .ARM_TDC(1'b0),
            .EXT_EN(1'b0),

            .TIMESTAMP(TIMESTAMP[15:0])
        );
      end
    endgenerate
`endif


// ----- AURORA ----- //
wire AURORA_RX_FIFO_READ;
wire AURORA_RX_FIFO_EMPTY;
wire [31:0] AURORA_RX_FIFO_DATA;
wire AUR_LOST_ERR;

rx_aurora_64b66b #(
    .BASEADDR(AURORA_RX_BASEADDR),
    .HIGHADDR(AURORA_RX_HIGHADDR),
    .ABUSWIDTH(32),
    .IDENTIFIER(0)
) i_aurora_rx (
    `ifdef USBPIX3
        .RXP( { 2'b00, MGT_RX_P[1], MGT_RX_P[0] } ),
        .RXN( { 2'b00, MGT_RX_N[1], MGT_RX_N[0] } ),
    `else
        .RXP( { MGT_RX_P[0], MGT_RX_P[1], MGT_RX_P[2], MGT_RX_P[3] } ),     // lane reordering
        .RXN( { MGT_RX_N[0], MGT_RX_N[1], MGT_RX_N[2], MGT_RX_N[3] } ),     // lane reordering
    `endif
    .TX_P(TX_P), .TX_N(TX_N),
    .RX_CLK_IN_P(RX_CLK_IN_P),
    .RX_CLK_IN_N(RX_CLK_IN_N),
    .INIT_CLK_IN_P(INIT_CLK_IN_P),
    .INIT_CLK_IN_N(INIT_CLK_IN_N),
    .REFCLK1_OUT(REFCLK1_OUT),
    .TX_OUT_CLK(TX_OUT_CLK),
    .AURORA_CLK_OUT(AURORA_CLK_OUT),

    .MGT_REF_SEL(CLK_SEL),
    .AURORA_RESET(AURORA_RESET),
    .LOST_ERROR(AUR_LOST_ERR),
    .RX_LANE_UP(RX_LANE_UP),
    .RX_CHANNEL_UP(RX_CHANNEL_UP),
    .PLL_LOCKED(PLL_LOCKED),
    .RX_SOFT_ERROR(RX_SOFT_ERROR), .RX_HARD_ERROR(RX_HARD_ERROR),

    .CLK_CMD(CLK_CMD),
    .CMD_DATA(CMD_DATA),
    .CMD_OUTPUT_EN(CMD_OUTPUT_EN),

    .FIFO_READ(AURORA_RX_FIFO_READ),
    .FIFO_EMPTY(AURORA_RX_FIFO_EMPTY),
    .FIFO_DATA(AURORA_RX_FIFO_DATA),

    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR)
);

// FIFO for Aurora data
wire AURORA_FIFO_FULL;

gerneric_fifo
#(
    .DATA_SIZE(32),
    .DEPTH(1024*8)
)  aurora_fifo_i
(
    .clk(BUS_CLK), .reset(BUS_RST),
    .write(!AURORA_RX_FIFO_EMPTY),
    .read(ARB_GRANT_AURORA_RX),
    .data_in(AURORA_RX_FIFO_DATA),
    .full(AURORA_FIFO_FULL),
    .empty(AURORA_FIFO_EMPTY),
    .data_out(AURORA_FIFO_DATA), .size()
);

assign FIFO_WRITE = ARB_WRITE_OUT;
assign TLU_FIFO_READ = ARB_GRANT_TLU_FIFO;
assign AURORA_RX_FIFO_READ = !AURORA_FIFO_FULL;
assign TDC_FIFO_READ = ARB_GRANT_TDC_FIFO;

assign FIFO_DATA = ARB_DATA_OUT;

// DEBUGGING PORTS
//assign DEBUG_TX0  = TX_OUT_CLK; // RX_CLK_IN_P   TX_OUT_CLK  AURORA_RESET    //CLKCMD && CMD_DATA;    // ------------------------------------------------------------------> DEBUGGING
//assign DEBUG_TX1 = REFCLK1_OUT;    //CMD_DATA;

endmodule
