#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import shutil

from bdaq53.tests import utils
from bdaq53.scans.test_registers import RegisterTest


class TestRegisterTest(unittest.TestCase):
    def test_test_registers(self):
        self.test = RegisterTest(utils.setup_cocotb())
        self.test.start(ignore=list(range(3, 138)))   # Only test register at address 1
        self.assertTrue(self.test.analyze())

    def tearDown(self):
        self.test.close()
        utils.close_sim()
        shutil.rmtree('output_data/', ignore_errors=True)


if __name__ == '__main__':
    unittest.main()
