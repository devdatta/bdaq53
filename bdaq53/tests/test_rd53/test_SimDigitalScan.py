#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import zlib # workaround for matplotlib segmentation fault
import unittest
import logging
import shutil
import tables as tb
import numpy as np

from bdaq53.tests import utils
from bdaq53.scans.scan_digital import DigitalScan


configuration = {
    'n_injections': 2,

    'mask_step': 2,
    'start_column': 160,
    'stop_column': 161,
    'start_row': 1,
    'stop_row': 120,
}


class TestDigitalScan(unittest.TestCase):
    def test_scan_digital(self):
        logging.info('Starting digital scan test')

        self.scan = DigitalScan(utils.setup_cocotb(20))
        self.scan.start(**configuration)
        self.scan.analyze()

        ''' Assert raw data '''
        with tb.open_file(self.scan.output_filename + '_interpreted.h5', 'r+') as in_file_h5:
            hist_occ = in_file_h5.root.HistOcc[:].sum(axis=2)

        self.assertEqual(np.sum(hist_occ), 119 * configuration['n_injections'], 'Incorrect number of hits has been recorded!')
        self.assertEqual(hist_occ[160, :].tolist(), [0] + [configuration['n_injections']] * 119 + [0] * 72)

    def tearDown(self):
        self.scan.close()
        utils.close_sim()
        shutil.rmtree('output_data/', ignore_errors=True)


if __name__ == '__main__':
    unittest.main()