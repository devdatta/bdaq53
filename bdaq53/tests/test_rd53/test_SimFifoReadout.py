#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import numpy as np
import logging

from bdaq53.tests import utils
from bdaq53.bdaq53 import BDAQ53
from bdaq53.rd53a import RD53A
from bdaq53.fifo_readout import FifoReadout
from bdaq53.analysis import analysis_utils


class TestFifoReadout(unittest.TestCase):
    def setUp(self):
        self.bdaq = BDAQ53(utils.setup_cocotb())
        self.bdaq.init()

        self.chip = RD53A(self.bdaq)

    def test_fifo_readout(self):
        logging.info('Starting FIFO readout test')

        # Configure cmd encoder
        self.bdaq['cmd'].reset()
        self.chip.write_command(self.chip.write_sync(write=False) * 32)

        # Wait for PLL lock
        self.assertTrue(self.bdaq.wait_for_pll_lock())

        fr = FifoReadout(self.bdaq)

        # Setup Aurora
        self.chip.setup_aurora()
        self.chip.write_command(self.chip.write_sync(write=False) * 32)
        self.chip.write_ecr()
        self.chip.registers.write_all(force=True)
        self.assertTrue(self.bdaq.wait_for_aurora_sync())

        fr.print_readout_status()
        fr.start(fill_buffer=True)

        # insert empty triggers
        indata = self.chip.send_trigger(trigger=0b1111, write=False) * 4
        self.chip.write_command(indata)

        # progress simulation
        for _ in range(100):
            self.bdaq['rx'].get_rx_ready()

        fr.stop()
        fr.print_readout_status()

        rawdata = np.concatenate([item[0] for item in fr.data])

        hits, hist_occ, hist_tot, hist_rel_bcid, hist_event_status, hist_bcid_error = analysis_utils.init_outs(len(rawdata), 0)
        analysis_utils.interpret_data(rawdata, hits, hist_occ, hist_tot,
                                      hist_rel_bcid, hist_event_status, hist_bcid_error, scan_param_id=0)

        self.assertEqual(len(hits), 32)

    def tearDown(self):
        self.bdaq.close()
        utils.close_sim()


if __name__ == '__main__':
    unittest.main()
