#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
import unittest

import tables as tb
import numpy as np

logger = logging.getLogger(__file__)


class TestScanScripts(unittest.TestCase):
    def test_digital_scan(self):
        ''' Test digital scan '''
        from bdaq53.scans import scan_digital
        scan = scan_digital.DigitalScan()
        scan.start(**scan_digital.local_configuration)
        scan.close()

        with tb.open_file(scan.output_filename + '_interpreted.h5') as in_file:
            enable_mask = in_file.root.configuration.enable_mask[:]
            logger.error(np.count_nonzero((in_file.root.HistOcc[:].sum(axis=2)[enable_mask] != 100)))
            logger.info('Integrated occupancy: %s' % np.sum(in_file.root.HistOcc[:].sum(axis=2)[enable_mask]))
            self.assertTrue(np.all(in_file.root.HistOcc[:].sum(axis=2)[enable_mask] == 100))
            # FIXME: Skip these two for now until shift in BCID and ToT is unterstood
            # self.assertTrue(np.all(in_file.root.HistRelBCID[:] == 15))
            # self.assertTrue(np.all(in_file.root.HistTot[:] == 2))
            self.assertTrue(np.all(in_file.root.HistBCIDError[:] == 0))
            self.assertTrue(np.all(in_file.root.HistEventStatus[:] == 0))

    def test_analog_scan(self):
        ''' Test analog scan '''
        from bdaq53.scans import scan_analog
        scan = scan_analog.AnalogScan()
        scan.start(**scan_analog.local_configuration)
        scan.close()

        with tb.open_file(scan.output_filename + '_interpreted.h5') as in_file:
            logger.error(np.count_nonzero((in_file.root.HistOcc[:].sum(axis=2) != 100)))
            self.assertTrue(np.count_nonzero(in_file.root.HistOcc[:].sum(axis=2) == 100) > 0.99 * 400 * 192)
            self.assertTrue(np.any(in_file.root.HistRelBCID[:]))
            self.assertTrue(np.any(in_file.root.HistTot[:]))
            # We expect BCID errors from SYNC
            # self.assertTrue(np.all(in_file.root.HistBCIDError[:] == 0))
            # self.assertTrue(np.all(in_file.root.HistEventStatus[:] == 0))

    def test_threshold_scan(self):
        ''' Test threshold scan and results '''
        from bdaq53.scans import scan_threshold
        scan = scan_threshold.ThresholdScan()
        scan.start(**scan_threshold.local_configuration)
        scan.close()

#         with tb.open_file(scan.output_filename + '_interpreted.h5') as in_file:
#             logger.error(np.count_nonzero((in_file.root.HistOcc[:].sum(axis=2) != 100)))
#             self.assertTrue(np.count_nonzero(in_file.root.HistOcc[:].sum(axis=2) == 100) > 0.99 * 400 * 192)
#             self.assertTrue(np.any(in_file.root.HistRelBCID[:]))
#             self.assertTrue(np.any(in_file.root.HistTot[:]))
#             # We expect BCID errors from SYNC
#             # self.assertTrue(np.all(in_file.root.HistBCIDError[:] == 0))
#             # self.assertTrue(np.all(in_file.root.HistEventStatus[:] == 0))

#     def test_fast_threshold_scan(self):
#         ''' Test fast threshold scan and results '''
#         from bdaq53.scans import scan_threshold_fast
#         scan = scan_threshold_fast.FastThresholdScan()
#         scan.start(**scan_threshold_fast.local_configuration)
#         scan.close()
#
#     def test_register_test(self):
#         from bdaq53.scans import test_registers
#         scan = test_registers.RegisterTest()
#         scan.start(**test_registers.local_configuration)
#         scan.close()
#
#     def test_threshold_noise_tuning(self):
#         from bdaq53.scans import tune_local_threshold_noise
#         scan = tune_local_threshold_noise.NoiseTuning()
#         scan.start(**tune_local_threshold_noise.local_configuration)
#         scan.close()


if __name__ == '__main__':
    unittest.main()
