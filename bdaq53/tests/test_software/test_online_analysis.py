#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
import os
import unittest

import tables as tb
import numpy as np
import matplotlib
matplotlib.use('Agg')  # noqa: E402 Allow headless plotting

import bdaq53  # noqa: E731
from bdaq53.analysis import online as oa
from bdaq53.tests import utils

bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))


def get_raw_data(raw_data_file):
    ''' Yield data of one readout

        Delay return if replay is too fast
    '''
    with tb.open_file(raw_data_file, mode="r") as in_file_h5:
        meta_data = in_file_h5.root.meta_data[:]
        raw_data = in_file_h5.root.raw_data
        n_readouts = meta_data.shape[0]

        for i in range(n_readouts):
            # Raw data indeces of readout
            i_start = meta_data['index_start'][i]
            i_stop = meta_data['index_stop'][i]

            yield raw_data[i_start:i_stop]


class TestOnlineAnalysis(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestOnlineAnalysis, cls).setUpClass()

        ana_logger = logging.getLogger('OnlineAnalysis')
        cls._ana_log_handler = utils.MockLoggingHandler(level='DEBUG')
        ana_logger.addHandler(cls._ana_log_handler)
        cls.ana_log_messages = cls._ana_log_handler.messages

        cls.occ_hist_oa = oa.OccupancyHistogramming()

    @classmethod
    def tearDownClass(cls):
        del cls.occ_hist_oa

    def test_occupancy_histogramming(self):
        ''' Test online occupancy histogramming '''
        raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
        raw_data_file_result = os.path.join(data_folder, 'analog_scan_interpreted_result.h5')
        self.occ_hist_oa.reset()
        # Check result
        for words in get_raw_data(raw_data_file):
            self.occ_hist_oa.add(words)
        occ_hist = self.occ_hist_oa.get()

        with tb.open_file(raw_data_file_result) as in_file:
            occ_hist_exptected = in_file.root.HistOcc[:, :, 0]
            self.assertTrue(np.array_equal(occ_hist_exptected, occ_hist))

    def test_occupancy_histogramming_errors(self):
        # Check error message when requesting histogram before analysis finished
        raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
        self.occ_hist_oa.reset()
        for words in get_raw_data(raw_data_file):
            self.occ_hist_oa.add(words)
        self.occ_hist_oa.get(timeout=0.001)
        # Check that warning was given
        self.assertTrue('Getting histogram while adding data' in self.ana_log_messages['warning'])

        # Check error message when resetting histogram before analysis finished
        self.occ_hist_oa.reset()
        for words in get_raw_data(raw_data_file):
            self.occ_hist_oa.add(words)
        self.occ_hist_oa.reset(timeout=0.001)
        # Check that warning was given
        self.assertTrue('Resetting histogram while adding data' in self.ana_log_messages['warning'])


if __name__ == '__main__':
    logging.basicConfig(filemode='w', level=logging.DEBUG)
    unittest.main()
