#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

''' Test methods of the analysis.analysis_utils module '''

import os
import unittest

import tables as tb
import numpy as np

import bdaq53.analysis.analysis_utils as au
from bdaq53.tests import utils

import bdaq53
bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data', 'fixtures'))


class TestAnalysisUtils(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):
        os.remove(os.path.join(data_folder, 'threshold_scan_fitted.h5'))
        os.remove(os.path.join(data_folder, 'threshold_scan_inv_fitted.h5'))
        os.remove(os.path.join(data_folder, 'analog_scan_chunked_interpreted.h5'))
        os.remove(os.path.join(data_folder, 'ext_trigger_scan_replayed.h5'))
        os.remove(os.path.join(data_folder, 'injection_delay_scan_scurves_interpreted.h5'))

    def test_replay_function(self):
        ''' Test triggered data replay function '''
        from bdaq53.scans.scan_eudaq import replay_triggered_data

        raw_data_file = os.path.join(data_folder, 'ext_trigger_scan.h5')
        raw_data_file_replayed = raw_data_file[:-3] + '_replayed.h5'

        with tb.open_file(raw_data_file_replayed, 'w') as out_file:
            raw_data_earray = out_file.create_earray(out_file.root,
                                                     name='raw_data',
                                                     atom=tb.UIntAtom(),
                                                     shape=(0,),
                                                     title='raw_data',
                                                     filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            for dat in replay_triggered_data(raw_data_file, real_time=False):
                raw_data_earray.append(dat)

        # Replayed raw data array has to be the same than original
        equal, msg = utils.compare_h5_files(raw_data_file,
                                            raw_data_file_replayed,
                                            node_names=('raw_data',))
        self.assertTrue(equal, msg=msg)

    def test_thr_scan_fit(self):
        ''' Test fitting of threshold scan data. '''
        with tb.open_file(os.path.join(data_folder, 'threshold_scan_fitted_result.h5')) as in_file:
            with tb.open_file(os.path.join(data_folder, 'threshold_scan_fitted.h5'), 'w') as out_file:
                config = au.ConfigDict(in_file.root.configuration.run_config[:])
                start = config['VCAL_HIGH_start']
                stop = config['VCAL_HIGH_stop']
                step = config['VCAL_HIGH_step']
                n_injections = config['n_injections']
                hist_scurve = in_file.root.HistSCurve[:]
                scan_params = range(start, stop, step)

                threshold_map, noise_map, chi2_map = au.fit_scurves_multithread(hist_scurve,
                                                                                scan_params=scan_params,
                                                                                n_injections=n_injections)
                filters = tb.Filters(complib='blosc', complevel=5, fletcher32=False)
                out_file.create_carray(out_file.root,
                                       name='ThresholdMap',
                                       title='Threshold Map',
                                       obj=threshold_map,
                                       filters=filters)
                out_file.create_carray(out_file.root,
                                       name='NoiseMap',
                                       title='Noise Map',
                                       obj=noise_map,
                                       filters=filters)

                out_file.create_carray(out_file.root,
                                       name='Chi2Map',
                                       title='Chi2 / ndf Map',
                                       obj=chi2_map,
                                       filters=filters)

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'threshold_scan_fitted.h5'),
            os.path.join(data_folder, 'threshold_scan_fitted_result.h5'),
            node_names=['NoiseMap', 'ThresholdMap', 'Chi2Map'],
            exact=False,
            # Allow Threshold/Noise to be 1% off due to fit
            rtol=0.01)
        self.assertTrue(data_equal, msg=error_msg)

    def test_thr_scan_inv_fit(self):
        ''' Test fitting of threshold scan data with x-inverted curves. '''
        with tb.open_file(os.path.join(data_folder, 'global_threshold_tuning_interpreted_result.h5')) as in_file:
            with tb.open_file(os.path.join(data_folder, 'threshold_scan_inv_fitted.h5'), 'w') as out_file:
                config = au.ConfigDict(in_file.root.configuration.run_config[:])
                start = config['VTH_start']
                stop = config['VTH_stop']
                step = config['VTH_step']
                n_injections = config['n_injections']
                hist_scurve = in_file.root.HistOcc[:].reshape(in_file.root.HistOcc.shape[0] * in_file.root.HistOcc.shape[1],
                                                              in_file.root.HistOcc.shape[2])
                scan_params = range(start, stop, -1 * step)

                threshold_map, noise_map, chi2_map = au.fit_scurves_multithread(hist_scurve,
                                                                                scan_params=scan_params,
                                                                                n_injections=n_injections,
                                                                                invert_x=True)
                filters = tb.Filters(complib='blosc',
                                     complevel=5,
                                     fletcher32=False)
                out_file.create_carray(out_file.root,
                                       name='ThresholdMap',
                                       title='Threshold Map',
                                       obj=threshold_map,
                                       filters=filters)
                out_file.create_carray(out_file.root,
                                       name='NoiseMap',
                                       title='Noise Map',
                                       obj=noise_map,
                                       filters=filters)
                out_file.create_carray(out_file.root,
                                       name='Chi2Map',
                                       title='Chi2 / ndf Map',
                                       obj=chi2_map,
                                       filters=filters)

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'threshold_scan_inv_fitted.h5'),
            os.path.join(data_folder, 'threshold_scan_inv_fitted_result.h5'),
            node_names=['NoiseMap', 'ThresholdMap', 'Chi2Map'],
            exact=False,
            # Allow Threshold/Noise to be 1% off due to fit
            rtol=0.01)
        self.assertTrue(data_equal, msg=error_msg)

    def test_step_function_fit(self):
        with tb.open_file(os.path.join(data_folder, 'injection_delay_scan_scurves.h5')) as in_file:
            scurves = in_file.root.HistSCurve[:]
            scan_param_range = np.arange(scurves.shape[1])
            threshold_map, noise_map, _ = au.fit_scurves_multithread(scurves, scan_param_range,
                                                                     n_injections=100,
                                                                     invert_x=False,
                                                                     optimize_fit_range=False)

            with tb.open_file(os.path.join(data_folder, 'injection_delay_scan_scurves_interpreted.h5'), 'w') as out_file:
                out_file.create_carray(out_file.root,
                                       name='HistScurveMean',
                                       title='Occupancy Histogram',
                                       obj=threshold_map,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))
                out_file.create_carray(out_file.root,
                                       name='HistScurveSigma',
                                       title='Occupancy Histogram',
                                       obj=noise_map,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'injection_delay_scan_scurves_interpreted.h5'),
            os.path.join(data_folder, 'injection_delay_scan_scurves_result.h5'),
            exact=False,
            # Allow Threshold/Noise to be 1% off due to fit
            rtol=0.01)
        self.assertTrue(data_equal, msg=error_msg)

    def test_helper_func_ana(self):
        ''' Test helper function for quick in-scan raw data analysis.
        '''
        chunk_size = 100000
        with tb.open_file(os.path.join(data_folder, 'analog_scan.h5')) as in_file:
            with tb.open_file(os.path.join(data_folder, 'analog_scan_chunked_interpreted.h5'), 'w') as out_file:
                raw_data = in_file.root.raw_data

                n_raw = raw_data.shape[0]

                (_, hist_occ, hist_tot, hist_rel_bcid,
                 hist_event_status, hist_bcid_error) = au.init_outs(n_raw * 4,
                                                                    n_scan_params=1)

                for i in range(0, n_raw, chunk_size):
                    data = au.analyze_chunk(rawdata=raw_data[i:i + chunk_size],
                                            return_hists=('hist_occ', 'hist_tot',
                                                          'hist_rel_bcid',
                                                          'hist_event_status',
                                                          'hist_bcid_error'),
                                            return_hits=True)
                    hist_occ += data['hist_occ']
                    hist_tot += data['hist_tot']
                    hist_rel_bcid += data['hist_rel_bcid']
                    hist_event_status += data['hist_event_status']
                    hist_bcid_error += data['hist_bcid_error']

                filters = tb.Filters(complib='blosc',
                                     complevel=5,
                                     fletcher32=False)
                out_file.create_carray(out_file.root,
                                       name='HistOcc',
                                       title='Occupancy Histogram',
                                       obj=hist_occ,
                                       filters=filters)
                out_file.create_carray(out_file.root,
                                       name='HistTot',
                                       title='ToT Histogram',
                                       obj=hist_tot,
                                       filters=filters)

        data_equal, error_msg = utils.compare_h5_files(os.path.join(data_folder, 'analog_scan_chunked_interpreted.h5'),
                                                       os.path.join(data_folder, 'analog_scan_interpreted_result.h5'),
                                                       # Only hists that do not
                                                       # need proper event
                                                       # building can be tested
                                                       node_names=('HistOcc', 'HistTot'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_fit_less_scurve_ana(self):
        ''' Test fit less S-Curve analysis for values that are negative '''
        n_injections = 100

        for offset in range(-100, 100, 10):
            # Test data
            x = np.array([-12, -11, -10, -9, -8, -7, -6, -5, -4, -3, -
                          2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
            y = np.array([100, 100, 100, 100, 100, 100, 100, 100, 100, 100,
                          100, 100, 59, 16, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

            x += offset
            y = y[::-1]

            sigma = au.get_noise(x=x,
                                 y=y,
                                 n_injections=n_injections)
            mu = au.get_threshold(x=x,
                                  y=y,
                                  n_injections=n_injections)
            mu_fit, sigma_fit, _ = au.fit_scurve(scurve_data=y, scan_params=x,
                                                 n_injections=n_injections,
                                                 sigma_0=sigma)

            self.assertTrue(np.allclose(mu_fit, mu, rtol=1e-02, atol=0.5))
            self.assertTrue(np.allclose(
                sigma_fit, sigma, rtol=1e-02, atol=0.5))

    def test_histogram_stat_funcs(self):
        ''' Test the helper function to get mean and RMS from histograms
        '''

        # Data
        counts = np.arange(100).reshape(10, 10)
        bin_positions_2d = np.repeat(np.arange(10), 10).reshape(10, 10)
        bin_positions_1d = np.arange(10)

        # Check feature that mean calculation is possible with 1 dimensional
        # bin position array only
        self.assertTrue(np.all(au.get_mean_from_histogram(counts, bin_positions_1d,
                                                          axis=0) == au.get_mean_from_histogram(counts, bin_positions_2d, axis=0)))

        # Check mean calculation for mean into axis=0
        mean = au.get_mean_from_histogram(counts, bin_positions_1d, axis=0)
        for j in range(10):
            mean_expected = np.sum([counts[i][j] * bin_positions_1d[i]
                                    for i in range(10)]).astype(np.float) / counts[:, j].sum()
            self.assertEqual(mean[j], mean_expected)
        # Check mean calculation for mean into axis=1
        mean = au.get_mean_from_histogram(counts, bin_positions_1d, axis=1)
        for j in range(10):
            mean_expected = np.sum([counts[j][i] * bin_positions_1d[i]
                                    for i in range(10)]).astype(np.float) / counts[j].sum()
            self.assertEqual(mean[j], mean_expected)

        # Check std calculation for std into axis=0
        rms = au.get_std_from_histogram(counts, bin_positions_2d, axis=0)
        for j in range(10):
            a = []
            for i in range(10):
                for _ in range(counts[i][j]):
                    a.append(bin_positions_1d[i])
            rms_expected = np.std(a)

            self.assertTrue(np.allclose(rms[j], rms_expected))

        # Check std calculation for std into axis=0
        rms = au.get_std_from_histogram(counts, bin_positions_2d.T, axis=1)
        for j in range(10):
            a = []
            for i in range(10):
                for _ in range(counts[j][i]):
                    a.append(bin_positions_1d[i])
            rms_expected = np.std(a)

            self.assertTrue(np.allclose(rms[j], rms_expected))

        # Check check on reasonable bin positions
        with self.assertRaises(ValueError) as context:
            au.get_std_from_histogram(counts, bin_positions_2d, axis=1)
            self.assertTrue('The bin position are all 0' in context.exception)

    def test_config_dict(self):
        ''' Check that the config dict data type casts are correct
            even when the key is a byte-array
        '''
        dtypes = [int, str, list, tuple, dict]

        # Test with string keys
        cfg = {'0': 0, '1': '1a', '2': [2], '3': (3, ), '4': {4: 4}}
        cfg_dict = au.ConfigDict(cfg)
        for i in range(5):
            self.assertTrue(isinstance(cfg_dict[str(i)], dtypes[i]))

        # Test with byte array keys
        cfg = {b'0': 0, b'1': '1a', b'2': [2], b'3': (3, ), b'4': {4: 4}}
        cfg_dict = au.ConfigDict(cfg)
        for i in range(5):
            print(str(i))
            self.assertTrue(isinstance(cfg_dict[str(i)], dtypes[i]))

        # Test with byte array values
        cfg = {b'0': 0, b'1': b'1a', b'2': [2], b'3': (3, ), b'4': {4: 4}}
        cfg_dict = au.ConfigDict(cfg)
        for i in range(5):
            self.assertTrue(isinstance(cfg_dict[str(i)], dtypes[i]))


if __name__ == '__main__':
    unittest.main()
