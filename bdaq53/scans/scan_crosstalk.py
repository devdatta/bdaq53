#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic scan injects a specified charge into one pixel per core
    and reads out all sourrounding pixels to check for crosstalk.
'''

import numpy as np

from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


local_configuration = {
    # Scan parameters
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,

    'column_step': 8,
    'row_step': 8,

    'maskfile': 'auto',
    'use_good_pixels_diff': False,

    # DAC settings
    'VCAL_MED': 0,
    'VCAL_HIGH': 4096
}


class CrosstalkScan(ScanBase):
    scan_id = "crosstalk_scan"

    def configure(self, VCAL_MED=1000, VCAL_HIGH=4000, **kwargs):
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED)

    def prepare_injection_masks(self, start_column, stop_column, start_row, stop_row, mask_step, column_step, row_step):
        mask_data = []
        cmd_mem_size = self.bdaq['cmd'].MEM_BYTES

        crosstalk_mask_total = np.zeros((400, 192), dtype=bool)
        crosstalk_mask_total[start_column:stop_column:column_step, start_row:stop_row:row_step] = True
        crosstalk_mask_total = crosstalk_mask_total[start_column:stop_column, start_row:stop_row]

        flavor_cols = []
        if start_column < 128:
            flavor_cols.append([start_column, min(stop_column, 128), 0])
        if start_column < 264 and stop_column > 128:
            flavor_cols.append([max(start_column, 128), min(stop_column, 264), 1])
        if stop_column > 264:
            flavor_cols.append([max(start_column, 264), stop_column, 2])

        for (start_column_flavor, stop_column_flavor, flavor_id) in flavor_cols:
            cols = (stop_column_flavor - start_column_flavor)
            rows = (stop_row - start_row)

            crosstalk_mask = crosstalk_mask_total[start_column_flavor:stop_column_flavor, start_row:stop_row]

            inj_mask = np.zeros((cols * rows), dtype=bool)
            inj_mask[::mask_step] = True

            # disable flavour that is not scaned (other way sync is doing something bad)
            indata = self.chip.enable_core_col_clock(core_cols=range(int(start_column_flavor / 8), int((stop_column_flavor) / 8)), write=False)

            for step in range(mask_step + 1):
                mask = []

                if(step == mask_step):
                    inj_mask[:] = False

                inj_mask_2d = np.reshape(inj_mask, (cols, rows))

                injection_mask_prev = self.chip.injection_mask.copy()
                self.chip.injection_mask[start_column_flavor:stop_column_flavor, start_row:stop_row] = np.logical_and(inj_mask_2d, crosstalk_mask)

                self.chip.enable_mask = np.invert(self.chip.injection_mask)

                # clauclulate the mask diffrence (xor) witin pixel pair (double column)
                injection_mask_xor = np.logical_xor(injection_mask_prev, self.chip.injection_mask)
                injection_mask_xor_dcol = np.logical_or(injection_mask_xor[::2, :], injection_mask_xor[1::2, :])
                to_change = np.where(injection_mask_xor_dcol > 0)

                if step == 0:
                    indata += self.chip.registers['PIX_DEFAULT_CONFIG'].get_write_command(0)
                else:
                    indata = self.chip.registers['PIX_DEFAULT_CONFIG'].get_write_command(0)

                for i in range(len(to_change[0])):
                    next_cmd = self.chip.write_double_pixel(to_change[0][i], to_change[1][i])

                    if len(indata) + len(next_cmd) + 1 > cmd_mem_size:  # +1 to overcome issue #145
                        mask.append(indata)
                        indata = self.chip.registers['PIX_DEFAULT_CONFIG'].get_write_command(0)

                    indata += next_cmd

                mask.append(indata)
                mask_data.append({'flavor': flavor_id, 'command': mask})

                inj_mask[1:] = inj_mask[:-1]  # shift mask
                inj_mask[0] = 0

        return mask_data

    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, mask_step=192 + 24, column_step=8, row_step=8, n_injections=100, **kwargs):
        '''
        Crosstalk scan main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        mask_step : int
            Row-stepsize of the injection mask. Every mask_stepth row is injected at once.
        column_step : int
            Every column_stepth column is injected to
        row_step : int
            Eveery row_stepth row is injected to
        n_injections : int
            Number of injections.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            VCAL_HIGH DAC value.
        '''

        self.logger.info('Preparing injection masks...')
        mask_data = self.prepare_injection_masks(start_column, stop_column, start_row, stop_row, mask_step, column_step, row_step)

        self.logger.info('Starting scan...')
        pbar = tqdm(total=len(mask_data), unit=' Mask steps')
        with self.readout():
            for mask in mask_data:
                self.chip.write_command(mask['command'])

                if mask['flavor'] == 0:
                    self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections)
                else:
                    self.chip.inject_analog_single(repetitions=n_injections)
                pbar.update(1)

        pbar.close()
        self.logger.success('Scan finished')

    def analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
            a.analyze_data()

        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()


if __name__ == "__main__":
    scan = CrosstalkScan()
    scan.start(**local_configuration)
    scan.close()
