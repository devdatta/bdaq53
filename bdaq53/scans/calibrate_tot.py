#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
   Creates the ToT calibration per pixel using the charge injection cicuit.
'''

from tqdm import tqdm
import tables as tb
import numpy as np

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting
from bdaq53.analysis import analysis_utils as au

local_configuration = {
    # Scan parameters
    'start_column': 130,
    'stop_column': 131,
    'start_row': 50,
    'stop_row': 51,
    'maskfile': None,
    'use_good_pixels_diff': False,

    'VCAL_MED': 500,
    # List of Delta VCAL high values which are scanned
    'VCAL_HIGH_values': range(800, 4001, 200)
}


def get_rms_from_2d_hist(counts, bin_positions, result):
    ''' Compute the standard deviation along the last axis of a 2d histogram
    by repeating elements of (bin_positions) a number of (counts) times.

    Parameters
    ----------
        counts: Array containing data to be averaged
        bin_positions: array_like associated with the values in counts.
        result: array like
            Filled with result
    '''

    for i in tqdm(np.arange(0, counts.shape[0]), unit=' Pixels', unit_scale=True):  # pixel loop
        for j in np.arange(0, counts.shape[1]):  # parameter loop
            if np.any(counts[i][j]):
                result[i][j] = np.std(np.repeat(bin_positions, counts[i][j]))
            else:
                result[i][j] = np.NaN


def hist_2d_mean(counts, bin_positions, axis=0, param_count=0):
    ''' Mean along axis 2d histogram of two data samples.
        Parameters

        ----------
        counts : Array containing data to be histogramed
        bin_positions: array_like associated with the values in counts.
        axis: None or int
        param_count : binning values
    '''

    mean = au.get_mean_from_histogram(counts, bin_positions, axis)
    x_bins = np.arange(param_count)
    x = np.tile(x_bins, mean[:].shape[0])
    y = mean[:].ravel()
    histogram = np.histogram2d(x, y, bins=(param_count, counts.shape[2]), range=((x_bins.min(), len(x_bins)), (0, counts.shape[2])))[0]
    return histogram


class TotCalibration(ScanBase):
    scan_id = "tot_calibration"

    def configure(self, **kwargs):
        super(TotCalibration, self).configure(**kwargs)
        self.chip.set_dacs(**kwargs)

    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, mask_step=192 + 24, n_injections=100, VCAL_MED=500, VCAL_HIGH_values=range(800, 4001, 200), **_):
        '''
        Threshold scan main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        mask_step : int
            Chip size mask of the injection. Distance between pixels that are injected.
        n_injections : int
            Number of injections.
        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_values : list
            List of VCAL_HIGH DAC values.
        '''

        mask_data = self.prepare_injection_masks(start_column, stop_column, start_row, stop_row, mask_step)
        values = VCAL_HIGH_values

        self.logger.info('Starting scan...')
        pbar = tqdm(total=len(mask_data) * len(values), unit=' Mask steps')
        for scan_param_id, value in enumerate(values):
            self.chip.setup_analog_injection(vcal_high=value, vcal_med=VCAL_MED)
            self.store_scan_par_values(scan_param_id=scan_param_id, vcal_high=value, vcal_med=VCAL_MED)
            with self.readout(scan_param_id=scan_param_id):
                for mask in mask_data:
                    self.chip.write_command(mask['command'])
                    self.chip.inject_analog_single(repetitions=n_injections, send_ecr=mask['flavor'] == 0)
                    pbar.update(1)
        pbar.close()
        self.logger.info('Scan finished')

    def analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', store_hits=True) as a:
            a.analyze_data()
            values = np.array(a.get_scan_param_values(scan_parameter='vcal_high'), dtype=np.float)
            chunk_size = a.chunk_size
            tot_max = 16
        # Create col, row, tot histograms from hits
        with tb.open_file(self.output_filename + '_interpreted.h5', "r+") as io_file:
            shape_3d = (400 * 192, values.shape[0], tot_max)
            hist_3d = np.zeros(shape=shape_3d, dtype=np.uint8)

            # Loop over all words in the actual raw data file in chunks
            self.logger.info('Histogramming hits...')
            for i in tqdm(range(0, io_file.root.Hits.shape[0], chunk_size)):
                hits = io_file.root.Hits[i:i + chunk_size]
                channel = hits["col"] + 400 * hits["row"]
                hist_3d += au.hist_3d_index(x=channel, y=hits["scan_param_id"], z=hits["tot"], shape=shape_3d)

            # Create pixel, scan par id, tot hist
            io_file.create_carray(io_file.root,
                                  name='HistTotCal',
                                  title='Tot calibration histogram with channel, scan par id and tot',
                                  obj=hist_3d,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))
            io_file.create_carray(io_file.root,
                                  name='HistTotCalMean',
                                  title='Mean tot calibration histogram',
                                  obj=au.get_mean_from_histogram(hist_3d, bin_positions=np.arange(tot_max), axis=2),
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))

            result = np.zeros(shape=(400 * 192, values.shape[0]))
            self.logger.info('Calculate RMS per pixel...')
            get_rms_from_2d_hist(hist_3d, np.arange(tot_max), result)
            io_file.create_carray(io_file.root,
                                  name='HistTotCalStd',
                                  title='Std tot calibration histogram',
                                  obj=result,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))
            io_file.create_carray(io_file.root,
                                  name='TOThist',
                                  title='2d histogram for TOT values',
                                  obj=hist_2d_mean(hist_3d, bin_positions=np.arange(tot_max), axis=2, param_count=values.shape[0]),
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))

        with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
            p.create_standard_plots()


if __name__ == "__main__":
    scan = TotCalibration()
    scan.start(**local_configuration)
    scan.close()
