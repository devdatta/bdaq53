#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script scans over different amounts of injected charge
    to find the effective threshold of the enabled pixels.
'''

import numpy as np
from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting
from bdaq53.analysis import online as oa

local_configuration = {
    # Scan parameters
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,
    'maskfile': 'auto',
    'use_good_pixels_diff': False,

    # Start threshold scan at injection setting where a fraction of min_response of the selected
    # pixels see the fraction of hits min_hits
    'min_response': 0.01,
    'min_hits': 0.2,
    # Start threshold scan at injection setting where a fraction of max_response of the selected
    # pixels see the fraction of hits max_hits
    'max_response': 0.99,
    'max_hits': 1.,

    'n_injections': 200,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 540,  # start value of injection
    'VCAL_HIGH_stop': 1300,  # maximum injection, can be None
    'VCAL_HIGH_step_fine': 10,  # step size during threshold scan
    'VCAL_HIGH_step_coarse': 100  # step when seaching start
}


class FastThresholdScan(ScanBase):
    scan_id = "fast_threshold_scan"

    def configure(self, **_):
        self.hist_occ = oa.OccupancyHistogramming()

    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, mask_step=192 + 24, n_injections=200,
             VCAL_MED=500, VCAL_HIGH_start=530, VCAL_HIGH_stop=900, VCAL_HIGH_step_fine=10, VCAL_HIGH_step_coarse=100,
             min_response=0.05, max_response=0.95, min_hits=0.2, max_hits=0.8, **_):
        '''
        Threshold scan main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        mask_step : int
            Chip size mask of the injection. Distance between pixels that are injected.
        n_injections : int
            Number of injections.
        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            First VCAL_HIGH value to scan.
        VCAL_HIGH_stop : int
            VCAL_HIGH value to stop the scan. This value is excluded from the scan.
        VCAL_HIGH_step : int
            VCAL_HIGH interval.
        '''

        self.logger.info('Preparing injection masks...')
        mask_data = self.prepare_injection_masks(start_column, stop_column, start_row, stop_row, mask_step)

        self.start_data_taking = False  # indicates if threshold scan has already started
        scan_param_id = -1  # do not store data before S-curve sampling
        vcal_high = VCAL_HIGH_start  # start value

        # Calculated max pixels that can respond
        sel_pix = self.chip.enable_mask[start_column:stop_column, start_row:stop_row]
        n_sel_pix = np.count_nonzero(sel_pix)

        if n_sel_pix == 0:
            raise ValueError('No pixel enabled')

        # Only start progressbar when scanning
        pbar = None

        self.logger.info('Starting scan...')
        while vcal_high < VCAL_HIGH_stop:
            if not self.start_data_taking:
                self.logger.info('Try delta vcal %d', vcal_high - VCAL_MED)
            else:
                pbar.update(vcal_high - VCAL_MED - pbar.n)

            self.chip.setup_analog_injection(vcal_high=vcal_high, vcal_med=VCAL_MED)
            with self.readout(scan_param_id=scan_param_id, callback=self.analyze_data_online):
                for mask in mask_data:
                    self.chip.write_command(mask['command'])
                    if mask['flavor'] == 0:
                        self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections)
                    else:
                        self.chip.inject_analog_single(repetitions=n_injections)
                if self.start_data_taking:
                    self.store_scan_par_values(scan_param_id=scan_param_id, vcal_high=vcal_high, vcal_med=VCAL_MED)

            # Interpret raw data and create occupancy histogram
            occupancy = self.hist_occ.get()
            # Log responding pixels
            if not self.start_data_taking:
                n_pix = np.count_nonzero(occupancy >= n_injections * min_hits)
                self.logger.info('%d of %d pixels (%1.2f percent) see %d percent of the hits', n_pix, n_sel_pix, float(n_pix) / n_sel_pix * 100., int(min_hits * 100.))
            else:
                n_pix = np.count_nonzero(occupancy >= n_injections * max_hits)
                self.logger.info('%d of %d pixels (%1.2f percent) see %d percent of the hits', n_pix, n_sel_pix, float(n_pix) / n_sel_pix * 100., int(max_hits * 100.))
            # Check for stop condition: number of pixels that see all hits
            if np.count_nonzero(occupancy >= n_injections * max_hits) >= n_sel_pix * max_response:
                self.logger.info('S-curve fully sampled. Stop scan')
                break

            if not self.start_data_taking:
                # Check start condition: number of pixels with hits
                if np.count_nonzero(occupancy >= n_injections * min_hits) >= float(n_sel_pix * min_response):
                    self.logger.info('Starting data taking')
                    self.start_data_taking = True
                    scan_param_id += 1
                    # Go back one step
                    if vcal_high - VCAL_HIGH_step_coarse + VCAL_HIGH_step_fine > VCAL_HIGH_start:
                        vcal_high = vcal_high - VCAL_HIGH_step_coarse + VCAL_HIGH_step_fine
                    else:
                        vcal_high = VCAL_HIGH_start
                    pbar = tqdm(initial=vcal_high - VCAL_MED, total=VCAL_HIGH_stop - VCAL_MED, unit=' Delta Vcal')
                else:
                    vcal_high += VCAL_HIGH_step_coarse
            else:  # already scanning, just increase scan pars and value
                scan_param_id += 1
                vcal_high += VCAL_HIGH_step_fine
        # Maximum charge reached and no abort triggered (break of while loop)
        else:
            self.logger.warning('Maximum injection reached. Abort.')

        if pbar is not None:
            pbar.close()

        self.hist_occ.stop.set()  # stop analysis process
        self.logger.success('Scan finished')

    # Used to overwrite data storing function: self.readout.handle_data
    def analyze_data_online(self, data_tuple):
        raw_data = data_tuple[0]
        self.hist_occ.add(raw_data)
        # Only store s-curve data when scan started
        if self.start_data_taking:
            super(FastThresholdScan, self).handle_data(data_tuple)

    def analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
            a.analyze_data()
            mean_thr = np.median(a.threshold_map[np.nonzero(a.threshold_map)])
            mean_noise = np.median(a.noise_map[np.nonzero(a.noise_map)])
            if np.isfinite(mean_thr):
                self.logger.info(
                    'Mean threshold is %i [Delta VCAL]' % (int(mean_thr)))
            else:
                self.logger.error('Mean threshold could not be determined!')

        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()

        return mean_thr, mean_noise


if __name__ == "__main__":
    scan = FastThresholdScan()
    scan.start(**local_configuration)
    scan.close()
