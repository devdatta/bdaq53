#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
   Creates the hitor calibration per pixel using the charge injection cicuit. In order to prevent wrong
   TDC measurements (e.g. due to AFE overshoot, ...) only TDC words are written to the data stream
   if a valid (in time) trigger is measured with the hit (HitOr output). For this hit delay measurement the
   correspondging FW needs to be used (see https://gitlab.cern.ch/silab/bdaq53/wikis/Hardware/Firmware-(development-versions))
   and TX0 has to be connected with RX0 (as short as possible). If the hit delay is not within the measurement range (255 x 1.5625 ns) adjust the
   delay of pulser_cmd_start_loop.
   If a hit delay measurement is not needed, set the `EN_TRIGGER_DIST` and `EN_NO_WRITE_TRIG_ERR` of the TDC module
   to zero.
'''

from tqdm import tqdm
import tables as tb
import numpy as np

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting
from bdaq53.analysis import analysis_utils as au

local_configuration = {
    # Scan parameters
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,
    'maskfile': None,
    'use_good_pixels_diff': False,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 800,
    'VCAL_HIGH_stop': 1000,
    'VCAL_HIGH_step': 100
}


class HitorCalibration(ScanBase):
    scan_id = "hitor_calibration"

    def configure(self, **kwargs):
        self.chip.set_dacs(**kwargs)
        self.load_hitbus_mask(**kwargs)
        self.enable_mask_copy = self.chip.enable_mask.copy()

        # Start CMD LOOP PULSER, 160 MHz
        self.bdaq['pulser_cmd_start_loop'].set_en(True)
        self.bdaq['pulser_cmd_start_loop'].set_width(400)
        self.bdaq['pulser_cmd_start_loop'].set_delay(80)
        self.bdaq['pulser_cmd_start_loop'].set_repeat(1)

        # configure all four TDC modules
        for i in range(4):
            self.bdaq['tdc_hitor%i' % i].EN_WRITE_TIMESTAMP = 1  # Writing trigger timestamp
            self.bdaq['tdc_hitor%i' % i].EN_TRIGGER_DIST = 1  # Measuring trigger to TDC delay with 640MHz clock
            self.bdaq['tdc_hitor%i' % i].EN_NO_WRITE_TRIG_ERR = 1  # Writing TDC word only if valid trigger occurred
            self.bdaq['tdc_hitor%i' % i].EN_INVERT_TDC = 0  # Inverting TDC input
            self.bdaq['tdc_hitor%i' % i].EN_INVERT_TRIGGER = 0  # Inverting trigger input
            self.bdaq['tdc_hitor%i' % i].ENABLE = 1  # enable TDC

    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, n_injections=100,
             VCAL_MED=500, VCAL_HIGH_start=800, VCAL_HIGH_stop=4000, VCAL_HIGH_step=200, **_):
        '''
        Calibrate hitor main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        n_injections : int
            Number of injections.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            First VCAL_HIGH value to scan.
        VCAL_HIGH_stop : int
            VCAL_HIGH value to stop the scan. This value is excluded from the scan.
        VCAL_HIGH_step : int
            VCAL_HIGH interval.
        '''

        values = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)
        self.logger.info('Starting scan...')
        # Deactivate whole pixel matrix.
        # FIXME: Better do this in configure step. But then plotting does not properly work since it uses the enable mask.
        self.chip.enable_mask[:] = False
        self.chip.hitbus_mask[:] = False
        self.chip.injection_mask[:] = False
        self.chip.write_masks()

        cols = range(start_column, stop_column, 4)
        rows = range(start_row, stop_row)

        pbar = tqdm(total=len(cols) * len(rows) * len(values))
        for scan_param_id, value in enumerate(values):
            self.chip.setup_analog_injection(vcal_high=value, vcal_med=VCAL_MED)
            self.store_scan_par_values(scan_param_id=scan_param_id, vcal_high=value, vcal_med=VCAL_MED)
            with self.readout(scan_param_id=scan_param_id):
                for col in cols:
                    # Selects 4 pixels on different hitbus lines
                    for p in range(4):
                        self.chip.enable_mask[col + p, rows[0] + 2 * p] = True
                        self.chip.hitbus_mask[col + p, rows[0] + 2 * p] = True
                        self.chip.injection_mask[col + p, rows[0] + 2 * p] = True
                        init_enable_mask = self.chip.enable_mask.copy()
                    # Loop the rows along the 4 columns
                    for row_roll in range(192):
                        self.chip.enable_mask = np.roll(init_enable_mask, shift=row_roll, axis=1)
                        # Restrict to selection
                        self.chip.enable_mask[:start_column] = False
                        self.chip.enable_mask[stop_column:] = False
                        self.chip.enable_mask[:, :start_row] = False
                        self.chip.enable_mask[:, stop_row:] = False
                        self.chip.enable_mask = np.logical_and(self.chip.enable_mask, self.enable_mask_copy)
                        if not np.any(self.chip.enable_mask):  # Nothing to do, skip to save time
                            continue
                        self.chip.hitbus_mask = self.chip.enable_mask
                        self.chip.injection_mask = self.chip.enable_mask
                        self.chip.write_masks(columns=range(col, col + 4))
                        self.chip.inject_analog_single(repetitions=n_injections, send_ecr=True if col + 4 < 128 else False, wait_cycles=400)
                        pbar.update(1)
                    # Deselects 4 pixels on different hitbus lines
                    self.chip.enable_mask[:] = False
                    self.chip.hitbus_mask[:] = False
                    self.chip.injection_mask[:] = False
                    self.chip.write_masks(columns=range(col, col + 4))

        for i in range(4):
            self.bdaq['tdc_hitor%i' % i].ENABLE = 0  # disable TDC
        pbar.close()
        self.logger.info('Scan finished')

    def analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', store_hits=True, cluster_hits=False, analyze_tdc=True) as a:
            a.analyze_data()
            max_scan_param_id = a.get_scan_param_values(scan_parameter='vcal_high').shape[0]
            chunk_size = a.chunk_size
            tot_max = 16
            tdc_max = 500

        # Create col, row, tot histograms from hits
        with tb.open_file(self.output_filename + '_interpreted.h5', "r+") as io_file:
            hist_tot_mean = np.zeros(shape=(400, 192, max_scan_param_id), dtype=np.float32)
            hist_tdc_mean = np.zeros(shape=(400, 192, max_scan_param_id), dtype=np.float32)
            hist_tot_std = np.zeros(shape=(400, 192, max_scan_param_id), dtype=np.float32)
            hist_tdc_std = np.zeros(shape=(400, 192, max_scan_param_id), dtype=np.float32)

            bin_positions_tot = np.tile(np.arange(tot_max), (400, 192)).reshape(400, 192, tot_max)
            bin_positions_tdc = np.tile(np.arange(tdc_max), (400, 192)).reshape(400, 192, tdc_max)
            hist_2d_tdc_vcal = np.zeros(shape=(max_scan_param_id, tdc_max), dtype=np.float32)

            # Loop over all words in the actual raw data file in chunks
            self.logger.info('Creating Histograms...')
            for par, hits in tqdm(au.hits_of_parameter(hits=io_file.root.Hits, chunk_size=chunk_size), total=max_scan_param_id):
                # Select only good tdc values
                selection = np.logical_and(hits["tdc_status"] == 1, hits["tdc_value"] < tdc_max)
                hits = hits[selection]
                tdc_value = hits['tdc_value']
                tot_value = hits["tot"]
                scan_param_id = hits['scan_param_id']
                col = hits["col"]
                row = hits["row"]
                # Histogram for each pixel TOT and TDC
                hist_tot = au.hist_3d_index(col, row, tot_value, shape=(400, 192, tot_max))
                hist_tdc = au.hist_3d_index(col, row, tdc_value, shape=(400, 192, tdc_max))
                # Save mean and std of TOT and TDC for each pixel per scan parameter
                hist_tot_mean[:, :, par] = au.get_mean_from_histogram(hist_tot, bin_positions=np.arange(tot_max), axis=2)
                hist_tdc_mean[:, :, par] = au.get_mean_from_histogram(hist_tdc, bin_positions=np.arange(tdc_max), axis=2)
                hist_tot_std[:, :, par] = au.get_std_from_histogram(hist_tot, bin_positions_tot, axis=2)
                hist_tdc_std[:, :, par] = au.get_std_from_histogram(hist_tdc, bin_positions_tdc, axis=2)

                # Histogram delta vcal values and tdc value
                x_edges = range(0, max_scan_param_id + 1)
                y_edges = range(0, tdc_max + 1)
                hist, _, _ = np.histogram2d(x=scan_param_id,
                                            y=tdc_value,
                                            bins=(x_edges, y_edges))
                hist_2d_tdc_vcal += hist

            # Store histograms
            io_file.create_carray(io_file.root,
                                  name='hist_2d_tdc_vcal',
                                  title='2D Hist TDC vs DeltaVCAL',
                                  obj=hist_2d_tdc_vcal,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))

            io_file.create_carray(io_file.root,
                                  name='hist_tot_mean',
                                  title='Mean tot calibration histogram',
                                  obj=hist_tot_mean,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))

            io_file.create_carray(io_file.root,
                                  name='hist_tdc_mean',
                                  title='Mean Tdc calibration histogram',
                                  obj=hist_tdc_mean,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))
            io_file.create_carray(io_file.root,
                                  name='hist_tot_std',
                                  title='Std tot calibration histogram',
                                  obj=hist_tot_std,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))

            io_file.create_carray(io_file.root,
                                  name='hist_tdc_std',
                                  title='Std Tdc calibration histogram',
                                  obj=hist_tdc_std,
                                  filters=tb.Filters(complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False))

        with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
            p.create_standard_plots()


if __name__ == "__main__":
    scan = HitorCalibration()
    scan.start(**local_configuration)
    scan.close()
