#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    External trigger scan with RD53A.

    For use with TLU (use RJ45) or RD53A HitOR (BDAQ self-trigger). For BDAQ self-trigger mode is also provided an extra scan script (source_scan.py)

    Note:
    Make sure that `TLU_TRIGGER_MAX_CLOCK_CYCLES` in bdaq_core.v is set correctly, number of bits used for TLU word
    should correspond to `TLU_TRIGGER_MAX_CLOCK_CYCLES - 1`.
'''

import time
import numpy as np

from tqdm import tqdm
from threading import Timer

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis import plotting


local_configuration = {
    # Scan parameters
    'start_column': 128,  # start column for mask
    'stop_column': 264,  # stop column for mask
    'start_row': 0,  # start row for mask
    'stop_row': 192,  # stop row for mask
    'maskfile': 'auto',
    'scan_timeout': 10,  # timeout for scan after which the scan will be stopped, in seconds; if False no limit on scan time
    'max_triggers': False,  # number of maximum received triggers after stopping readout, if False no limit on received trigger

    'trigger_latency': 100,  # latency of trigger in units of 25 ns (BCs)
    'trigger_delay': 57,  # trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,  # length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 500,  # length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Should be adjusted for longer trigger length.

    # Trigger configuration
    'TRIGGER': {
        'TRIGGER_MODE': 3,  # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_LOW_TIMEOUT': 0,  # Maximum wait cycles for TLU trigger low.
        'TRIGGER_SELECT': 0,  # Selecting trigger input: RX1 (multi purpose) (4), RX0 (TDC loop-trough) (2), HitOR (TDC loop-trough) (1), disabled (0)
        'TRIGGER_INVERT': 0,  # Inverting trigger input: RX1 (multi purpose) (4), RX0 (TDC loop-trough) (2), HitOR (TDC loop-trough) (1), disabled (0)
        'TRIGGER_VETO_SELECT': 0,  # Selecting trigger veto: AZ VETO (2), RX FIFO full (1), disabled (0)
        'TRIGGER_HANDSHAKE_ACCEPT_WAIT_CYCLES': 5,  # TLU trigger minimum length in TLU clock cycles
        'DATA_FORMAT': 0,  # Select trigger data format: only trigger number (0), only time stamp (1), combined, 15 bit time stamp + 16 bit trigger number (2)
        'EN_TLU_VETO': 0,  # Assert TLU veto when external veto. Activate this in order to VETO triggers if SYNC FE is enbaled.
        'TRIGGER_DATA_DELAY': 15,  # Depends on the cable length and should be adjusted (run scan/tune_tlu.py in bdaq53 repository)
        'TRIGGER_COUNTER': 0
    }
}


class ExtTriggerScan(ScanBase):
    scan_id = "ext_trigger_scan"

    def configure(self, use_logo_mask=False, **kwargs):
        self.logger.info('Configuring TLU module...')
        self.bdaq.configure_tlu_module(**kwargs)    # configure tlu module using trigger configuration
        self.bdaq.configure_trigger_cmd_pulse(**kwargs)    # configure trigger command pulse
        self.bdaq.configure_tlu_veto_pulse(**kwargs)    # configure veto pulse
        self.chip.registers['LATENCY_CONFIG'].write(kwargs['trigger_latency'])    # set trigger latency
        self.chip.registers['GP_LVDS_ROUTE'].write(0)
        self.n_trigger = 0    # Count trigger words in rawdata stream

        if use_logo_mask:
            from pylab import imread
            img = np.invert(imread('../logo.bmp').astype('bool'))
            img_mask = np.zeros((400, 192), dtype=bool)

            img_mask[105:295, 1:191] = np.transpose(img[:, :, 0])
            img_mask[:, 0] = False
            img_mask[:, -1] = False
            img_mask[0:105, :] = False
            img_mask[295:400, :] = False

            self.chip.hitbus_mask = img_mask
            self.chip.write_masks()

        if any(x in range(0, 128) for x in range(kwargs.get('start_column'), kwargs.get('stop_column'))):
            self.logger.info('SYNC enabled: Enabling auto-zeroing')
            self.chip.az_setup(delay=80, repeat=0, width=6, synch=6, **kwargs)
            self.chip.az_start()

    def scan(self, **kwargs):
        '''
        ExtTriggerScan scan main loop
        '''

        self.logger.info('Starting scan...')
        if kwargs['scan_timeout']:
            pbar = tqdm(total=int(kwargs['scan_timeout']), unit='')  # [s]
        else:
            pbar = tqdm(total=kwargs['max_triggers'], unit=' Triggers')
        wait = 1  # wait units for updating progressbar in seconds

        with self.readout(**kwargs):
            self.stop_scan = False
            # Scan loop
            while not self.stop_scan:
                try:
                    triggers = self.bdaq.get_trigger_counter()
                    # read tlu error counters
                    trig_low_timeout_errors, trig_accept_errors = self.bdaq.get_tlu_erros()
                    if trig_low_timeout_errors != 0 or trig_accept_errors != 0:
                        self.logger.warning('TLU errors detected! TRIGGER_LOW_TIMEOUT_ERROR_COUNTER: % i, TLU_TRIGGER_ACCEPT_ERROR_COUNTER: %i' % (
                                            trig_low_timeout_errors, trig_accept_errors))
                    time.sleep(wait)
                    try:
                        if kwargs['scan_timeout']:
                            pbar.update(wait)
                        else:
                            pbar.update(self.bdaq.get_trigger_counter() - triggers)
                    except ValueError:
                        pass
                    if kwargs['max_triggers'] and triggers >= kwargs['max_triggers']:
                        self.stop_scan = True
                        self.logger.info('Trigger limit was reached: %i' % kwargs['max_triggers'])
                except KeyboardInterrupt:  # react on keyboard interupt
                    self.logger.info('Scan was stopped due to keyboard interrupt')
                    self.stop_scan = True

        pbar.close()

        self.chip.az_stop()

        self.logger.info('Total amount of triggers accepted: %d', self.bdaq.get_trigger_counter())
        self.logger.info('Total amount of triggers collected: %d', self.n_trigger)
        self.logger.success('Scan finished')

    def analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
            a.analyze_data()

        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()

    def handle_data(self, data_tuple):
        ''' Check recorded trigger number '''
        super(ExtTriggerScan, self).handle_data(data_tuple)

        raw_data = data_tuple[0]
        sel = np.bitwise_and(raw_data, au.TRIGGER_ID) > 0
        trigger_words = raw_data[sel]
        trigger_number = np.bitwise_and(trigger_words, au.TRG_MASK)
        trigger_inc = np.diff(trigger_number)
        trigger_issues = np.logical_and(trigger_inc != 1, trigger_inc != -2**31)

        self.n_trigger += trigger_number.shape[0]

        if np.any(trigger_issues):
            self.logger.warning('Trigger numbers not strictly increasing')
            if np.count_nonzero(trigger_issues > 0) <= 10:
                self.logger.warning('Trigger delta(s): %s', str(trigger_inc[trigger_issues > 0]))
            else:
                self.logger.warning('More than 10 trigger numbers not strictly increasing in this readout!')

    def start_readout(self, *args, **kwargs):
        super(ExtTriggerScan, self).start_readout(*args, **kwargs)

        self.scan_timeout = kwargs.pop('scan_timeout', None)

        def timeout():
            try:
                self.progressbar.finish()
            except AttributeError:
                pass
            self.stop_scan = True
            self.logger.info('Scan timeout was reached')

        if self.scan_timeout:
            self.scan_timeout_timer = Timer(self.scan_timeout, timeout)
            if self.scan_timeout:
                self.scan_timeout_timer.start()

        self.bdaq['cmd'].set_ext_trigger(True)    # enable external trigger
        self.bdaq.set_tlu_module(True)    # enable TLU module

    def stop_readout(self, timeout=10.0):
        # disable TLU module
        self.bdaq.set_tlu_module(False)
        # disable external trigger
        self.bdaq['cmd'].set_ext_trigger(False)

        if self.scan_timeout:
            self.scan_timeout_timer.cancel()
        self.fifo_readout.stop(timeout=timeout)


if __name__ == "__main__":
    try:
        scan = ExtTriggerScan()
        scan.start(**local_configuration)
        scan.notify('bdaq53 external trigger scan has finished!')
        scan.close()
    except Exception as e:
        scan.notify('ERROR: bdaq53 external trigger scan has failed!')
