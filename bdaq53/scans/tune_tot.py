#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#
'''
    This script tunes ToT to a defined target value (with specified delta to target value) using binary search algorithm.
    Tuning ToT for all FEs in parallel is supported.
'''

from tqdm import tqdm
import numpy as np

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting
from bdaq53.analysis import online as oa
import bdaq53.analysis.analysis_utils as au

local_configuration = {
    # Scan parameters
    'start_column': 0,
    'stop_column': 128,
    'start_row': 0,
    'stop_row': 192,
    'maskfile': None,
    'mask_diff': False,

    'target_tot': {'SYNC': 5.3, 'LIN': 5.3, 'DIFF': 5.3},  # target value for each FE to which ToT is tuned
    'delta_tot': 0.2,  # max. difference between target ToT and tuned ToT value for each FE
    'lower_limit': {'SYNC': 20, 'LIN': 10, 'DIFF': 40},  # lower bound for the feedback register value for each FE
    'upper_limit': {'SYNC': 50, 'LIN': 40, 'DIFF': 70},  # upper bound for the feedback register value for each FE
    'max_iterations': 10,  # max. number of iterations after which tuning is aborted

    # DAC settings
    'VCAL_MED': 500,
    'VCAL_HIGH': 1100  # charge of 6000 electrons
}


class TuneTot(ScanBase):
    scan_id = "tot_tuning"

    def configure(self, VCAL_MED=1000, VCAL_HIGH=4000, **_):
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED)
        self.tot_hist = oa.TotHistogramming()

    def take_data(self):
        for mask in self.mask_data:
            self.chip.write_command(mask['command'])
            if mask['flavor'] == 0:
                self.chip.write_global_pulse(width=9, write=True)
                self.chip.write_sync_01(write=False) * 80
                self.chip.inject_analog_single(send_ecr=True, repetitions=self.n_injections)
            else:
                self.chip.inject_analog_single(repetitions=self.n_injections)
            self.pbar.update(1)

    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, mask_step=192 + 24, n_injections=100, target_tot=5.3, delta_tot=0.2, lower_limit=10, upper_limit=40, max_iterations=10, **_):
        '''
        ToT tuning main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        mask_step : int
            Row-stepsize of the injection mask. Every mask_stepth row is injected at once.
        n_injections : int
            Number of injections.
        target_tot : float
            Target value to which ToT is tuned.
        delta_tot : float
            Maximum difference between target ToT and tuned ToT value.
        lower_limit : int
            Lower limit for the feeback current register.
        upper_limit : int
            Upper limit for the feeback current register.
        max_iterations : int
            Maximum number of iterations after which tuning is aborted.

        '''

        self.logger.info('Preparing injection masks...')
        self.mask_data = self.prepare_injection_masks(start_column, stop_column, start_row, stop_row, mask_step)
        self.start_data_taking = True
        self.n_injections = n_injections
        n_iterations = 0

        feedback_current, fb_curr_registers, actual_tot_mean, scan_par_values = {}, {}, {}, {}

        # Determine active FEs and feedback registers which need to be adjusted
        active_FEs = []
        if len(set(range(0, 128)).intersection(set(range(start_column, stop_column)))) > 0:
            active_FEs.append('SYNC')
            fb_curr_registers.update({'SYNC': 'IBIAS_KRUM_SYNC'})
        if len(set(range(128, 264)).intersection(set(range(start_column, stop_column)))) > 0:
            active_FEs.append('LIN')
            fb_curr_registers.update({'LIN': 'KRUM_CURR_LIN'})
        if len(set(range(264, 400)).intersection(set(range(start_column, stop_column)))) > 0:
            active_FEs.append('DIFF')
            fb_curr_registers.update({'DIFF': 'VFF_DIFF'})

        self.logger.info('Starting scan...')
        self.logger.info('Tune ToT for: ' + ', '.join(active_FEs))
        self.pbar = tqdm(total=len(self.mask_data) * max_iterations, unit=' Mask steps')
        with self.readout(callback=self.analyze_data_online):
            # Binary search loop until max iteration is reached or all FEs were tuned
            while n_iterations < max_iterations and len(active_FEs) > 0:
                n_iterations += 1
                for active_FE in active_FEs:
                    actual_fb_curr_register = fb_curr_registers[active_FE]
                    feedback_current.update({active_FE: int(np.floor((lower_limit[active_FE] + upper_limit[active_FE]) / 2.0))})
                    self.logger.info('Setting %s = %i (iteration %i)' % (actual_fb_curr_register, feedback_current[active_FE], n_iterations))
                    # Write feedback current
                    self.chip.write_register(register=actual_fb_curr_register, data=feedback_current[active_FE], write=True)
                    scan_par_values.update({actual_fb_curr_register: feedback_current[active_FE]})

                # Store scan parameter values
                self.store_scan_par_values(scan_param_id=n_iterations, **scan_par_values)
                # Take data
                self.take_data()

                # Get mean of ToT from online analysis
                for active_FE in active_FEs:
                    tot = self.tot_hist.get()
                    actual_fb_curr_register = fb_curr_registers[active_FE]
                    hist_tot = tot[self.chip.column_range[active_FE][0]:self.chip.column_range[active_FE][1], :, :].sum(axis=(0, 1))
                    actual_tot_mean.update({active_FE: au.get_mean_from_histogram(hist_tot, np.arange(hist_tot.shape[0]))})
                    self.logger.info('Mean ToT at %s = %i is %.2f' % (actual_fb_curr_register, feedback_current[active_FE], actual_tot_mean[active_FE]))

                    # Update upper and lower limit based on mean ToT
                    if np.sqrt((actual_tot_mean[active_FE] - target_tot[active_FE])**2) < delta_tot:  # Break if difference to target is smaller then delta
                        self.logger.success('ToT tuning was succesful! Mean ToT is %.2f (target was %.2f) at %s = %i' % (actual_tot_mean[active_FE], target_tot[active_FE], actual_fb_curr_register, feedback_current[active_FE]))
                        # Remove succesfuly tuned FE from list
                        active_FEs.remove(active_FE)
                        break
                    else:  # Update limits
                        if target_tot[active_FE] < actual_tot_mean[active_FE]:
                            lower_limit.update({active_FE: feedback_current[active_FE] + 1})
                        else:
                            upper_limit.update({active_FE: feedback_current[active_FE] + 1})

        # Max iteration is reached. Check if still have untuned ToT for activated FEs.
        if len(active_FEs) > 0:
            self.logger.warning('Number of maximum iteration is reached. Could not tune ToT for: ' + ', '.join(active_FEs))

        self.start_data_taking = False
        self.pbar.close()
        self.tot_hist.stop.set()  # stop analysis process
        self.logger.success('Scan finished')

    # Used to overwrite data storing function: self.readout.handle_data
    def analyze_data_online(self, data_tuple):
        raw_data = data_tuple[0]
        self.tot_hist.add(raw_data)
        # Only store ToT histograms when scan started
        if self.start_data_taking:
            super(TuneTot, self).handle_data(data_tuple)

    def analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
            a.analyze_data()

        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()


if __name__ == "__main__":
    scan = TuneTot()
    scan.start(**local_configuration)
    scan.close()
