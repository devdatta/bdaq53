#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#


'''
    This test checks continuously for SEU induces bitflips
'''

import time
import tables as tb

from bdaq53.scan_base import ScanBase
from bdaq53.register_utils import RD53ARegisterParser
from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis.plotting import Plotting


local_configuration = {
    'scan_timeout': False,
    'scan_interval': 10,
    'correct_errors': False,

    'addresses': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                  11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                  21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                  31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
                  41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
                  51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
                  61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
                  71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
                  81, 82, 83, 84, 85, 86, 87, 88, 89, 90,
                  91, 92, 93, 94, 95, 96, 97, 98, 99, 100,
                  101, 102, 103, 104, 105, 106, 107, 108, 109, 110,
                  111, 112, 113, 114, 115, 116, 117, 119, 120,

                  131, 132, 133, 134, 135]
}


class DataTable(tb.IsDescription):
    timestamp = tb.Int64Col(pos=0)
    errors = tb.Int64Col(pos=1)
    integrated_errors = tb.Int64Col(pos=2)


class SEUTest(ScanBase):
    scan_id = "seu_test"

    def configure(self, **kwargs):
        self.bdaq.set_monitor_filter(True)
        self.chip.enable_monitor_data()

    def compare(self, register_data):
        self.logger.info('Comparing data...')
        errors = 0
        for address, value in self.reference_data.items():
            try:
                if register_data[address] != value:
                    errors += 1
                    self.logger.error('Register at address %i has value %s, should be %s' % (address, bin(value), bin(register_data[address])))
            except KeyError:
                errors += 1
                self.logger.error('Register data is missing! Address %i was not recorded in this iteration.' % address)

        self.results.append((time.time(), errors))

        if errors == 0:
            self.logger.success('No errors detected in this iteration.')
        else:
            self.logger.error('%i errors detected in this iteration.' % errors)

    def scan(self, scan_timeout=300, scan_interval=30, correct_errors=False, addresses=[1], **kwargs):
        '''
        SEU test main loop
        '''

        self.rp = RD53ARegisterParser()
        self.addresses = addresses

        self.results = []

        self.logger.info('Reading reference data...')
        self.reference_data = {}
        with self.readout(scan_param_id=0):
            for address in self.addresses:
                self.chip._read_register(register=address)

        raw_data = list(au.words_of_parameter(self.h5_file.root.raw_data[:], self.h5_file.root.meta_data[:]))[0][1]
        for reg in au.process_userk(au.interpret_userk_data(raw_data)):
            self.reference_data.update({reg[0]: reg[2]})
        self.results.append((time.time(), 0, 0))

        self.logger.info('Starting scan...')
        start_timestamp = time.time()
        stop_scan = False
        scan_param_id = 0
        delay = 0
        while not stop_scan:
            try:
                time.sleep(scan_interval - delay)
                self.logger.info('Reading selected registers...')
                scan_param_id += 1
                this_scan_data = {}
                with self.readout(scan_param_id=scan_param_id):
                    for address in self.addresses:
                        self.chip._read_register(register=address)

                raw_data = list(au.words_of_parameter(self.h5_file.root.raw_data[:], self.h5_file.root.meta_data[:]))[scan_param_id][1]
                for reg in au.process_userk(au.interpret_userk_data(raw_data)):
                    this_scan_data.update({reg[0]: reg[2]})

                self.compare(this_scan_data)
                if correct_errors:
                    for reg, value in self.reference_data.values():
                        self.chip.registers[reg].write(value)
                        self.chip.write_command(self.chip.write_sync(write=False) * 10)
                else:
                    self.reference_data = this_scan_data

                if scan_timeout and time.time() > start_timestamp + scan_timeout:
                    stop_scan = True
                    self.logger.info('Run timeout has been reached.')
            except KeyboardInterrupt:
                stop_scan = True
            if not stop_scan:
                self.logger.info('Continuing scan...')
            if scan_param_id == 1:
                delay = time.time() - start_timestamp - scan_interval

        data_table = self.h5_file.create_table(self.h5_file.root, name='seu_data', title='SEU data', description=DataTable)
        for iteration in self.results:
            row = data_table.row
            row['timestamp'] = iteration[0]
            row['errors'] = iteration[1]
            row.append()
        self.logger.success('Scan finished')

    def analyze(self):
        self.logger.info('Analyzing data...')
        au.copy_configuration_node(self.output_filename + '.h5', self.output_filename + '_interpreted.h5')
        with tb.open_file(self.output_filename + '.h5', 'r') as in_file:
            results = in_file.root.seu_data[:]
            with tb.open_file(self.output_filename + '_interpreted.h5', 'a') as out_file:
                in_file.copy_node(in_file.root.seu_data, out_file.root)

        timestamps, errors, integrated_errors = [], [], []

        for iteration in results:
            timestamps.append(iteration[0])
            errors.append(iteration[1])
            try:
                integrated_errors.append(integrated_errors[-1] + iteration[1])
            except IndexError:
                integrated_errors.append(0)

        with Plotting(analyzed_data_file=self.output_filename + '_interpreted.h5') as p:
            p.logger.info('Creating selected plots...')
            p.create_parameter_page()
            p._plot_1d_vs_time([timestamps, errors], title='Register Errors', y_axis_title='# of errors')
            p._plot_1d_vs_time([timestamps, integrated_errors], title='Integrated Register Errors', y_axis_title='# of errors')


if __name__ == "__main__":
    test = SEUTest()
    test.start(**local_configuration)
    test.close()
