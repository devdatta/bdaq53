#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#


'''
    This meta script performs a threshold scan for every parameter in a range defined py 'parameter_start', 'parameter'stop'
    and 'parameter_step'. In the end, the mean threshold and noise are plotted as a function of the parameter.
'''

import tables as tb

from bdaq53.meta_scan_base import MetaScanBase
from bdaq53.scans.scan_threshold import ThresholdScan
from bdaq53.analysis.plotting import Plotting


local_configuration = {
    # Scan parameters
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,
    'maskfile': 'auto',

    'parameter_name': 'LDAC_LIN',
    'parameter_start': 100,
    'parameter_stop': 150,
    'parameter_step': 10,

    # Threshold scan parameters
    'VCAL_MED': 500,
    'VCAL_HIGH_start': 600,
    'VCAL_HIGH_stop': 1000,
    'VCAL_HIGH_step': 10
}


class DataTable(tb.IsDescription):
    parameter = tb.UInt32Col(pos=0)
    threshold = tb.Float64Col(pos=1)
    noise = tb.Float64Col(pos=2)


class MetaParamThrScn(MetaScanBase):
    scan_id = 'meta_threshold_scan_parametric'

    def scan(self, parameter_name='LDAC_LIN', parameter_start=50, parameter_stop=180, parameter_step=1, **kwargs):
        self.param_range = range(parameter_start, parameter_stop, parameter_step)
        data_table = self.h5_file.create_table(self.h5_file.root, name='data', title='Data', description=DataTable)

        for param in self.param_range:
            kwargs[parameter_name] = param
            row = data_table.row

            self.logger.info('Performing ThresholdScan for %s = %i' % (parameter_name, param))
            th = ThresholdScan()
            self.scans.append(th)
            th.logger.addHandler(self.fh)
            th.start(**kwargs)

            mean_thr, mean_noise = th.analyze()
            row['parameter'] = param
            row['threshold'] = mean_thr
            row['noise'] = mean_noise
            row.append()
            data_table.flush()

            th.close()

    def analyze(self):
        with tb.open_file(self.output_filename + '.h5', mode='r', title=self.scan_id) as self.h5_file:
            with tb.open_file(self.output_filename + '_interpreted.h5', mode='a', title=self.scan_id) as self.interpreted_data_file:
                self.h5_file.copy_node(self.h5_file.root.data, self.interpreted_data_file.root, recursive=True)

        with Plotting(analyzed_data_file=self.output_filename + '_interpreted.h5') as p:
            p.create_standard_plots()


if __name__ == "__main__":
    mpt = MetaParamThrScn()
    mpt.start(**local_configuration)
    mpt.analyze()
