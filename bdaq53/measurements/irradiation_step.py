#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Irradiation measurement script to be performed at every dose step
'''

import os
import time
import yaml
import logging
import copy
import numpy as np

from basil.dut import Dut
from bdaq53.rd53a import RD53A
from bdaq53.bdaq53 import BDAQ53
from bdaq53.analysis import analysis_utils as au
from bdaq53.scans.scan_threshold import ThresholdScan
from bdaq53.scans.meta_tune_local_threshold import MetaTDACTuning


step = '0MRAD'

output_dir = ''
initial_maskfile_LIN = ''
initial_maskfile_DIFF = ''


configuration_untuned_coarse_threshold_scan = {
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 4000,
    'VCAL_HIGH_step': 100,
    'maskfile': None,
    'use_good_pixels_diff': False
}

configuration_untuned_fine_threshold_scan = {
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 800,
    'VCAL_HIGH_step': 10,
    'maskfile': None,
    'use_good_pixels_diff': False
}

configuration_initial_tuning_lin_threshold_scan = {
    'start_column': 128,
    'stop_column': 264,
    'VCAL_HIGH_start': 550,
    'VCAL_HIGH_stop': 850,
    'VCAL_HIGH_step': 10,
    'maskfile': initial_maskfile_LIN,
    'use_good_pixels_diff': False
}

configuration_initial_tuning_diff_threshold_scan = {
    'start_column': 264,
    'stop_column': 400,
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 800,
    'VCAL_HIGH_step': 10,
    'maskfile': initial_maskfile_DIFF,
    'use_good_pixels_diff': False
}

configuration_tuning_lin = {
    'start_column': 128,
    'stop_column': 264,
    'maskfile': None,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 800,
    'VCAL_HIGH_step': 10
}

configuration_tuned_lin_threshold_scan = {
    'start_column': 128,
    'stop_column': 264,
    'VCAL_HIGH_start': 550,
    'VCAL_HIGH_stop': 850,
    'VCAL_HIGH_step': 10,
    'maskfile': 'auto',
    'use_good_pixels_diff': False
}

configuration_tuning_diff = {
    'start_column': 264,
    'stop_column': 400,
    'maskfile': None,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 800,
    'VCAL_HIGH_step': 10
}

configuration_tuned_diff_threshold_scan = {
    'start_column': 264,
    'stop_column': 400,
    'VCAL_HIGH_start': 500,
    'VCAL_HIGH_stop': 800,
    'VCAL_HIGH_step': 10,
    'maskfile': 'auto',
    'use_good_pixels_diff': False
}


def append_data(scan, data):
    timestamp = time.time()

    try:
        with open(out_file, 'r') as of:
            old_data = yaml.load(of) or {}
    except IOError:
        old_data = {}

    if scan not in old_data.keys():
        old_data[scan] = {}

    with open(out_file, 'w') as of:
        old_data[scan][timestamp] = {}
        for key in data.keys():
            old_data[scan][timestamp][key] = data[key]
        yaml.dump(old_data, of)


if __name__ == "__main__":
    try:
        out_dir = os.path.join(output_dir, step)

        with open('../testbench.yaml', 'r') as tb:
            config = yaml.load(tb)

        config['output_directory'] = out_dir

        with open('../testbench.yaml', 'w') as tb:
            yaml.dump(config, tb)

        logger = logging.getLogger('Irradiation')
        logger.success = lambda msg, *args, **kwargs: logger.log(logging.SUCCESS, msg, *args, **kwargs)

        if os.path.isdir(out_dir):
            logger.error('Output directory already exists!')
            raise
        else:
            os.mkdir(out_dir)

        logfile = os.path.join(out_dir, 'irradiation.log')
        fh = logging.FileHandler(logfile)
        fh.setLevel(logging.INFO)
        fh.setFormatter(logging.Formatter("%(asctime)s - [%(name)-15s] - %(levelname)-7s %(message)s"))
        logger.addHandler(fh)

        dut = Dut('irradiation.yaml')
        dut.init()

        out_file = os.path.join(out_dir, 'irradiation.dat')

        logger.info('Starting new irradiation step at timestamp %1.6f...' % (time.time()))

        try:    # Coarse untuned threshold scan
            logger.info('Start coarse untuned threshold scan...')
            scan = ThresholdScan()
            mean_thr, mean_noise = scan.start(**configuration_untuned_coarse_threshold_scan)
            scan.close()

            append_data('threshold_scan_untuned_coarse', {'threshold': float(mean_thr), 'noise': float(mean_noise)})

            logger.success('Coarse untuned threshold scan finished! Mean threshold is %i, mean noise is %i.' % (mean_thr, mean_noise))
        except Exception as e:
            scan.close()
            logger.exception('Exception during coarse untuned threshold scan: %s' % e)

        try:    # Fine untuned threshold scan
            logger.info('Start fine untuned threshold scan...')
            scan = ThresholdScan()
            mean_thr, mean_noise = scan.start(**configuration_untuned_fine_threshold_scan)
            scan.close()

            append_data('threshold_scan_untuned_fine', {'threshold': float(mean_thr), 'noise': float(mean_noise)})

            logger.success('Fine untuned threshold scan finished! Mean threshold is %i, mean noise is %i.' % (mean_thr, mean_noise))
        except Exception as e:
            scan.close()
            logger.exception('Exception during fine untuned threshold scan: %s' % e)

        try:    # Initial tuning threshold scan for LIN
            logger.info('Start initial tuning threshold scan for LIN...')
            scan = ThresholdScan()
            mean_thr, mean_noise = scan.start(**configuration_initial_tuning_lin_threshold_scan)
            scan.close()

            append_data('threshold_scan_initial_tuning_LIN', {'threshold': float(mean_thr), 'noise': float(mean_noise)})

            logger.success('Initial tuning threshold scan for LIN finished! Mean threshold is %i, mean noise is %i.' % (mean_thr, mean_noise))
        except Exception as e:
            scan.close()
            logger.exception('Exception during initial tuning LIN threshold scan: %s' % e)

        try:    # Initial tuning threshold scan for DIFF
            logger.info('Start initial tuning threshold scan for DIFF...')
            scan = ThresholdScan()
            mean_thr, mean_noise = scan.start(**configuration_initial_tuning_diff_threshold_scan)
            scan.close()

            append_data('threshold_scan_initial_tuning_DIFF', {'threshold': float(mean_thr), 'noise': float(mean_noise)})

            logger.success('Initial tuning threshold scan for DIFF finished! Mean threshold is %i, mean noise is %i.' % (mean_thr, mean_noise))
        except Exception as e:
            scan.close()
            logger.exception('Exception during initial tuning DIFF threshold scan: %s' % e)

        try:    # Tuning LIN
            logger.info('Start threshold tuning for LIN...')
            tuning = MetaTDACTuning()
            tuning.start(**configuration_tuning_lin)
            logger.success('LIN tuning finished!')
        except Exception as e:
            scan.close()
            logger.exception('Exception during tuning of LIN: %s' % e)

        try:    # Fine tuned threshold scan for LIN
            logger.info('Start tuned threshold scan for LIN...')
            scan = ThresholdScan()
            mean_thr, mean_noise = scan.start(**configuration_tuned_lin_threshold_scan)
            scan.close()

            append_data('threshold_scan_tuned_LIN', {'threshold': float(mean_thr), 'noise': float(mean_noise)})

            logger.success('Tuned threshold scan for LIN finished! Mean threshold is %i, mean noise is %i.' % (mean_thr, mean_noise))
        except Exception as e:
            scan.close()
            logger.exception('Exception during tuned LIN threshold scan: %s' % e)

        try:    # Tuning DIFF
            logger.info('Start threshold tuning for DIFF...')
            tuning = MetaTDACTuning()
            tuning.start(**configuration_tuning_diff)
            logger.success('DIFF tuning finished!')
        except Exception as e:
            scan.close()
            logger.exception('Exception during tuning of DIFF: %s' % e)

        try:    # Fine tuned threshold scan for LIN
            logger.info('Start tuned threshold scan for DIFF...')
            scan = ThresholdScan()
            mean_thr, mean_noise = scan.start(**configuration_tuned_diff_threshold_scan)
            scan.close()

            append_data('threshold_scan_tuned_DIFF', {'threshold': float(mean_thr), 'noise': float(mean_noise)})

            logger.success('Tuned threshold scan for DIFF finished! Mean threshold is %i, mean noise is %i.' % (mean_thr, mean_noise))
        except Exception as e:
            scan.close()
            logger.exception('Exception during tuned DIFF threshold scan: %s' % e)

        try:    # Read Ring Oscillators
            bdaq = BDAQ53()
            bdaq.init()

            chip = RD53A(bdaq)
            chip.init()
            chip.init_communication()

            T_NTC = float(chip._measure_temperature_ntc_CERNFMC())
            logger.info("Temperature with NTC is: %1.2f C", T_NTC)

            T_Sens = dut['Thermohygrometer'].get_temperature()[0]
            RH_Sens = dut['Thermohygrometer'].get_humidity()[0]
            append_data('Temperature', {'NTC': T_NTC, 'Sensirion': T_Sens, 'Sensirion_RH': RH_Sens})

            append_data('ring_oscillators', chip.get_ring_oscilators())
            bdaq.close()
        except Exception as e:
            bdaq.close()
            logger.exception('Exception during Ring Oscillator readout: %s' % e)

    except KeyboardInterrupt:
        logger.exception('Stopping irradiation loop at timestamp %1.6f...' % (time.time()))
