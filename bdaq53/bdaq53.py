#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import yaml
import logging
import os
import time
import struct
import coloredlogs
import numpy as np

from basil.dut import Dut

import pkg_resources
VERSION = pkg_resources.get_distribution("bdaq53").version

loglevel = logging.INFO

''' Set up main logger '''
for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)
logging.getLogger('basil.HL.RegisterHardwareLayer').setLevel(logging.WARNING)

logging.SUCCESS = 25  # WARNING(30) > SUCCESS(25) > INFO(20)
logging.addLevelName(logging.SUCCESS, 'SUCCESS')

coloredlogs.DEFAULT_FIELD_STYLES = {'asctime': {},
                                    'hostname': {},
                                    'levelname': {'bold': True},
                                    'name': {},
                                    'programname': {}}
coloredlogs.DEFAULT_LEVEL_STYLES = {'critical': {'color': 'red', 'bold': True},
                                    'debug': {'color': 'magenta'},
                                    'error': {'color': 'red', 'bold': True},
                                    'info': {},
                                    'success': {'color': 'green'},
                                    'warning': {'color': 'yellow'}}

coloredlogs.install(fmt="%(asctime)s - [%(name)-15s] - %(levelname)-7s %(message)s", milliseconds=True)

logger = logging.getLogger('BDAQ53')
logger.setLevel(loglevel)
logger.success = lambda msg, *args, **kwargs: logger.log(logging.SUCCESS, msg, *args, **kwargs)


class BDAQ53(Dut):
    '''
    Main class for BDAQ53 readout system
    '''

    ''' Map hardware IDs for board identification '''
    hw_map = {
        0: 'SIMULATION',
        1: 'BDAQ53',
        2: 'USBPix3',
        3: 'KC705',
        4: 'GENESYS 2'
    }

    hw_con_map = {
        0: 'SMA',
        1: 'FMC_LPC',
        2: 'FMC_HPC',
        3: 'Displayport'

    }

    ''' Options concerning the readout hardware '''
    board_options_map = {
        '640Mbps': 0x01
    }

    def __init__(self, conf=None, bench_config=None):
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        self.configuration = {}

        try:
            if bench_config is None:
                bench_config = os.path.join(self.proj_dir, 'bdaq53' + os.sep + 'testbench.yaml')
            with open(bench_config) as f:
                self.configuration['bench'] = yaml.load(f)
        except TypeError:
            self.configuration['bench'] = bench_config

        if not conf:
            conf = os.path.join(self.proj_dir, 'bdaq53' + os.sep + 'bdaq53.yaml')
        logger.debug("Loading configuration file from %s" % conf)

        super(BDAQ53, self).__init__(conf)

    def init(self, **kwargs):
        super(BDAQ53, self).init()

        self.fw_version, self.board_version, self.board_options, self.connector_version = self.get_daq_version()
        logger.success('Found board %s with %s running firmware version %s' % (self.board_version, self.connector_version, self.fw_version))

        if self.fw_version != VERSION.split('.')[0] + '.' + VERSION.split('.')[1]:  # Compare only the first two blocks
            raise Exception("Firmware version (%s) is different than software version (%s)! Please update." % (self.fw_version, VERSION))

        if self.board_version == 'BDAQ53' or self.board_version == 'USBPix3':
            if self['rx'].get_Si570_is_configured() is False:
                from bdaq53 import si570
                si570_conf = {'name': 'si570', 'type': 'bdaq53.si570', 'interface': 'intf', 'base_addr': 0xba, 'init': {'frequency': 160.0}}
                bdaq53a_clk_gen = si570.si570(self['i2c'], si570_conf)
                self['cmd'].set_output_en(False)
                self['rx'].reset()
                time.sleep(0.1)
                bdaq53a_clk_gen.init()
                time.sleep(0.1)
                self['cmd'].set_output_en(True)
                self['rx'].set_Si570_is_configured(True)
            else:
                logger.info('Si570 oscillator is already configured')
        elif self.board_version == 'KC705':
            self._kc705_setup_si5324(**kwargs)
        elif self.board_version == 'SIMULATION':
            pass

        # Configure cmd encoder
        self['cmd'].reset()
        time.sleep(0.1)
        # Wait for PLL lock
        self.wait_for_pll_lock()

        self.setup_aurora()

        self.print_powered_dp_connectors()

    def get_daq_version(self):
        ret = self['intf'].read(0x0000, 2)
        fw_version = str('%s.%s' % (ret[1], ret[0]))

        ret = self['intf'].read(0x0002, 2)
        board_version = self.hw_map[ret[0] + (ret[1] << 8)]

        ret = self['intf'].read(0x0004, 1)
        board_options = ret[0]

        ret = self['intf'].read(0x0005, 2)
        connector_version = self.hw_con_map[ret[0] + (ret[1] << 8)]

        return fw_version, board_version, board_options, connector_version

    def _kc705_set_i2c_mux(self, value):
        ''' Configure the I2C MUX and returns the base address of the selected device '''
        _i2c_mux_map = {
            'Si570': (0x01, 0x5d),
            'FMC_HPC': (0x02, 0x00),
            'FMC_LPC': (0x04, 0x00),
            'I2C_EEPROM': (0x08, 0x54),
            'SFP_MODULE': (0x10, 0x50),
            'ADV7511': (0x20, 0x39),
            'DDR3_SODIMM': (0x40, (0x50, 0x18)),
            'Si5324': (0x80, 0x68)
        }
        if value in _i2c_mux_map:
            self['i2c'].write(0xe8, [_i2c_mux_map[value][0]])
            logger.debug('I2C mux set to: %s' % value)
        else:
            logger.error('I2C mux setting invalid: %s' % value)

        base_addr = _i2c_mux_map[value][1]
        logger.debug('I2C base address: %s' % hex(base_addr))

        return base_addr

    def _kc705_setup_si5324(self, **kwargs):
        ''' Calculated register values for Si5324 have to be modified in order to work!
            N1_HS, NC1_LS, N2_HS, N2_LS, N32, BWSEL '''
        _si5324_f_map = {
            200: (7, 4, 10, 112000, 22857, 2),
            180: (7, 4, 10, 100800, 22857, 2),
            170: (5, 6, 10, 102000, 22857, 2),
            160: (8, 4, 10, 102400, 22857, 2),
            150: (5, 3, 6, 33249, 7036, 2),
            140: (9, 4, 10, 100800, 22857, 2),
            120: (11, 4, 11, 32000, 7619, 2),
            100: (9, 4, 11, 32000, 15238, 2)
        }

        frequency = kwargs.get('aurora_ref', 160)

        _si5324_base_address = self._kc705_set_i2c_mux('Si5324')

        def si5324_read(addr):
            self['i2c'].write(0xd0, [addr])
            return self['i2c'].read(0xd0, 1)[0]

        def si5324_write(addr, data):
            self['i2c'].write(0xd0, [addr, data & 0xff])

        # Based on: https://github.com/m-labs/si5324_test/blob/master/firmware/runtime/si5324.c
        self['i2c'].write(0xd0, [134])
        ident = struct.unpack(">H", bytearray(self['i2c'].read(0xd0, 2)))[0]
        if ident != 0x0182:
            raise ValueError("It is not Si5324 chip.")

        # Select XA/XB input
        si5324_write(0, si5324_read(0) | 0x40)  # Free running mode=1, CKOUT_ALWAYS_ON = 0
        si5324_write(11, 0x41)  # Disable CLKIN1
        si5324_write(6, 0x0F)  # Disable CKOUT2 (SFOUT2_REG=001), set CKOUT1 to LVDS (SFOUT1_REG=111)
        si5324_write(21, si5324_read(21) & 0xfe)  # CKSEL_PIN = 0
        si5324_write(3, 0x55)  # CKIN2 selected, SQ_ICAL=1

        if frequency in _si5324_f_map:
            register_set = _si5324_f_map[frequency]

            N1_HS = register_set[0] - 4
            NC1_LS = register_set[1] - 1
            N2_HS = register_set[2] - 4
            N2_LS = register_set[3] - 1
            N32 = register_set[4] - 1
            BWSEL = register_set[5]

            logging.debug('Si5324: Setting registers to %s' % str(register_set))
        else:
            logger.error('Si5324: No valid frequency specified: %u' % frequency)

        si5324_write(2, (si5324_read(2) & 0x0f) | (BWSEL << 4))
        si5324_write(25, N1_HS << 5)
        si5324_write(31, NC1_LS >> 16)
        si5324_write(32, NC1_LS >> 8)
        si5324_write(33, NC1_LS)
        si5324_write(40, (N2_HS << 5) | (N2_LS >> 16))
        si5324_write(41, N2_LS >> 8)
        si5324_write(42, N2_LS)
        si5324_write(46, N32 >> 16)
        si5324_write(47, N32 >> 8)
        si5324_write(48, N32)
        si5324_write(137, si5324_read(137) | 0x01)  # FASTLOCK=1
        si5324_write(136, 0x40)  # ICAL=1

        time.sleep(0.1)

        LOS1_INT = (si5324_read(129) & 0x02) == 0
        LOSX_INT = (si5324_read(129) & 0x01) == 0
        LOL_INT = (si5324_read(130) & 0x01) == 0

        logger.debug('Si5324: Has input: %d' % (LOS1_INT))
        logger.debug('Si5324: Has xtal %d:' % (LOSX_INT))
        logger.debug('Si5324: Locked: %d' % (LOL_INT))

        logger.info('Si5324: Clock set to %u MHz.' % frequency)

        if LOL_INT is False:
            logger.warning('Si5324: Not locked.')

    def _kc705_get_temperature_NTC_CERNFMC(self):
        ''' Measure the temperature of the SCC NTC using the CERN FMC card connected to KC705 board'''

        if self.board_version != 'KC705':
            raise RuntimeError('_kc705_get_temperature_NTC_CERNFMC() is only available with KC705 and CERN FMC card')

        # -----constants-------------
        # FIXME: ove to calibration object?
        ntc_adc_vdd = 2.5  # ntc_adc_vdd of the ADC
        ntc_R1 = 39000  # Resistance 1 of the voltage divider, in series with NTC in FMC-card
        ntc_R25C = 10e3  # NTC constant
        ntc_T25 = 298.15
        ntc_beta = 3435  # Beta NTC constant
        ntc_adc_lsb = 0.001  # ntc_adc_lsb value of the ADC for normal configuration. Can be changed by changing the configuration register.
        # ---------------------------

        ntc_adc_base_address = 0x90

        # Read the values of the 1 bit ADC in CERN-FMC card:
        if self.connector_version == 'FMC_HPC':
            self._kc705_set_i2c_mux('FMC_HPC')
            logger.debug('I2C mux: FMC HPC selected')
        elif self.connector_version == 'FMC_LPC':
            self._kc705_set_i2c_mux('FMC_LPC')
            logger.debug('I2C mux: FMC LPC selected')
        else:
            raise RuntimeError('_kc705_get_temperature_NTC_CERNFMC() is only available with KC705 and CERN FMC card')

        self['i2c'].write(ntc_adc_base_address, [0b00000001])  # address of ADC and write the addresss pointer register to point the configuration register(default 0x8583)
        self['i2c'].write(ntc_adc_base_address, [0b00000001, 0x85, 0x83])  # Reset the ADC to start adc_raw single conversion. with this config (which is the default) in the conversion register we will read the voltage drop between the terminals of the NTC resistor.

        self['i2c'].write(ntc_adc_base_address, [0b00000000])
        adc_raw = self['i2c'].read(ntc_adc_base_address, 2)  # read two bytes of the conversion register of adc

        logger.debug('NTC ADC raw data: %s' % (hex(adc_raw[0]) + ' ' + hex(adc_raw[1])))
        adc = (((adc_raw[0] << 8) | adc_raw[1]) >> 4)
        V_adc = ntc_adc_lsb * adc
        R_ntc = (ntc_R1 * V_adc) / (ntc_adc_vdd - V_adc)
        T_ntc = (1.0 / ((1.0 / ntc_T25) + ((1.0 / ntc_beta) * (np.log(R_ntc / ntc_R25C))))) - (ntc_T25 - 25)

        return round(T_ntc, 3)

    def get_temperature_NTC(self):
        if self.board_version == 'KC705':
            return self._kc705_get_temperature_NTC_CERNFMC()
        elif self.board_version == 'BDAQ53':
            raise NotImplementedError('NTC readout is not yet implemented with BDAQ board.')
        else:
            raise NotImplementedError('NTC readout is not not supported on this hardware platform.')

    def get_DP_SENSE(self, DP_ID):
        ''' Read back the vddd_sense lines to identify powered chips '''
        if self.board_version == 'BDAQ53':
            sense = self['DP_CONTROL'].get_data()
            logging.debug('Slow_control sense: %s' % bin(sense[0]))
            if 0 <= DP_ID < 4:
                return ((sense[0] & (1 << DP_ID)) is False)
            else:
                logger.error('Invalid DP_ID (0..3)')
        else:
            logger.warning('RD53A slow control is only available for BDAQ53 hardware')
            return False

    def set_DP_RESET(self, DP_ID, value):
        ''' Controls the POR lines. if set to 1, the POR is pulled to local chip ground via opto-coupler '''
        if self.board_version == 'BDAQ53':
            if 0 <= DP_ID < 4:
                self['DP_CONTROL'].set_data([value << (4 + DP_ID)])
            else:
                logger.error('Invalid DP_ID (0..3)')
        else:
            logger.warning('RD53A slow control is only available for BDAQ53 hardware')
            return False

    def print_powered_dp_connectors(self):
        ''' Report the VDDD_SENSE signal status for the given Displayport connectors'''
        if self.board_version == 'BDAQ53':
            for i in range(4):
                if self.get_DP_SENSE(i):
                    logger.info('VDDD_SENSE detected at Displayport ID %i' % i)

    def dp_power_on_reset(self, DP_ID):
        ''' Short reset pulse for given DP_ID '''
        self.set_DP_RESET(DP_ID, True)
        time.sleep(0.1)
        self.set_DP_RESET(DP_ID, False)

    def set_monitor_filter(self, state=True):
        self['rx'].set_USER_K_FILTER_MASK_1(0x01)  # only allow frames containing register data
        self['rx'].set_USER_K_FILTER_MASK_2(0x02)  # only allow frames containing register data
        self['rx'].set_USER_K_FILTER_MASK_3(0x04)  # only allow frames containing register data
        self['rx'].set_USER_K_FILTER_EN(state)  # set the filter
        logger.debug('USER_K filter set to %s' % state)

    def write_digilent_dac(self, value):
        ''' Writes integer upto 16 bits to the externally via PMOD connected Digilent DAC '''
        if self.board_version == 'BDAQ53':
            byts = []
            value = int(value)
            for i in range(0, 2):
                byts.append(value >> (i * 8) & 0xff)
            byts.reverse()
            self['spi_dac'].set_data(byts, addr=0)
            self['spi_dac'].start()
        else:
            logger.warning('Digilent DAC is only available for BDAQ53 hardware')
            return False

    def setup_aurora(self):
        if self.configuration['bench']['bypass_mode'] is True:
            if self.board_version == 'KC705'and self.connector_version == 'FMC_LPC' and self.board_options & self.board_options_map['640Mbps']:
                logger.info("Switching FMC card to BYPASS MODE @ 640Mb/s")
                self['cmd'].set_bypass_mode(True)
            else:
                raise NotImplementedError('Bypass mode is only supported for the KC705+FMC_LPC readout hardware @ 640Mb/s')
        elif self.board_options & self.board_options_map['640Mbps']:
            logger.info("Aurora receiver running at 640Mb/s")
            self['cmd'].set_bypass_mode(False)
        else:
            logger.info("Aurora receiver running at 1.28Gb/s")
            self['cmd'].set_bypass_mode(False)

    def wait_for_aurora_sync(self, timeout=1000):
        logger.debug("Waiting for Aurora sync...")
        times = 0

        while times < timeout and self['rx'].get_rx_ready() == 0:
            times += 1

        if self['rx'].get_rx_ready() == 1:
            logger.debug("Aurora link synchronized")
            return True
        else:
            self['cmd'].reset()
            raise RuntimeError('Timeout while waiting for Aurora Sync.')

    def wait_for_pll_lock(self, timeout=1000):
        logger.debug("Waiting for PLL lock...")
        times = 0

        while times < timeout and self['rx'].get_pll_locked() == 0:
            times += 1

        if self['rx'].get_pll_locked() == 1:
            logger.debug("PLL locked")
            return True
        else:
            raise RuntimeError('Timeout while waiting for PLL to lock.')

    def get_trigger_counter(self):
        return self['tlu']['TRIGGER_COUNTER']

    def set_tlu_module(self, trigger_enable):
        self['tlu']['TRIGGER_ENABLE'] = trigger_enable

    def set_trigger_data_delay(self, trigger_data_delay):
        self['tlu']['TRIGGER_DATA_DELAY'] = trigger_data_delay

    def configure_tlu_module(self, **kwargs):
        # Reset first TLU module
        self['tlu']['RESET'] = 1
        # Set specified registers
        for key, value in kwargs['TRIGGER'].items():
            self['tlu'][key] = value
        # Set maximum number of triggers
        if kwargs['max_triggers']:
            self['tlu']['MAX_TRIGGERS'] = kwargs['max_triggers']
        else:
            # unlimited number of triggers
            self['tlu']['MAX_TRIGGERS'] = 0

    def get_tlu_erros(self):
        return (self['tlu']['TRIGGER_LOW_TIMEOUT_ERROR_COUNTER'], self['tlu']['TLU_TRIGGER_ACCEPT_ERROR_COUNTER'])

    def configure_trigger_cmd_pulse(self, **kwargs):
        # configures pulse which is sent to CMD for incoming triggers; factor 4 is needed for conversion from 160 MHz to 40 MHz (BC)
        self['pulser_trig'].set_en(True)
        self['pulser_trig'].set_width(kwargs['trigger_length'] * 4)
        self['pulser_trig'].set_delay(kwargs['trigger_delay'] * 4)
        self['pulser_trig'].set_repeat(1)

    def configure_tlu_veto_pulse(self, **kwargs):
        # configures pulse for veto of new triggers; factor 4 is needed for conversion from 160 MHz to 40 MHz (BC)
        self['pulser_veto'].set_en(True)
        self['pulser_veto'].set_width(4)
        self['pulser_veto'].set_delay(kwargs['veto_length'] * 4)
        self['pulser_veto'].set_repeat(1)
