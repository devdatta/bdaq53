#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

''' Module to manage firmware download, compilation and flashing using vivado.
    Mainly for CI runner but also useful during headless session (e.g. test beams)
'''

import urllib
import re
import logging
import requests
import math
import fileinput
import pkg_resources
import tarfile
import shutil
import os.path
from sys import platform

import pexpect
from tqdm import tqdm
import git

import bdaq53

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

repository = r'https://gitlab.cern.ch/silab/bdaq53/'
firmware_dev_url = repository + r'wikis/Hardware/Firmware-(development-versions)'
firmware_url = repository + r'tags'

sitcp_repo = r'https://github.com/BeeBeansTechnologies/SiTCP_Netlist_for_Kintex7'

bdaq53_path = os.path.dirname(bdaq53.__file__)


def get_web_page(url):
    ''' Read web page.
    '''
    response = urllib.request.urlopen(url)  # Add md for markdown
    return response.read().decode('utf-8')


def download_firmware(name, url):
    ''' Download firmware tar.gz file from url

        The release firmwares are stored with the tag information (firmware_url)
        and the development firmware is attached to a wiki page (firmware_dev_url)
    '''
    response = get_web_page(url)
    links = re.findall(r'\[%s\]\((.*?)\)' % name, response)
    links = [repository + 'wikis/' + ''.join(link) for link in links]

    if not links:
        all_firmwares = re.findall(r'%s(.*?)%s' % (r'/uploads/\w+/', '.tar.gz'), response)
        logging.error('No firmware with name %s at %s\nPossible firmwares: %s', name, url, ', '.join(all_firmwares))
        return
    if len(links) > 1:
        raise RuntimeError('Found multiple firmwares with name %s at %s: %s', name, url, str(links))
    download_link = links[0]
    logging.info('Downloading %s', download_link)
    # Streaming, so we can iterate over the response.
    r = requests.get(download_link, stream=True)

    # Total size in bytes.
    total_size = int(r.headers.get('content-length', 0))
    block_size = 1024
    wrote = 0
    with open(name, 'wb') as f:
        for data in tqdm(r.iter_content(block_size), total=math.ceil(total_size // block_size), unit='KB', unit_scale=True):
            wrote = wrote + len(data)
            f.write(data)
    if total_size != 0 and wrote != total_size:
        raise RuntimeError('Download failed!')
    return name


def unpack_bit_file(name):
    ''' Extracts the bit file from the tar.gz file

        name: string
            Name of bit file (.tar.gz suffix) and
            Name of compressed bitfile (name.bit)
    '''
    tar = tarfile.open(name, "r:gz")
    name_no_suffix = name[:name.find('.')]
    for m in tar.getmembers():
        if name_no_suffix + '.bit' in m.name:
            m.name = os.path.basename(m.name)  # unpack without folder
            tar.extract(m, path=".")
            logging.debug('Unpacked %s', m.name)
    tar.close()
    return name_no_suffix + '.bit'


def flash_firmware(name):
    ''' Flash firmware using vivado in tcl mode
    '''

    def get_return_string(timeout=1):
        ''' Helper function to get full return string.

            This complexity needed here since Xilinx does multi line returns
        '''
        flushed = bytearray()
        try:
            while not vivado.expect(r'.+', timeout=timeout):
                flushed += vivado.match.group(0)
        except pexpect.exceptions.TIMEOUT:
            pass
        return flushed.decode('utf-8')

    logger.info('Flash firmware %s', name)

    try:
        vivado = pexpect.spawn('vivado_lab -mode tcl', timeout=10)  # try lab version
        vivado.expect('Vivado', timeout=5)
    except pexpect.exceptions.ExceptionPexpect:
        try:
            vivado = pexpect.spawn('vivado -mode tcl', timeout=10)  # try full version
            vivado.expect('Vivado', timeout=5)
        except pexpect.exceptions.ExceptionPexpect:
            logger.error('Cannot execute vivado / vivado_lab commend')
            return
    vivado.expect(['vivado_lab%', 'Vivado%'])  # Booted up when showing prompt

    vivado.sendline('open_hw')
    vivado.expect(['vivado_lab%', 'Vivado%'])  # Command finished when showing prompt

    vivado.sendline('connect_hw_server')
    vivado.expect('localhost')  # Printed when successfull
    get_return_string()

    vivado.sendline('current_hw_target')
    ret = get_return_string()
    if 'WARNING' in ret:
        logger.error('Cannot find programmer hardware, Xilinx warning:\n%s', ret)
        vivado.sendline('exit')
        vivado.expect('Exiting')
        return

    vivado.sendline('open_hw_target')
    vivado.expect('Opening hw_target')  # Printed when programmer found

    vivado.sendline('current_hw_device [lindex [get_hw_devices] 0]')
    vivado.expect(['vivado_lab%', 'Vivado%'])  # Printed when finished

    vivado.sendline('set devPart [get_property PART [current_hw_device]]')
    vivado.expect(['vivado_lab%', 'Vivado%'])  # Printed when finished

    vivado.sendline('set_property PROGRAM.FILE {%s} [current_hw_device]' % name)
    vivado.expect(['vivado_lab%', 'Vivado%'])  # Printed when finished

    vivado.sendline('program_hw_devices [current_hw_device]')
    vivado.expect('End of startup status: HIGH')  # firmware upload successfull

    vivado.sendline('exit')
    vivado.expect('Exiting')

    logger.info('SUCCESS!')


def compile_firmware(name):
    ''' Compile firmware using vivado in tcl mode
    '''

    def get_return_string(timeout=1):
        ''' Helper function to get full return string.

            This complexity needed here since Xilinx does multi line returns
        '''
        flushed = bytearray()
        try:
            while not vivado.expect(r'.+', timeout=timeout):
                flushed += vivado.match.group(0)
        except (pexpect.exceptions.TIMEOUT, pexpect.exceptions.EOF):
            pass
        return flushed.decode('utf-8')

    supported_firmwares = ['BDAQ53', 'USBPIX3', 'KC705',
                           'BDAQ53_RX640', 'USBPIX3_RX640', 'KC705_RX640']
    if name not in supported_firmwares:
        logger.error('Can only compile firmwares: %s', ','.join(supported_firmwares))
        return

    logger.info('Compile firmware %s', name)

    vivado_tcl = os.path.join(bdaq53_path, '..', 'firmware/vivado')

    # Use mappings from run.tcl
    fpga_types = {'BDAQ53': 'xc7k160tffg676-2',
                  'USBPIX3': 'xc7k160tfbg676-1',
                  'KC705': 'xc7k325tffg900-2'}
    constrains_files = {'BDAQ53': '../src/bdaq53.xdc',
                        'USBPIX3': '../src/usbpix3.xdc',
                        'KC705': '../src/kc705_gmii.xdc'}
    flash_sizes = {'BDAQ53': '64',
                   'USBPIX3': '64',
                   'KC705': '16'}

    for k, v in fpga_types.items():
        if k in name:
            fpga_type = v
            constrain_files = constrains_files[k]
            flash_size = flash_sizes[k]
            board_name = k

    if '_SMA' in name:
        connector = '_SMA'
    elif '_FMC_LPC' in name:
        connector = '_FMC_LPC'
    else:
        connector = '""'

    if 'RX640' in name:
        option = '_RX640'
    else:
        option = '""'

    command_args = fpga_type + ' ' + board_name + ' ' + connector + ' ' + constrain_files + ' ' + flash_size + ' ' + option
    command = 'vivado -mode tcl -source run.tcl -tclargs %s' % command_args
    logger.info('Compiling firmware. Takes about 10 minutes!')
    try:
        vivado = pexpect.spawn(command, cwd=vivado_tcl, timeout=10)
        vivado.expect('Vivado', timeout=5)
    except pexpect.exceptions.ExceptionPexpect:
        logger.error('Cannot execute vivado command %d.\nMaybe paid version is missing, that is needed for compilation?', command)
        return

    import time
    timeout = 10  # 50 seconds with no new print to screen
    t = 0
    while t < timeout:
        r = get_return_string()
        if r:
            if 'write_cfgmem completed successfully' in r:
                break
            print('.', end='', flush=True)
            t = 0
        else:
            time.sleep(5)
            t += 1
    else:
        raise RuntimeError('Timeout during compilation, check vivado.log')

    # Move firmware to current folder
    cwd = os.getcwd()
    vivado_tcl = os.path.join(bdaq53_path, '..', 'firmware/vivado')
    shutil.move(os.path.join(vivado_tcl, r'output/%s' % name + '.bit'), cwd)
    logger.info('SUCCESS!')


def find_vivado(path):
    ''' Search in std. installation paths for vivado(_lab) binary
    '''

    if platform == "linux" or platform == "linux2":
        linux_install_path = '/opt/' if not path else path
        # Try vivado full install
        paths = where(name='vivado', path=linux_install_path)
        for path in paths:
            if 'bin' in path:
                return os.path.dirname(os.path.realpath(path))
        # Try vivado lab install
        paths = where(name='vivado_lab', path=linux_install_path)
        for path in paths:
            if 'bin' in path:
                return os.path.dirname(os.path.realpath(path))
    else:
        raise NotImplementedError('Only Linux supported')


def where(name, path, flags=os.F_OK):
    result = []
    paths = [path]
    for outerpath in paths:
        for innerpath, _, _ in os.walk(outerpath):
            path = os.path.join(innerpath, name)
            if os.access(path, flags):
                result.append(os.path.normpath(path))
    return result


def main(name, path=None, create=False):
    ''' Steps to download/compile/flash matching firmware to FPGA

        name: str
            Firmware name:
                If name has .bit suffix try to flash from local file
                If not available find suitable firmware online, download, extract, and  flash
        compile: boolean
            Compile firmware
    '''

    vivado_path = find_vivado(path)
    if vivado_path:
        logger.debug('Found vivado binary at %s', vivado_path)
        os.environ["PATH"] += os.pathsep + vivado_path
    else:
        if path:
            logger.error('Cannot find vivado installation in %s', path)
        else:
            logger.error('Cannot find vivado installation!')
            if not create:
                logger.error('Install vivado lab from here:\nhttps://www.xilinx.com/support/download.html')
            else:
                logger.error('Install vivado paid version to be able to compile firmware')
        return

    if not create:
        if os.path.isfile(name):
            logger.info('Found existing local bit file')
            bit_file = name
        else:
            if not name.endswith('.tar.gz'):
                name += '.tar.gz'
            stable_firmware = True  # std. setting: use stable (tag) firmware
            version = pkg_resources.get_distribution("bdaq53").version
            if not os.getenv('CI'):
                try:
                    import git
                    try:
                        repo = git.Repo(search_parent_directories=True, path=bdaq53_path)
                        active_branch = repo.active_branch
                        if active_branch != 'master':
                            stable_firmware = False  # use development firmware
                    except git.InvalidGitRepositoryError:  # no github repo --> use stable firmware
                        pass
                except ImportError:  # git not available
                    logger.warning('Git not properly installed, assume software release %s', version)
                    pass
                if stable_firmware:
                    tag_list = get_tag_list(firmware_url)
                    matches = [i for i in range(len(tag_list)) if version in tag_list[i]]
                    if not matches:
                        raise RuntimeError('Cannot find tag version %s at %s', version, firmware_url)
                    tag_url = firmware_url + '/' + tag_list[matches[0]]
                    logger.info('Download stable firmware version %s', version)
                    archiv_name = download_firmware(name, tag_url)
                else:
                    logger.info('Download development firmware')
                    archiv_name = download_firmware(name, firmware_dev_url + '.md')
            else:  # always use development version for CI runner
                archiv_name = download_firmware(name, firmware_dev_url + '.md')
            if not archiv_name:
                return
            bit_file = unpack_bit_file(archiv_name)
        flash_firmware(bit_file)
    else:
        get_si_tcp()  # get missing SiTCP sources
        compile_firmware(name)


def get_tag_list(url):
    ''' Extracts all tag names from firmware_url

        This is needed since the naming scheme is inconsistent
    '''

    response = get_web_page(url)
    return re.findall(r'href="/silab/bdaq53/tags/(.*?)"', response)


def get_si_tcp():
    ''' Download SiTCP sources from official github repo and apply patches
    '''

    def line_prepender(filename, line):
        with open(filename, 'rb+') as f:
            content = f.read()
            f.seek(0, 0)
            # Python 3, wtf?
            add = bytearray()
            add.extend(map(ord, line))
            add.extend(map(ord, '\n'))
            f.write(add + content)

    sitcp_folder = os.path.join(bdaq53_path, '..', 'firmware/SiTCP/')

    # Only download if not already existing SiTCP git repository
    if not os.path.isdir(os.path.join(sitcp_folder, '.git')):
        logger.info('Downloading SiTCP')

        # Has to be moved to be allowed to use existing folder for git checkout
        shutil.move(sitcp_folder + '.gitkeep', os.path.join(sitcp_folder, '..'))
        git.Repo.clone_from(url=sitcp_repo,
                            to_path=sitcp_folder, branch='master')
        shutil.move(os.path.join(sitcp_folder, '..', '.gitkeep'), sitcp_folder)
        # Patch sources, see README of bdaq53
        line_prepender(filename=sitcp_folder + 'TIMER.v', line=r'`default_nettype wire')
        line_prepender(filename=sitcp_folder + 'WRAP_SiTCP_GMII_XC7K_32K.V', line=r'`default_nettype wire')
        for line in fileinput.input([sitcp_folder + 'WRAP_SiTCP_GMII_XC7K_32K.V'], inplace=True):
            print(line.replace("assign\tMY_IP_ADDR[31:0]\t= (~FORCE_DEFAULTn | (EXT_IP_ADDR[31:0]==32'd0) \t? DEFAULT_IP_ADDR[31:0]\t\t: EXT_IP_ADDR[31:0]\t\t);",
                               'assign\tMY_IP_ADDR[31:0]\t= EXT_IP_ADDR[31:0];'), end='')
    else:  # update if existing
        g = git.cmd.Git(sitcp_folder)
        g.pull()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    main('BDAQ53_RX640')
