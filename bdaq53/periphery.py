#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os
import time
import logging
import yaml
from tqdm import tqdm

import basil
from basil.dut import Dut

loglevel = logging.getLogger('RD53A').getEffectiveLevel()


class DeviceNotFoundError(IOError):
    pass


class BDAQ53Periphery(object):
    scc_power_names = {'(s)LDO': ['VDD', 'VINA', 'VIND'], 'Direct Powering': ['VDDA', 'VDDD']}

    def __init__(self, bench_config=None, conf=None):
        self.logger = logging.getLogger('Periphery')
        self.logger.setLevel(loglevel)
        self.logger.success = lambda msg, *args, **kwargs: self.logger.log(logging.SUCCESS, msg, *args, **kwargs)
        logfile = None
        for handler in logging.getLogger('RD53A').handlers:
            if type(handler) is logging.FileHandler:
                logfile = handler
                break
        if logfile:
            self.logger.addHandler(logfile)

        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        if conf is None:
            self.conf = os.path.join(self.proj_dir, 'bdaq53' + os.sep + 'periphery.yaml')
        else:
            self.conf = conf

        if bench_config is None:
            bench_config = os.path.join(self.proj_dir, 'bdaq53' + os.sep + 'testbench.yaml')

        with open(bench_config) as f:
            self.bench_config = yaml.load(f)
        self.devices = None

    def __del__(self):
        self.close()

    def init(self):
        self.devices = Dut(self.conf)
        if not self.bench_config.get('use_periphery', False):
            self.devices = None
            self.logger.info('Periphery is disabled.')
            return
        try:
            self.devices.init()
            self.logger.debug('Initialized SCC powersupply %s' % (self.devices['SCC_Powersupply'].get_name()))
            try:
                self.logger.debug('Initialized sensor bias powersupply %s' % (self.devices['SensorBias'].get_name()))
            except KeyError:
                pass
            try:
                self.logger.debug('Initialized Multimeter %s' % (self.devices['Multimeter'].get_name()))
            except KeyError:
                pass
        except (IOError, OSError, ImportError, AttributeError) as e:
            self.devices = None
            self.logger.error('Cannot initialize periphery: %s' % e)

    def close(self):
        if self.devices:
            self.devices.close()

    def power_on_BDAQ(self):
        if self.devices is None:
            self.logger.debug('No periphery set up!')
            return

        for ps in self.devices:
            if not isinstance(ps, basil.RL.FunctionalRegister.FunctionalRegister):
                continue

            if ps.name == 'BDAQ':
                ps.set_enable(on=True)
                self.logger.success('Powered on BDAQ board!')
                time.sleep(2)
                break

    def power_off_BDAQ(self):
        if self.devices is None:
            self.logger.debug('No periphery set up!')
            return

        for ps in self.devices:
            if not isinstance(ps, basil.RL.FunctionalRegister.FunctionalRegister):
                continue

            if ps.name == 'BDAQ':
                ps.set_enable(on=False)
                self.logger.success('Powered off BDAQ board!')
                continue

    def power_on_SCC(self, **kwargs):
        if self.devices is None:
            self.logger.debug('No periphery set up!')
            return

        try:
            turned_on = False
            for ps in self.devices:
                if not isinstance(ps, basil.RL.FunctionalRegister.FunctionalRegister):
                    continue

                else:
                    mode = None
                    for md in self.scc_power_names.keys():
                        if ps.name in self.scc_power_names[md]:
                            mode = md

                    if mode is None:
                        continue

                    voltage = kwargs.get(ps.name)
                    cur_lim = kwargs.get(ps.name + '_cur_lim', 0.5)

                    if mode == 'Direct Powering' and voltage > 1.4:
                        raise ValueError('Voltage in direct powering mode should not be above 1.4V! Make sure everything is configured correctly!')

                    if not voltage:
                        self.logger.debug('Value for %s is not defined!' % ps.name)
                        continue

                    self.logger.debug('Set channel %s to %1.3fV with current limit %1.3fA' % (ps.name, voltage, cur_lim))

                    ps.set_voltage(voltage)
                    if cur_lim:
                        ps.set_current_limit(cur_lim)

                    if float(ps.get_current()) == 0.0:
                        ps.set_enable(on=True)
                        turned_on = True

            if turned_on:
                self.logger.success('Powered on SCC in %s mode!' % mode)
                time.sleep(3)
            else:
                self.logger.info('SCC already set up!')
        except KeyError as e:
            self.logger.error('There was an error setting up the SCC powersupply: %s' % e)

    def power_off_SCC(self):
        if self.devices is None:
            self.logger.debug('No periphery set up!')
            return

        try:
            for ps in self.devices:
                if not isinstance(ps, basil.RL.FunctionalRegister.FunctionalRegister):
                    continue

                if ps.name not in [item for sublist in self.scc_power_names.values() for item in sublist]:
                    continue

                self.logger.debug('Turn off channel %s' % (ps.name))

                ps.set_enable(on=False)

            self.logger.success('Powered off SCC')
        except KeyError:
            self.logger.debug('No powersupply found.')

    def get_power_SCC(self, log=False):
        voltages, currents = {}, {}
        if self.devices is None:
            self.logger.debug('No periphery set up!')
            return voltages, currents

        try:
            for ps in self.devices:
                if not isinstance(ps, basil.RL.FunctionalRegister.FunctionalRegister):
                    continue

                if ps.name not in [item for sublist in self.scc_power_names.values() for item in sublist]:
                    continue

                voltages[ps.name] = ps.get_voltage()
                currents[ps.name] = ps.get_current()

            if log and len(currents) > 0:
                text = ''
                for key, value in currents.items():
                    text += key + ': ' + str(value) + 'A @ ' + str(voltages[key]) + 'V, '
                self.logger.info('%s' % (text[:-2]))

            return voltages, currents

        except KeyError:
            self.logger.debug('No powersupply found.')

    def power_on_sensor_bias(self, **kwargs):
        if self.devices is None:
            self.logger.debug('No periphery set up!')
            return

        try:
            set_up = False
            voltage = kwargs.get('sensor_bias')
            current_limit = kwargs.get('sensor_bias_cur_lim', 1e-6)

            if not voltage:
                self.logger.debug('Value for SensorBias is not defined!')
                return

            self.devices['SensorBias'].set_current_limit(current_limit)

            if self.get_sensor_bias()[1] < 1e-10:
                self.devices['SensorBias'].set_voltage(0)
                self.devices['SensorBias'].on()

            self._ramp_sensor_bias_to(voltage)
            time.sleep(5)
            set_up = True

            if set_up:
                self.logger.success('Powered on sensor bias. Bias current is %s' % (self.get_sensor_bias()[1]))
        except KeyError as e:
            self.logger.error('There was an error setting up the powersupply: %s' % e)

    def power_off_sensor_bias(self):
        if self.devices is None:
            self.logger.debug('No periphery set up!')
            return

        try:
            set_up = False
            for ps in self.devices:
                if isinstance(ps, basil.RL.FunctionalRegister.FunctionalRegister):
                    continue

                elif ps.name == 'SensorBias':
                    self._ramp_sensor_bias_to(0)
                    self.devices['SensorBias'].off()
                    set_up = True

            if set_up:
                self.logger.success('Powered off sensor bias voltage.')
        except KeyError as e:
            self.logger.error('There was an error setting up the powersupply: %s' % e)

    def get_sensor_bias(self):
        if self.devices is None:
            self.logger.debug('No periphery set up!')
            return 0, 0

        voltage = int(float(self.devices['SensorBias'].get_voltage().split(',')[0]))
        current = float(self.devices['SensorBias'].get_current().split(',')[1])
        return voltage, current

    def set_sensor_bias(self, voltage, verbose=True):
        if self.devices is None:
            self.logger.debug('No periphery set up!')
            return

        self._ramp_sensor_bias_to(voltage, verbose=verbose)

    def _ramp_sensor_bias_to(self, voltage, verbose=True):
        start_voltage = int(float(self.devices['SensorBias'].get_voltage().split(',')[0]))
        if abs(start_voltage - voltage) > 10:
            stepsize = 2
        elif abs(start_voltage - voltage) > 100:
            stepsize = 10
        else:
            stepsize = 1

        voltage_step = stepsize if voltage > start_voltage else -1 * stepsize
        add = 1 if voltage > 0 else -1

        if verbose:
            self.logger.info('Ramping bias voltage to %iV...' % voltage)
            pbar = tqdm(total=len(range(start_voltage, voltage + add, voltage_step)), unit=' V')
        for v in range(start_voltage, voltage + add, voltage_step):
            self.devices['SensorBias'].set_voltage(v)
            if verbose:
                pbar.update(1)
            time.sleep(0.5)
        if verbose:
            pbar.close()

    def get_multimeter_voltage(self):
        try:
            voltage = float(self.devices['Multimeter'].get_voltage().split(',')[0])
            return voltage
        except (TypeError, KeyError):
            self.logger.warning('No multimeter set up!')
            return 0.0

    def get_multimeter_current(self):
        try:
            current = float(self.devices['Multimeter'].get_current().split(',')[1])
            return current
        except (TypeError, KeyError):
            self.logger.warning('No multimeter set up!')
            return 0.0
