#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import yaml
import logging
import os
import time
import struct
import numpy as np
import tables as tb

from tables.exceptions import NoSuchNodeError

from basil.utils.BitLogic import BitLogic

from bdaq53.analysis import analysis_utils
from bdaq53.register_utils import RD53ARegisterParser


logger = logging.getLogger('RD53A')
logger.setLevel(logging.getLogger('BDAQ53').getEffectiveLevel())
logger.success = lambda msg, *args, **kwargs: logger.log(logging.SUCCESS, msg, *args, **kwargs)


class RD53ACalibration(object):
    # Natural constants
    e = 1.6021766208e-19
    kb = 1.38064852e-23

    def __init__(self, calibration=None):
        if calibration is None:
            calibration = {}
        # Chip specific calibration constants
        self.ADC_a = calibration.get('ADC_a', 0.19)   # Conversion factors ADC counts - voltage: slope
        self.ADC_b = calibration.get('ADC_b', 8.21)   # Conversion factors ADC counts - voltage: intercept
        self.Nf = calibration.get('Nf', 1.31)  # Chip specific conversion factor voltage - temperature
        self.T_a = calibration.get('T_a', 1.032)    # Chip specific correction factors temperature sensors - NTC
        self.T_b = calibration.get('T_b', -0.65)    # Chip specific correction factors temperature sensors - NTC

    def _dV_to_T(self, dV):
        return self.e / (self.Nf * self.kb * np.log(15)) * dV - 273.15

    def _cal_to_NTC(self, T):
        return (T - self.T_b) / self.T_a

    def get_V_from_ADC(self, ADC, use_offset=True):
        if use_offset:
            return round(((self.ADC_a * ADC + self.ADC_b) / 1000), 3)
        else:
            return round(((self.ADC_a * ADC) / 1000), 3)

    def get_temperature_from_ADC(self, dADC):
        return round((self._dV_to_T(self.get_V_from_ADC(dADC, use_offset=False))), 3)


class RD53ARegister(dict):
    def __init__(self, chip, name, address, size, default, value, mode, reset, description):
        self.chip = chip
        self.changed = False
        super(RD53ARegister, self).__init__()

        self['name'] = name
        self['address'] = eval(address)
        self['size'] = size
        self['default'] = eval(default)
        self['value'] = eval(value)
        self['mode'] = mode
        self['reset'] = reset
        self['description'] = description

    def __str__(self, *args, **kwargs):
        text = self['name']
        text += ': '
        text += self['description']
        text += '\nAddress : '
        text += hex(self['address'])
        text += '\nSize    : '
        text += str(self['size'])
        text += '\nValue   : '
        text += ('0b{0:0' + str(self['size']) + 'b} ({1:d})').format(self['value'], self['value'])
        text += '\nDefault : '
        text += ('0b{0:0' + str(self['size']) + 'b} ({1:d})').format(self['default'], str(self['default']))
        text += '\nMode    : '
        if self['mode'] == 1:
            text += 'r/w'
        else:
            text += 'r'

        return text

    def _assert_value(self, value):
        if type(value) == int:
            return value
        if type(value) == str:
            if value[:2] == '0b':
                return int(value, 2)
            if value[:2] == '0x':
                return int(value, 16)
        raise ValueError('Invalid value: {0}'.format(value))

    def set(self, value):
        value = self._assert_value(value)
        if value != self['value']:
            self.changed = True
            self['value'] = value

    def get(self):
        return self['value']

    def print_value(self):
        print(('{0} = 0b{1:0' + str(self['size']) + 'b} ({2:d})').format(self['name'], self['value'], self['value']))

    def write(self, value=None, verify=False, write_ctr=0):
        value = self._assert_value(value)
        if value is not None:
            self['value'] = value
        logger.debug(('Writing value 0b{0:0' + str(self['size']) + 'b} to register {1}').format(self['value'], self['name']))
        self.chip._write_register(self['address'], self['value'])

        if verify:
            if self.read() != value:
                if write_ctr >= 10:
                    raise RuntimeError('Could not verify value {0} in register {1}'.format(value, self['name']))
                self.write(value=value, verify=True, write_ctr=write_ctr + 1)

    def get_write_command(self, value=None):
        if value is not None:
            self['value'] = value
        return self.chip._write_register(self['address'], self['value'], write=False)

    def read(self):
        val = self.chip._get_register_value(self['address'])
        if val != self['value'] and self['mode'] is 1:
            logger.warning(('Register {0} did not have the expected value: Expected 0b{2:0' + str(self['size']) + 'b}, got 0b{1:0' + str(self['size']) + 'b}').format(self['name'], val, self['value']))
        return val

    def reset(self):
        ''' Overwrite a register with its default value'''
        if self['reset'] is 1 and self['mode'] is 1:
            self.write(self['default'])


class RD53ARegisterObject(dict):
    ''' RD53A register object collects all RD53A registers,
        creates them on initialization from a yaml file and
        handles all manipulation of all registers at once.
    '''

    def __init__(self, chip, lookup_file=None):
        self.chip = chip
        super(RD53ARegisterObject, self).__init__()

        if lookup_file is None:
            lookup_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'register_lookup.yaml')

        with open(lookup_file, 'r') as f:
            regs = yaml.load(f)

        for reg in regs['registers']:
            self._add(name=reg['name'], address=reg['address'], size=reg['size'], default=reg['default'], mode=reg['mode'], reset=reg['reset'], description=reg['description'])

    def _add(self, name, address, size, default, mode, reset, value=None, description=''):
        if value is None:
            value = default
        self[name] = RD53ARegister(chip=self.chip, name=name, address=address, size=size, default=default, mode=mode, reset=reset, value=value, description=description)

    def get_name_from_address(self, address):
        for reg in self.values():
            if reg['address'] is address:
                return reg['name']
        else:
            raise ValueError('No register found with address {0}'.format(address))

    def write_all(self, force=False):
        '''
        Collect all registers that were changed in software and write to chip
        If force==True, write all software values to chip
        '''
        indata = self.chip.write_sync_01(write=False)
        for reg in self.values():
            if reg['mode'] != 1 or (not force and not reg.changed):
                continue

            indata += self.chip._write_register(reg['address'], reg['value'], write=False)
            indata += self.chip.write_sync_01(write=False) * 100

            if len(indata) > 3500:
                indata += self.chip.write_sync_01(write=False) * 100
                self.chip.write_command(indata)
                indata = self.chip.write_sync_01(write=False)

        self.chip.write_command(indata)

    def check_all(self):
        ''' Compare all chip registers to software and log result '''
        for reg in self.values():
            if reg['mode'] is 1 and reg['name'] not in ['PIX_PORTAL']:
                reg.read()

    def update_all(self):
        ''' Set software status to chip registers '''
        for reg in self.values():
            if reg['mode'] is 1:
                reg.read()

    def reset_all(self):
        ''' Set all chip registers to default values '''
        for reg in self.values():
            if reg['reset'] is 1:
                reg.set(reg['default'])
        self.write_all(force=True)


class RD53A(object):
    '''
    Main class for RD53A chip
    '''

    cmd_data_map = {
        0: 0b01101010,
        1: 0b01101100,
        2: 0b01110001,
        3: 0b01110010,
        4: 0b01110100,
        5: 0b10001011,
        6: 0b10001101,
        7: 0b10001110,
        8: 0b10010011,
        9: 0b10010101,
        10: 0b10010110,
        11: 0b10011001,
        12: 0b10011010,
        13: 0b10011100,
        14: 0b10100011,
        15: 0b10100101,
        16: 0b10100110,
        17: 0b10101001,
        18: 0b10101010,
        19: 0b10101100,
        20: 0b10110001,
        21: 0b10110010,
        22: 0b10110100,
        23: 0b11000011,
        24: 0b11000101,
        25: 0b11000110,
        26: 0b11001001,
        27: 0b11001010,
        28: 0b11001100,
        29: 0b11010001,
        30: 0b11010010,
        31: 0b11010100
    }

    trigger_map = {
        0: 0b00101011,
        1: 0b00101011,
        2: 0b00101101,
        3: 0b00101110,
        4: 0b00110011,
        5: 0b00110101,
        6: 0b00110110,
        7: 0b00111001,
        8: 0b00111010,
        9: 0b00111100,
        10: 0b01001011,
        11: 0b01001101,
        12: 0b01001110,
        13: 0b01010011,
        14: 0b01010101,
        15: 0b01010110
    }

    CMD_GLOBAL_PULSE = 0b01011100
    CMD_CAL = 0b01100011
    CMD_REGISTER = 0b01100110
    CMD_RDREG = 0b01100101
    CMD_NULL = 0b01101001
    CMD_ECR = 0b01011010
    CMD_BCR = 0b01011001
    CMD_SYNCH = 0b10000001
    CMD_SYNCL = 0b01111110
    CMD_SYNC = [0b10000001, 0b01111110]  # 0x(817E)

    macro_regs = [{'columns': range(0, 32),
                   'macro_columns': range(0, 16),
                   'macro_name': 'EN_MACRO_COL_CAL_SYNC_1'},

                  {'columns': range(32, 64),
                   'macro_columns': range(16, 32),
                   'macro_name': 'EN_MACRO_COL_CAL_SYNC_2'},

                  {'columns': range(64, 96),
                   'macro_columns': range(32, 48),
                   'macro_name': 'EN_MACRO_COL_CAL_SYNC_3'},

                  {'columns': range(96, 128),
                   'macro_columns': range(48, 64),
                   'macro_name': 'EN_MACRO_COL_CAL_SYNC_4'},

                  {'columns': range(128, 160),
                   'macro_columns': range(64, 80),
                   'macro_name': 'EN_MACRO_COL_CAL_LIN_1'},

                  {'columns': range(160, 192),
                   'macro_columns': range(80, 96),
                   'macro_name': 'EN_MACRO_COL_CAL_LIN_2'},

                  {'columns': range(192, 224),
                   'macro_columns': range(96, 112),
                   'macro_name': 'EN_MACRO_COL_CAL_LIN_3'},

                  {'columns': range(224, 256),
                   'macro_columns': range(112, 128),
                   'macro_name': 'EN_MACRO_COL_CAL_LIN_4'},

                  {'columns': range(256, 264),
                   'macro_columns': range(128, 132),
                   'macro_name': 'EN_MACRO_COL_CAL_LIN_5'},

                  {'columns': range(264, 296),
                   'macro_columns': range(132, 148),
                   'macro_name': 'EN_MACRO_COL_CAL_DIFF_1'},

                  {'columns': range(296, 328),
                   'macro_columns': range(148, 164),
                   'macro_name': 'EN_MACRO_COL_CAL_DIFF_2'},

                  {'columns': range(328, 360),
                   'macro_columns': range(164, 180),
                   'macro_name': 'EN_MACRO_COL_CAL_DIFF_3'},

                  {'columns': range(360, 392),
                   'macro_columns': range(180, 196),
                   'macro_name': 'EN_MACRO_COL_CAL_DIFF_4'},

                  {'columns': range(392, 400),
                   'macro_columns': range(196, 200),
                   'macro_name': 'EN_MACRO_COL_CAL_DIFF_5'}]

    core_regs = [{'columns': range(0, 128),
                  'core_columns': range(0, 16),
                  'core_name': 'EN_CORE_COL_SYNC'},

                 {'columns': range(128, 256),
                  'core_columns': range(16, 32),
                  'core_name': 'EN_CORE_COL_LIN_1'},

                 {'columns': range(256, 264),
                  'core_columns': range(32, 33),
                  'core_name': 'EN_CORE_COL_LIN_2'},

                 {'columns': range(264, 392),
                  'core_columns': range(33, 49),
                  'core_name': 'EN_CORE_COL_DIFF_1'},

                 {'columns': range(392, 400),
                  'core_columns': range(49, 50),
                  'core_name': 'EN_CORE_COL_DIFF_2'}]

    default_dac_values = {'IBIASP1_SYNC': 100,
                          'IBIASP2_SYNC': 150,
                          'IBIAS_SF_SYNC': 100,
                          'IBIAS_KRUM_SYNC': 140,
                          'IBIAS_DISC_SYNC': 200,
                          'ICTRL_SYNCT_SYNC': 100,
                          'VBL_SYNC': 450,
                          'VTH_SYNC': 300,
                          'VREF_KRUM_SYNC': 490,
                          'CONF_FE_SYNC': 0x0002,
                          'PA_IN_BIAS_LIN': 300,
                          'FC_BIAS_LIN': 20,
                          'KRUM_CURR_LIN': 50,
                          'LDAC_LIN': 80,
                          'COMP_LIN': 110,
                          'REF_KRUM_LIN': 300,
                          'Vthreshold_LIN': 408,
                          'PRMP_DIFF': 533,
                          'FOL_DIFF': 542,
                          'PRECOMP_DIFF': 551,
                          'COMP_DIFF': 528,
                          'VFF_DIFF': 164,
                          'VTH1_DIFF': 1023,
                          'VTH2_DIFF': 0,
                          'LCC_DIFF': 20,
                          'CONF_FE_DIFF': 0b10}

    voltage_mux = {'ADCbandgap': 0,
                   'CAL_MED_left': 1,
                   'CAL_HI_left': 2,
                   'TEMPSENS_1': 3,
                   'RADSENS_1': 4,
                   'TEMPSENS_2': 5,
                   'RADSENS_2': 6,
                   'TEMPSENS_4': 7,
                   'RADSENS_4': 8,
                   'VREF_VDAC': 9,
                   'VOUT_BG': 10,
                   'IMUX_out': 11,
                   'VCAL_MED': 12,
                   'VCAL_HIGH': 13,
                   'RADSENS_3': 14,
                   'TEMPSENS_3': 15,
                   'REF_KRUM_LIN': 16,
                   'Vthreshold_LIN': 17,
                   'VTH_SYNC': 18,
                   'VBL_SYNC': 19,
                   'VREF_KRUM_SYNC': 20,
                   'VTH_HI_DIFF': 21,
                   'VTH_LO_DIFF': 22,
                   'VIN_Ana_SLDO': 23,
                   'VOUT_Ana_SLDO': 24,
                   'VREF_Ana_SLDO': 25,
                   'VOFF_Ana_SLDO': 26,
                   'ground': 27,
                   'ground1': 28,
                   'VIN_Dig_SLDO': 29,
                   'VOUT_Dig_SLDO': 30,
                   'VREF_Dig_SLDO': 31,
                   'VOFF_Dig_SLDO': 32,
                   'ground2': 33}

    current_mux = {'IREF': 0,
                   'IBIASP1_SYNC': 1,
                   'IBIASP2_SYNC': 2,
                   'IBIAS_DISC_SYNC': 3,
                   'IBIAS_SF_SYNC': 4,
                   'ICTRL_SYNCT_SYNC': 5,
                   'IBIAS_KRUM_SYNC': 6,
                   'COMP_LIN': 7,
                   'FC_BIAS_LIN': 8,
                   'KRUM_CURR_LIN': 9,
                   'LDAC_LIN': 10,
                   'PA_IN_BIAS_LIN': 11,
                   'COMP_DIFF': 12,
                   'PRECOMP_DIFF': 13,
                   'FOL_DIFF': 14,
                   'PRMP_DIFF': 15,
                   'LCC_DIFF': 16,
                   'VFF_DIFF': 17,
                   'VTH1_DIFF': 18,
                   'VTH2_DIFF': 19,
                   'CDR_CP_IBIAS': 20,
                   'VCO_BUFF_BIAS': 21,
                   'VCO_IBIAS': 22,
                   'CML_TAP_BIAS0': 23,
                   'CML_TAP_BIAS1': 24,
                   'CML_TAP_BIAS2': 25}

    column_range = {'SYNC': [0, 128],
                    'LIN': [128, 264],
                    'DIFF': [264, 400]}

    def __init__(self, bdaq, config=None):
        self.bdaq = bdaq
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        if config is None:
            logger.warning('No configuration supplied. Using default values!')
            self.configuration = {}
            self.configuration['general'] = {}
            self.configuration['calibration'] = {}
            self.configuration['registers'] = {}
        elif type(config) == analysis_utils.ConfigDict:
            self.configuration = config
        elif type(config) == str and os.path.isfile(config):
            with open(config) as f:
                self.configuration = yaml.load(f)
        elif type(config) == str and os.path.isfile(os.path.join(os.path.join(self.proj_dir, 'bdaq53' + os.sep + 'scans'), config)):
            with open(os.path.join(os.path.join(self.proj_dir, 'bdaq53' + os.sep + 'scans'), config)) as f:
                self.configuration = yaml.load(f)
        else:
            raise TypeError('Supplied config has unknown format!')

        if 'chip_id' in self.configuration['general']:
            if self.configuration['general']['chip_id']:
                self.chip_id = self.configuration['general']['chip_id']
            else:
                self.chip_id = 8
        else:
            self.chip_id = 8
        if self.chip_id not in range(9):
            raise ValueError("Invalid Chip ID %r, use integer 0-8" % self.chip_id)

        self.registers = RD53ARegisterObject(self)

        self.calibration = RD53ACalibration(self.configuration['calibration'])
        self.reset_masks()

    def init(self, **kwargs):
        if self.bdaq.board_version != 'SIMULATION':
            self.write_trimbits()   # Try to write trimbits for better communication
            self.init_communication(**kwargs)
            self.write_trimbits(verify=True)  # Make sure that trimbits are written
            self.reset()
        else:
            self.init_communication(**kwargs)

    def get_sn(self):
        return self.configuration['general']['chip_sn']

    def write_trimbits(self, verify=False):
        '''
        Write trimbits for all reference voltages to the chip, if they are supplied in the chip config.
        '''

        if 'trim' not in self.configuration:
            return

        vref_a_trim = self.configuration['trim'].get('VREF_A_TRIM', 16)
        vref_d_trim = self.configuration['trim'].get('VREF_D_TRIM', 16)
        self.registers['VOLTAGE_TRIM'].write(int('{0:05b}'.format(vref_a_trim) + '{0:05b}'.format(vref_d_trim), 2), verify=verify)

        mon_bg_trim = self.configuration['trim'].get('MON_BG_TRIM', 0)
        mon_adc_trim = self.configuration['trim'].get('MON_ADC_TRIM', 0)
        self.registers['MONITOR_CONFIG'].write(int('{0:05b}'.format(mon_bg_trim) + '{0:06b}'.format(mon_adc_trim), 2), verify=verify)

    def setup_aurora(self, tx_lanes=1, CB_Wait=255, CB_Send=1, only_cb=False, write=True, bypass_mode=False, **_):
        indata = self.write_sync(write=False) * 32
        if bypass_mode:
            logger.info("Switching chip to BYPASS MODE")
            self.registers['CDR_CONFIG'].set(int('0b'
                                                 + '1'  # CDR_SEL_DEL_CLK ***
                                                 + '00'  # CDR_PD_SEL[1:0]
                                                 + '0100'  # CDR_PD_DEL[3:0]
                                                 + '0'  # CDR_EN_GCK2
                                                 + '011'  # CDR_VCO_GAIN[2:0]
                                                 + '111',  # CDR_SEL_SER_CLK[2:0] ***
                                                 2))

        elif self.bdaq.board_options & self.bdaq.board_options_map['640Mbps']:
            logger.info("Chip is running at 640Mb/s")
            self.registers['CDR_CONFIG'].set(int('0b'
                                                 + '0'  # CDR_SEL_DEL_CLK
                                                   + '00'  # CDR_PD_SEL[1:0]
                                                   + '0100'  # CDR_PD_DEL[3:0]
                                                   + '0'  # CDR_EN_GCK2
                                                   + '011'  # CDR_VCO_GAIN[2:0]
                                                   + '001',  # CDR_SEL_SER_CLK[2:0]
                                                 2))
        else:
            logger.info("Chip is running at 1.28Gb/s")
        if only_cb is False:
            logger.debug("Aurora settings: Lanes: TX=%u, CB_Wait=%u, CB_Send=%u", tx_lanes, CB_Wait, CB_Send)
            if tx_lanes == 4:
                logger.info("4 Aurora lanes active")
                # Sets 4-lane-mode
                self.registers['OUTPUT_CONFIG'].set(0b00111100)
                # Enable 4 CML outputs
                self.registers['CML_CONFIG'].set(0b00001111)
            elif tx_lanes == 2:
                logger.info("2 Aurora lanes active")
                # Sets 2-lane-mode
                self.registers['OUTPUT_CONFIG'].set(0b00001100)
                # Enable 2 CML outputs
                self.registers['CML_CONFIG'].set(0b00000011)
            elif tx_lanes == 1:
                logger.info("1 Aurora lane active")
                # Sets 2-lane-mode
                self.registers['OUTPUT_CONFIG'].set(0b00000100)
                # Enable 2 CML outputs
                self.registers['CML_CONFIG'].set(0b00000001)
            else:
                logger.error("Aurora lane configuration (1,2,4) must be specified")
        else:
            logger.debug("Aurora settings: CB_Wait=%u, CB_Send=%u", CB_Wait, CB_Send)

        # Set CB frame distance and number
        self.registers['AURORA_CB_CONFIG0'].set(((CB_Wait << 4) | CB_Send & 0x0f) & 0xff)
        self.registers['AURORA_CB_CONFIG1'].set((CB_Wait & 0xfffff) >> 4)  # Set CB frame distance and number

        self.registers.write_all()

        # Reset Aurora and serializers
        self.send_global_pulse(bitnames=['reset_aurora', 'reset_serializer'], pulse_width=14)

    def init_communication(self, **kwargs):
        logger.info('Initializing communication...')

        # Configure cmd encoder
        self.bdaq['cmd'].reset()
        time.sleep(0.1)
        self.write_command(self.write_sync(write=False) * 32)
        # Wait for PLL lock
        self.bdaq.wait_for_pll_lock()

        self.setup_aurora(**kwargs)

        # Workaround for locking problems
        for _ in range(30):
            self.write_command(self.write_sync(write=False) * 32)
            self.write_ecr()
            try:
                self.bdaq.wait_for_aurora_sync()
            except RuntimeError:
                self.registers.write_all(force=True)
            else:
                break

            self.write_command([0x00] * 1000, repetitions=1)
            time.sleep(0.01)
        else:
            self.bdaq.wait_for_aurora_sync()

        logger.success('Communication established')

    def write_global_pulse(self, width, write=True):
        # 0101_1100    ChipId<3:0>,0    Width<3:0>,0
        indata = [self.CMD_GLOBAL_PULSE] * 2  # [0b01011100]
        chip_id_bits = self.chip_id << 1
        indata += [self.cmd_data_map[chip_id_bits]]
        width_bits = width << 1
        indata += [self.cmd_data_map[width_bits]]

        if write:
            self.write_command(indata)

        return indata

    def write_cal(self, cal_edge_mode=0, cal_edge_width=10, cal_edge_dly=2, cal_aux_value=0, cal_aux_dly=0, write=True):
        '''
            Command to send a digital or analog injection to the chip.
            Digital or analog injection is selected globally via the INJECTION_SELECT register.

            For digital injection, only CAL_edge signal is relevant:
                - CAL_edge_mode switches between step (0) and pulse (0) mode
                - CAL_edge_dly is counted in bunch crossings. It sets the delay before the rising edge of the signal
                - CAL_edge_width is the duration of the pulse (only in pulse mode) and is counted in cycles of the 160MHz clock
            For analog injection, the CAL_aux signal is used as well:
                - CAL_aux_value is the value of the CAL_aux signal
                - CAL_aux_dly is counted in cycles of the 160MHz clock and sets the delay before the edge of the signal

            {ChipId[3:0],CalEdgeMode, CalEdgeDelay[2:0],CalEdgeWidth[5:4]}{CalEdgeWidth[3:0],CalAuxMode, CalAuxDly[4:0]}
        '''
        indata = [self.CMD_CAL] * 2
        chip_id_bits = self.chip_id << 1
        indata += [self.cmd_data_map[(chip_id_bits + cal_edge_mode)]]
        cal_edge_dly_bits = BitLogic.from_value(0, size=3)
        cal_edge_dly_bits[:] = cal_edge_dly
        cal_edge_width_bits = BitLogic.from_value(0, size=6)
        cal_edge_width_bits[:] = cal_edge_width
        cal_aux_dly_bits = BitLogic.from_value(0, size=5)
        cal_aux_dly_bits[:] = cal_aux_dly

        indata += [self.cmd_data_map[(cal_edge_dly_bits[2:0].tovalue() << 2) + cal_edge_width_bits[5:4].tovalue()]]
        indata += [self.cmd_data_map[(cal_edge_width_bits[3:0].tovalue() << 1) + cal_aux_value]]
        indata += [self.cmd_data_map[cal_aux_dly_bits[4:0].tovalue()]]

        if write:
            self.write_command(indata)

        return indata

    def _read_register(self, address, write=True):
        indata = [self.CMD_RDREG] * 2
        chip_id_bits = self.chip_id << 1
        indata += [self.cmd_data_map[chip_id_bits]]
        addr_bits = BitLogic.from_value(0, size=9 + 6)
        addr_bits[14:6] = address
        indata += [self.cmd_data_map[addr_bits[14:10].tovalue()]]
        indata += [self.cmd_data_map[addr_bits[9:5].tovalue()]]
        indata += [self.cmd_data_map[addr_bits[4:0].tovalue()]]
        if write:
            self.write_command(indata)
        return indata

    def _get_register_value(self, address, timeout=1000):
        self.enable_monitor_data()
        self.send_global_pulse('reset_monitor_data', pulse_width=4)
        self.bdaq['FIFO']['RESET']
        if self.bdaq.board_version == 'SIMULATION':
            self.write_command(self.write_sync_01(write=False) * 1000)
        self._read_register(address)

        for _ in range(timeout):
            if self.bdaq['FIFO'].get_FIFO_SIZE() > 0:
                data = self.bdaq['FIFO'].get_data()
                userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(data))
                if len(userk_data) == 0:
                    raise RuntimeError('Received invalid user_k data!')
                return userk_data['Data'][0]
            self.write_command(self.write_sync_01(write=False) * 10)
        else:
            raise RuntimeError('Timeout while waiting for register response.')

    def _write_register(self, address, data, write=True):
        '''
            Sends write command to register with data

            Parameters:
            ----------
                address : int
                    Address of the register to be written to

            Returns:
            ----------
                indata : binarray
                    Boolean representation of register write command.
        '''
        if np.issubdtype(type(data), np.signedinteger):
            bits = 9 + 16
            data_to_send = data | (address << 16)
            bits_to_send = "{0:025b}".format(data_to_send)
            wr_reg_mode = 0
        else:
            if(len(data) != 6):
                raise ValueError('Error while writing data %s to register %s:\n Command data size was not 1 or 6. It was: %s' % (data, register, len(data)))

            wr_reg_mode = 1
            bits = 9 + len(data) * 16

            data_to_send = (address << 6 * 16) | (data[0] << 5 * 16) | (data[1] << 4 * 16) | (data[2] << 3 * 16) | (data[3] << 2 * 16) | (data[4] << 1 * 16) | data[5]
            bits_to_send = "{0:0105b}".format(data_to_send)

        indata = [self.CMD_REGISTER] * 2
        chip_id_bits = self.chip_id << 1
        header = chip_id_bits + wr_reg_mode
        indata += [self.cmd_data_map[header]]

        for b in range(bits // 5):
            v = '0b' + bits_to_send[b * 5:b * 5 + 5]
            indata += [self.cmd_data_map[int(v, 2)]]

        if write:
            self.write_command(indata)

        return indata

    def write_null(self, write=True):
        indata = [self.CMD_NULL] * 2  # [0b01101001]
        if write:
            self.write_command(indata)
        return indata

    def write_ecr(self, write=True):
        indata = [self.CMD_ECR] * 2  # [0b01011010]
        if write:
            self.write_command(indata)
        return indata

    def write_bcr(self, write=True):
        indata = [self.CMD_BCR] * 2  # [0b01011001]
        if write:
            self.write_command(indata)
        return indata

    def write_sync(self, write=True):
        indata = [self.CMD_SYNCH]
        indata += [self.CMD_SYNCL]
        if write:
            self.write_command(indata)
        return indata

    def write_sync_01(self, write=True):
        indata = [0b01010101]
        indata += [0b01010101]
        if write:
            self.write_command(indata)
        return indata

    def send_trigger(self, trigger, tag=0, write=True):
        # Trigger is always followed by 5 Data bits
        indata = [self.trigger_map[trigger]]
        indata += [self.cmd_data_map[tag]]
        if write:
            self.write_command(indata)
        return indata

    def send_trigger_tag(self, trigger, trigger_tag, write=True):
        if trigger == 0:
            logger.error("Illegal trigger number")
            return
        else:
            indata = [self.trigger_map[trigger]]
            indata += [trigger_tag]
            if write:
                self.write_command(indata)
            return indata

    def write_command(self, data, repetitions=1, wait_for_done=True):
        '''
            Write data to the command encoder.

            Parameters:
            ----------
                data : list
                    Up to [get_cmd_size()] bytes
                repetitions : integer
                    Sets repetitions of the current request. 1...2^16-1. Default value = 1.
                wait_for_done : boolean
                    Wait for completion after sending the command. Not advisable in case of repetition mode.
        '''

        if isinstance(data[0], list):
            for indata in data:
                self.write_command(indata, repetitions, wait_for_done)
            return

        assert (0 < repetitions < 65536), "Repetition value must be 0<n<2^16"
        if repetitions > 1:
            logger.debug("Repeating command %i times." % (repetitions))

        self.bdaq['cmd'].set_data(data)
        self.bdaq['cmd'].set_size(len(data))
        self.bdaq['cmd'].set_repetitions(repetitions)
        self.bdaq['cmd'].start()

        if wait_for_done:
            while (not self.bdaq['cmd'].is_done()):
                pass

    def send_global_pulse(self, bitnames, pulse_width, global_pulse_route_file=None):
        '''
            Used to set a single or multiple bits of GLOBAL_PULSE_ROUTE register by name, using names defined in global_pulse_route.yaml

            Parameters:
            ----------
                bitnames : str or list of str
                    Name(s) of the bit(s) to be set to 1
        '''

        if not global_pulse_route_file:
            global_pulse_route_file = os.path.join(self.proj_dir, 'bdaq53' + os.sep + 'global_pulse_route.yaml')

        with open(global_pulse_route_file, 'r') as infile:
            global_pulse_route_map = yaml.load(infile)

        if type(bitnames) == str:
            bitnames = [bitnames]

        listofnames = [bit['name'] for bit in global_pulse_route_map]

        for bitname in bitnames:
            if bitname not in listofnames:
                logger.warning('Could not find %s in possible values for register GLOBAL_PULSE_ROUTE' % bitname)

        val = ''
        for i in range(16):
            if global_pulse_route_map[i]['name'] in bitnames:
                val += '1'
            else:
                val += '0'

        val = '0b' + val[::-1]

        self.registers['GLOBAL_PULSE_ROUTE'].write(int(val, 2))
        self.write_global_pulse(width=pulse_width)

    def reset(self):
        if self.bdaq.board_version == 'SIMULATION':
            return

        self.registers.reset_all()

        self.reset_masks()
        self.write_masks()

        self.send_global_pulse('sync_fe_autozeroing', pulse_width=14)    # FIXME: Do we need this?

        self.enable_core_col_clock(range(50))  # Enable clock on full chip
        self.enable_macro_col_cal(range(200))  # Enable analog calibration on full chip

        self.set_dacs()

    def reset_masks(self, enable=True, injection=True, hitbus=True, tdac=True, lin_gain_sel=True):
        '''
            Resets all masks to default (False / 0)
        '''

        if enable:
            self.enable_mask = np.zeros((400, 192), dtype=bool)
        if injection:
            self.injection_mask = np.zeros((400, 192), dtype=bool)
        if hitbus:
            self.hitbus_mask = np.zeros((400, 192), dtype=bool)
        if tdac:
            self.tdac_mask = np.zeros((400, 192), dtype=int)
        if lin_gain_sel:
            self.lin_gain_sel_mask = np.ones((400, 192), dtype=int)

    def apply_good_pixel_mask_diff(self):
        '''
        This function disables pixels with too high parasitic capacitance on the comparator output
        in the differntial front end. The matrix represents one digital core
        with 1 indicating enabled pixels and 0 indicating disabled pixels.
        '''
        logger.info('Applying good pixel mask to Differential Front-End.')
        good_pixel_cell = np.transpose([[0, 0, 1, 1, 0, 0, 0, 0],
                                        [0, 0, 0, 1, 0, 0, 0, 0],
                                        [0, 0, 0, 1, 0, 0, 0, 0],
                                        [0, 0, 0, 1, 1, 0, 0, 0],
                                        [0, 0, 0, 1, 1, 0, 0, 0],
                                        [0, 0, 0, 1, 1, 0, 0, 0],
                                        [0, 0, 0, 1, 0, 0, 0, 0],
                                        [0, 0, 0, 0, 1, 0, 0, 0]])
        good_pixel_mask = np.tile(good_pixel_cell, (17, 24))
        self.enable_mask[264:, :] = self.enable_mask[264:, :] & good_pixel_mask

    def inject_digital(self, cal_edge_width=10, cal_edge_dly=2, fine_delay=9, latency=121, repetitions=1):
        '''
            Injects a digital pulse in all enabled pixels

            ----------
            Parameters:
                latency : int
                    Number of write_sync commands between injection and trigger, accepts values [117:124]
                repetitions : int
                    Number of times, the injection command is repeated, i.e. number of injections
        '''

        self.registers['INJECTION_SELECT'].write(int('0b01{0:b}'.format(fine_delay), 2))  # Enable digital injection with zero delay

        indata = self.write_sync_01(write=False) * 10
        indata += self.write_cal(cal_edge_width=cal_edge_width, cal_edge_dly=cal_edge_dly, cal_edge_mode=1, write=False)  # Injection
        indata += self.write_sync_01(write=False) * latency  # Wait for latency
        indata += self.send_trigger(trigger=0b1111, write=False) * 8  # Trigger
        if self.bdaq.board_version != 'SIMULATION':
            indata += self.write_sync_01(write=False) * 800  # Wait for data
        self.write_command(indata, repetitions=repetitions)

    def setup_analog_injection(self, vcal_high, vcal_med, fine_delay=9):
        self.registers['INJECTION_SELECT'].write(int('0b00{0:b}'.format(fine_delay), 2))  # Enable analog injection in uniform mode with configured fine delay
        self.registers['VCAL_HIGH'].write(vcal_high)  # Set VCAL_HIGH
        self.registers['VCAL_MED'].write(vcal_med)  # Set VCAL_MED
        self.registers['GLOBAL_PULSE_ROUTE'].write(0b0100000000000000)  # With every injection reset command decoder to avoid SYNC getting stuck
        self.write_cal(cal_edge_mode=1, cal_edge_width=0, cal_edge_dly=0)  # CalEdge -> 0

    def inject_analog_single(self, repetitions=1, latency=122, wait_cycles=400, send_ecr=False, write=True):
        '''
            Injects a single analog pulse in all enabled pixels

            ----------
            Parameters:
                latency : int
                    Number of write_sync commands between injection and trigger
        '''

        indata = self.write_sync_01(write=False)

        if send_ecr:
            indata += self.write_global_pulse(width=8, write=False)
            # Send ECR always twice. This helps reducing the number of data errors introduced by ECR.
            indata += self.write_ecr(write=False)
            indata += self.write_ecr(write=False)
            indata += self.write_sync_01(write=False) * 60

        indata += self.write_cal(cal_edge_mode=0, cal_edge_width=1, cal_edge_dly=0, write=False)  # CalEdge -> 1 (inject)
        indata += self.write_sync_01(write=False) * latency  # Wait for latency
        indata += self.send_trigger(trigger=0b1111, write=False) * 8  # Trigger
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=0, cal_edge_dly=0, write=False)  # CalEdge -> 0

        if self.bdaq.board_version != 'SIMULATION':
            indata += self.write_sync_01(write=False) * wait_cycles  # Wait for data

        if write:
            self.write_command(indata, repetitions=repetitions)

        return indata

    def toggle_output_select(self, fine_delay=9, wait_cycles=400, latency=121, repetitions=1):
        '''
            Toggles between digital and analog injection. If the comparator is stuck high
            this creates a transient on hit_t and thus a hit.

            Note
            ----
            Chip implementation:
            assign hit_t = EnDigHit ? (CalEdge & cal_en) : hit ;
            assign HitOut = hit_t & hit_en;

            See also Figure 29 in RD53 manual.

            Parameters:
            ----------
                latency : int
                    Number of write_sync commands between injection and trigger, accepts values [117:124]
        '''

        # Enable digital injection = falling edge for stuck high pixels
        indata = self.registers['INJECTION_SELECT'].get_write_command(int('0b01' + str(bin(fine_delay))[2:], 2))
        indata += self.write_sync_01(write=False) * 10
        # Enable analog injection, this creates a rising edge if comparator output is stuck high
        indata += self.registers['INJECTION_SELECT'].get_write_command(int('0b00' + str(bin(fine_delay))[2:], 2))
        indata += self.write_sync_01(write=False) * latency  # Wait for latency
        indata += self.send_trigger(trigger=0b1111, write=False) * 8  # Trigger
        indata += self.write_sync_01(write=False) * wait_cycles
        self.write_command(indata, repetitions=repetitions)

    def enable_core_col_clock(self, core_cols=None, write=True):
        '''
            Enable clocking of given core columns. After POR everything is disabled.

            ----------
            Parameters:
                core_cols : list of int
                    A list of core columns to enable. Default = None disables everything
        '''

        # Disable everything
        indata = self.write_sync_01(write=False)
        indata += self.registers['EN_CORE_COL_SYNC'].get_write_command(0)
        indata += self.registers['EN_CORE_COL_LIN_1'].get_write_command(0)
        indata += self.registers['EN_CORE_COL_LIN_2'].get_write_command(0)
        indata += self.registers['EN_CORE_COL_DIFF_1'].get_write_command(0)
        indata += self.registers['EN_CORE_COL_DIFF_2'].get_write_command(0)

        if core_cols:
            indata += self.write_sync_01(write=False)
            for r in self.core_regs:
                bits = []
                for i, core_col in enumerate(r['core_columns']):
                    if core_col in core_cols:
                        bits.append(i)

                if not bits:
                    continue
                bits = list(set(bits))  # Remove duplicates

                data = ''
                for i in range(len(r['core_columns'])):
                    data += '1' if i in bits else '0'

                indata += self.registers[r['core_name']].get_write_command(int(data[::-1], 2))

        if write:
            self.write_command(indata)

        return indata

    def enable_macro_col_cal(self, macro_cols=None):
        '''
            Enable analog calibration of given macro (double-) columns. After POR everything is enabled.

            ----------
            Parameters:
                macro_cols : list of int
                    A list of macro columns to enable. Default = None disables everything
        '''

        # Disable everything
        self.registers['EN_MACRO_COL_CAL_SYNC_1'].set(0)
        self.registers['EN_MACRO_COL_CAL_SYNC_2'].set(0)
        self.registers['EN_MACRO_COL_CAL_SYNC_3'].set(0)
        self.registers['EN_MACRO_COL_CAL_SYNC_4'].set(0)

        self.registers['EN_MACRO_COL_CAL_LIN_1'].set(0)
        self.registers['EN_MACRO_COL_CAL_LIN_2'].set(0)
        self.registers['EN_MACRO_COL_CAL_LIN_3'].set(0)
        self.registers['EN_MACRO_COL_CAL_LIN_4'].set(0)
        self.registers['EN_MACRO_COL_CAL_LIN_5'].set(0)

        self.registers['EN_MACRO_COL_CAL_DIFF_1'].set(0)
        self.registers['EN_MACRO_COL_CAL_DIFF_2'].set(0)
        self.registers['EN_MACRO_COL_CAL_DIFF_3'].set(0)
        self.registers['EN_MACRO_COL_CAL_DIFF_4'].set(0)
        self.registers['EN_MACRO_COL_CAL_DIFF_5'].set(0)

        if not macro_cols:
            return

        # Enable given columns
        for r in self.macro_regs:
            bits = []
            for i, macro_col in enumerate(r['macro_columns']):
                if macro_col in macro_cols:
                    bits.append(i)

            if not bits:
                continue
            bits = list(set(bits))  # Remove duplicates

            data = ''
            for i in range(len(r['macro_columns'])):
                data += '1' if i in bits else '0'

            self.registers[r['macro_name']].set(int(data[::-1], 2))
        self.registers.write_all()

    def get_pixel_config(self, column, row):
        enable = str(int(self.enable_mask[column, row]))
        injection_enable = str(int(self.injection_mask[column, row]))
        hitbus_enable = str(int(self.hitbus_mask[column, row]))

        if column >= 128 and column < 264:
            if self.tdac_mask[column, row] < 0:
                self.tdac_mask[column, row] = 0
            tdac = str(bin(self.lin_gain_sel_mask[column, row]))[2:] + str(bin(self.tdac_mask[column, row]))[2:].zfill(4)
        elif column >= 264:
            if self.tdac_mask[column, row] >= 0:
                tdac = str(bin(self.tdac_mask[column, row]))[2:].zfill(5)
            else:
                tdac = '1' + str(bin(abs(self.tdac_mask[column, row])))[2:].zfill(4)
        else:
            tdac = '00000'

        return tdac + hitbus_enable + injection_enable + enable

    def write_double_pixel(self, double_column, row):  # double column?

        indata = self.registers['REGION_ROW'].get_write_command(row)
        indata += self.registers['REGION_COL'].get_write_command(double_column)

        column = double_column * 2

        pixel_data = '0b' + self.get_pixel_config(column + 1, row) + self.get_pixel_config(column, row)
        indata += self.registers['PIX_PORTAL'].get_write_command(int(pixel_data, 2))

        return indata

    def write_masks(self, columns=range(400), write=True):
        '''
            Apply global masks to selected columns.
            In principle, you can always write to all columns, since the information is saved in the mask, but this is very slow.
            To speed things up, you should only write the masks to the necessary columns.

            ----------
            Parameters:
                columns : list of int
                    A list of the columns to write the masks to
        '''

        # TODO: Possible speedup using broadcast mode?

        logger.debug('Writing masks...')
        self.registers['PIX_MODE'].write(0b0000000000001000)    # Disable broadcast, enable auto-row
        self.registers['PIX_DEFAULT_CONFIG'].write(0)  # Disable default config

        out_data = []

        for column in columns:
            pixel_index = 0 if column % 2 == 0 else 1

            if pixel_index == 0:
                double_column = True if column + 1 in columns else False
            else:
                double_column = False
                if column - 1 in columns:
                    continue

            pair_index = 0 if column % 4 < 2 else 1
            region_in_core_column = 0 if column % 8 < 4 else 1
            core_column = int(column / 8)

            # 6'b core_column + 1'b region_in_core_column + 1'b pair_index
            column_data = '0b' + str(bin(core_column))[2:].zfill(6) + str(region_in_core_column) + str(pair_index)

            indata = self.registers['REGION_ROW'].get_write_command(0)
            indata += self.registers['REGION_COL'].get_write_command(int(column_data, 2))

            mask = []
            for row in range(192):
                enable = str(int(self.enable_mask[column, row]))
                injection_enable = str(int(self.injection_mask[column, row]))
                hitbus_enable = str(int(self.hitbus_mask[column, row]))
                if column in range(128, 264):
                    if self.tdac_mask[column, row] < 0:
                        self.tdac_mask[column, row] = 0
                    tdac = str(bin(self.lin_gain_sel_mask[column, row]))[2:] + str(bin(self.tdac_mask[column, row]))[2:].zfill(4)
                elif column in range(264, 400):
                    if self.tdac_mask[column, row] >= 0:
                        tdac = str(bin(self.tdac_mask[column, row]))[2:].zfill(5)
                    else:
                        tdac = '1' + str(bin(abs(self.tdac_mask[column, row])))[2:].zfill(4)
                else:
                    if self.tdac_mask[column, row] < 0:
                        self.tdac_mask[column, row] = 0
                    tdac = str(bin(self.tdac_mask[column, row]))[2:].zfill(5)

                pixel_data = '0b'
                if double_column:
                    dc_enable = str(int(self.enable_mask[column + 1, row]))
                    dc_injection_enable = str(int(self.injection_mask[column + 1, row]))
                    dc_hitbus_enable = str(int(self.hitbus_mask[column + 1, row]))
                    if column in range(128, 264):
                        if self.tdac_mask[column + 1, row] < 0:
                            self.tdac_mask[column + 1, row] = 0
                        dc_tdac = str(bin(self.lin_gain_sel_mask[column + 1, row]))[2:] + str(bin(self.tdac_mask[column + 1, row]))[2:].zfill(4)
                    elif column in range(264, 400):
                        if self.tdac_mask[column + 1, row] >= 0:
                            dc_tdac = str(bin(self.tdac_mask[column + 1, row]))[2:].zfill(5)
                        else:
                            dc_tdac = '1' + str(bin(abs(self.tdac_mask[column + 1, row])))[2:].zfill(4)
                    else:
                        if self.tdac_mask[column + 1, row] < 0:
                            self.tdac_mask[column + 1, row] = 0
                        dc_tdac = str(bin(self.tdac_mask[column + 1, row]))[2:].zfill(5)
                    pixel_data += dc_tdac + dc_hitbus_enable + dc_injection_enable + dc_enable + tdac + hitbus_enable + injection_enable + enable
                else:
                    if pixel_index == 0:
                        pixel_data += '00000000' + tdac + hitbus_enable + injection_enable + enable
                    else:
                        pixel_data += tdac + hitbus_enable + injection_enable + enable + '00000000'

                mask.append(int(pixel_data, 2))

                if row % 6 == 5:
                    indata += self.registers['PIX_PORTAL'].get_write_command(mask)
                    mask = []

            if len(out_data) != 0 and (len(out_data[-1]) + len(indata)) < self.bdaq['cmd'].get_mem_size():
                out_data[-1] = out_data[-1] + indata
            else:
                out_data.append(indata)

            indata = []

        if write:
            for indata in out_data:
                self.write_command(indata)

        return out_data

    def enable_monitor_data(self):
        self.bdaq.set_monitor_filter(True)
        self.send_global_pulse('enable_monitoring', 4)
        logger.debug('Monitor data enabled')

    def get_ADC_value(self, name):
        '''
        Sends multiplexer settings and reads ADC measurement
        Output is in ADC LSB.

        Parameters:
        ----------
            address: str or int
                The name of the ADC register to be measured

        Returns:
        ----------
            ADC value
        '''
        mux_type = None

        if name in self.current_mux.keys():
            mux_type = 'current'
            address = self.current_mux[name]
            bitstring = int('1' + format(address, '06b') + format(11, '07b'), 2)
        elif name in self.voltage_mux.keys():
            mux_type = 'voltage'
            address = self.voltage_mux[name]
            bitstring = int('1' + format(32, '06b') + format(address, '07b'), 2)
        else:
            raise ValueError('Invalid address: %s (%s)' % (address, type(address)))

        self.send_global_pulse(bitnames='reset_monitor_data', pulse_width=4)  # Reset Monitor Data
        self.send_global_pulse(bitnames='reset_adc', pulse_width=4)  # Reset ADC
        self.registers['MONITOR_SELECT'].write(bitstring)  # Select MUX
        self.send_global_pulse(bitnames='adc_start_conversion', pulse_width=4)  # Start ADC

        val_adc = self.registers['MonitoringDataADC'].read()    # Read out ADC value
        if mux_type is 'voltage':
            val_real = self.calibration.get_V_from_ADC(val_adc)
        elif mux_type is 'current':
            # FIXME: To be calibrated
            val_real = None
        return val_adc, val_real

    def get_chip_status(self, timeout=10000):
        '''
            Returns a map of all important chip parameters.
            Can only be called before or after a scan!
        '''

        voltages = {}
        currents = {}

        logger.info('Recording chip status...')
        try:
            for vmonitor in self.voltage_mux.keys():
                voltages[vmonitor] = self.get_ADC_value(vmonitor)

            for cmonitor in self.current_mux.keys():
                currents[cmonitor] = self.get_ADC_value(cmonitor)
        except RuntimeError as e:
            logger.error('There was an error while receiving the chip status: %s' % e)

        return voltages, currents

    def get_ring_oscillators(self, pulse_width=8):
        oscillators = {}
        for i in range(8):
            self.write_command(self.write_sync(write=False) * 300)
            self.registers['RING_OSC_ENABLE'].write(0b11111111)
            self.registers['RING_OSC_' + str(i)].write(0b0)
            self.send_global_pulse('start_ringosc', pulse_width=pulse_width)
            self.write_command(self.write_sync(write=False) * 40)

            oscillators['RING_OSC_' + str(i)] = self.registers['RING_OSC_' + str(i)].read()

        return oscillators

    def get_temperature_sensors(self, samples=1, diode_current=15):
        sensors = ['TEMPSENS_1', 'TEMPSENS_2', 'TEMPSENS_3', 'TEMPSENS_4']

        temp, adc_delta = {}, {}
        try:
            for sensor in sensors:
                temp[sensor] = {}
                lower, upper = [], []

                for reps in range(samples):
                    bitstring = int('1' + format(diode_current, '04b') + '01' + format(diode_current, '04b') + '0', 2)
                    self.registers['SENSOR_CONFIG_0'].write(bitstring)
                    self.registers['SENSOR_CONFIG_1'].write(bitstring)
                    self.write_sync_01()
                    lower.append(self.get_ADC_value(address=sensor))

                    bitstring = int('1' + format(diode_current, '04b') + '11' + format(diode_current, '04b') + '1', 2)
                    self.registers['SENSOR_CONFIG_0'].write(bitstring)
                    self.registers['SENSOR_CONFIG_1'].write(bitstring)
                    self.write_sync_01()
                    upper.append(self.get_ADC_value(address=sensor))

                temp[sensor]['dADC'] = int(np.mean(upper) - np.mean(lower))
                temp[sensor]['temp'] = self.calibration.get_temperature_from_ADC(temp[sensor]['dADC'])

            logger.info('Mean chip temperature is (%i +- 4)°C' % np.mean([val['temp'] for val in temp.values()]))
        except RuntimeError as e:
            logger.error('There was an error reading temperature sensors: %s' % e)
        return temp

    def set_dacs(self):
        self.dacs = {}
        for key, value in self.configuration['registers'].items():
            self.registers[key].set(value)
            self.dacs[key] = value
        self.registers.write_all()

    def set_tdac(self, **kwargs):
        TDAC = kwargs.get('TDAC')
        if TDAC is None:  # Set LIN to 7 and DIFF to 0 by default
            TDAC = np.zeros((400, 192), dtype=np.int8)
            TDAC[128:264, :] = 7  # LIN

        mask = np.zeros((400, 192), dtype=np.int8)
        mask[:, :] = TDAC

        try:
            maskfile = kwargs.get('maskfile')
            with tb.open_file(maskfile, 'r') as infile:
                logger.info('Loading TDAC mask from file: %s' % (maskfile))
                mask = infile.root.TDAC_mask[:]
        except (TypeError, NoSuchNodeError):
            pass

        self.tdac_mask[:, :] = mask

    def az_setup(self, delay, repeat, width=6, synch=1, **kwargs):
        '''
            Auto-zeroing configuration for the synchronous front-end

            Parameters:
            ----------
                dalay: AZ command distance in [us]
                width: global pulse width (default: 6)
                repeat: How many times. 0 = infinite
                synch >= 0  for width<6  (the = value is the minimum amount of sync needed to correctly mask the HitOr. With more it still works but with higher dead-time
                synch >= 1  for width=6
                      >= 5  for width=7
                      >= 13 for width=8
                      >= 29 for width=9
        '''
        self.bdaq['pulse_gen_az'].reset()
        if self.bdaq['pulse_gen_az'].is_done():
            logger.info('Setting auto-zeroing period to %s us' % delay)
            self.bdaq['pulse_gen_az'].set_delay(delay * 160)
            self.bdaq['pulse_gen_az'].set_width(1)
            self.bdaq['pulse_gen_az'].set_repeat(repeat)
        else:
            while not self.self.bdaq['pulse_gen_az'].is_done():
                logger.warning('Auto-zeroing pluse generator is busy. Waiting...')
                time.sleep(0.1)

        indata = self.registers['Vthreshold_LIN'].get_write_command(1023)  # Workarount for AZ pulse injecting noise into LIN

        indata += self.registers['EN_CORE_COL_SYNC'].get_write_command(0)
        indata += self.send_global_pulse('sync_fe_autozeroing', pulse_width=width, write=False)  # Send AZ, pulse width should be ~400 ns (2^n * 6.25 ns)
        indata += self.write_sync_01(write=False) * synch
        indata += self.registers['EN_CORE_COL_SYNC'].get_write_command(0xffff)

        indata += self.registers['Vthreshold_LIN'].get_write_command(kwargs['Vthreshold_LIN'])  # Workarount for AZ pulse injecting noise into LIN
        indata += self.write_ecr(write=False)

        self.write_command(indata)

        self.self.bdaq['cmd'].set_az_veto_cycles(len(indata) * 8)

    def az_start(self):
        ''' Software-controlled start '''
        logger.debug('Starting auto-zeroing')
        self.bdaq['pulse_gen_az'].start()

    def az_stop(self):
        ''' Software-controlled stop '''
        logger.debug('Stoping auto-zeroing')
        self.bdaq['pulse_gen_az'].reset()


if __name__ == '__main__':
    rd53a_chip = RD53A()
    rd53a_chip.init()
