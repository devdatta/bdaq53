# RD53A_DAQ

DAQ and verification module for [RD53A](https://gitlab.cern.ch/rd53/RD53A) prototype based on [Basil](https://github.com/SiLab-Bonn/basil) framework.

