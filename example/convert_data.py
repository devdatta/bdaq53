#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This simple script is an example on how to export a node from an interpredet h5 file as csv.
'''

import csv
import os

import tables as tb


analyzed_data_file = '_interpreted.h5'
node_name = 'HistOcc'


with tb.open_file(analyzed_data_file, 'r') as infile:
    data = infile.get_node('/' + node_name)[:].T

outpath = os.path.join(os.path.dirname(analyzed_data_file), node_name + '.csv')
with open(outpath, 'w') as outfile:
    if len(data.shape) == 1:
        csv.writer(outfile).writerow(data)
    else:
        csv.writer(outfile).writerows(data)

print('Dumped node \'%s\' to %s' % (node_name, outpath))
