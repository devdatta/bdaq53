#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This minimal example script shows how to manipulate RD53A registers and read analog data with the internal ADC.
'''

from bdaq53.bdaq53 import BDAQ53
from bdaq53.rd53a import RD53A

# Init BDAQ anc chip objects
bdaq = BDAQ53()
bdaq.init()

# Read NTC temperature with KC705 and CERN FMC card
try:
    print(bdaq.get_temperature_NTC())
except NotImplementedError:
    pass

chip = RD53A(bdaq)
chip.init()

# Read a register
print('VCAL_HIGH = ', chip.registers['VCAL_HIGH'].read())

# Write a register
chip.registers['VCAL_HIGH'].write(value=1000)
print('VCAL_HIGH = ', chip.registers['VCAL_HIGH'].read())

# Read a value from internal ADC
print('VREF_A = {0}V'.format(chip.get_ADC_value('VREF_Ana_SLDO')[1]))

# Read all ring oscillators
print(chip.get_ring_oscillators())

# Read all possible ADC values
print(chip.get_chip_status())
