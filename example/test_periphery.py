#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Test if your periphery (e.g. SCC powersupply) is set up correctly
'''

import os
import time
import logging
import coloredlogs

import basil

from bdaq53.periphery import BDAQ53Periphery


settings = {
    'VINA': 1.7,
    'VIND': 1.7,
    
    'VINA_cur_lim': 0.7,
    'VIND_cur_lim': 0.7,
    
    'sensor_bias'  : -0
    }

loglevel = logging.INFO

''' Set up main logger '''
for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)
logging.getLogger('basil.HL.RegisterHardwareLayer').setLevel(logging.WARNING)

logging.SUCCESS = 25  # WARNING(30) > SUCCESS(25) > INFO(20)
logging.addLevelName(logging.SUCCESS, 'SUCCESS')

coloredlogs.DEFAULT_FIELD_STYLES = {'asctime': {},
                                    'hostname': {},
                                    'levelname': {'bold': True},
                                    'name': {},
                                    'programname': {}}
coloredlogs.DEFAULT_LEVEL_STYLES = {'critical': {'color': 'red', 'bold': True},
                                    'debug': {'color': 'magenta'},
                                    'error': {'color': 'red', 'bold': True},
                                    'info': {},
                                    'success': {'color': 'green'},
                                    'warning': {'color': 'yellow'}}

coloredlogs.install(fmt="%(asctime)s - [%(name)-15s] - %(levelname)-7s %(message)s", milliseconds=True)

logger = logging.getLogger('RD53A')
logger.setLevel(loglevel)
logger.success = lambda msg, *args, **kwargs: logger.log(logging.SUCCESS, msg, *args, **kwargs)

if __name__ == '__main__':
    periphery = BDAQ53Periphery()
    periphery.init()
    
    periphery.power_off_sensor_bias()
    time.sleep(1)
    periphery.power_off_SCC()
    time.sleep(1)
    periphery.power_off_BDAQ()
    
    time.sleep(5)
    
    periphery.power_on_BDAQ()
    time.sleep(1)
    periphery.power_on_SCC(**settings)
    time.sleep(5)
    periphery.power_on_sensor_bias(**settings)
