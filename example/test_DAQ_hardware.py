# ------------------------------------------------------------
# BDAQ53: Simple hardware test
# Basic DAQ hardware and chip configuration
#
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import os
import time
import numpy as np
import logging
import yaml
from basil.dut import Dut
from bdaq53.bdaq53 import BDAQ53
from bdaq53.rd53a import RD53A

activelanes = 1


# Initialization
with open('../bdaq53/bdaq53.yaml', 'r') as f:
    cnfg = yaml.load(f)

bdaq = BDAQ53(cnfg)
bdaq.init()

chip = RD53A(bdaq)
chip.init(tx_lanes=activelanes)

# Testing
# TODO

# Closing
bdaq.close()
