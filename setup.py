#!/usr/bin/env python

from setuptools import setup
from setuptools import find_packages

import bdaq53

version = bdaq53.__version__

author = 'Michael Daas, Yannick Dieter, Tomasz Hemperek, David-Leon Pohl, Mark Standke, Marco Vogt'
author_email = ''

# Requirements
install_requires = ['basil-daq>=3.0.0<4', 'bitarray>=0.8.1', 'matplotlib>=2.1.2',
                    'numpy', 'online_monitor>=0.4.0<0.5',
                    'pixel_clusterizer>=3.1.3<4', 'tables', 'pyyaml', 'pyzmq',
                    'scipy', 'numba', 'tqdm', 'pyserial', 'slackclient', 'gitpython',
                    'pexpect', 'coloredlogs']

setup(
    name='bdaq53',
    version=version,
    description='DAQ for RD53A prototype',
    url='https://gitlab.cern.ch/silab/bdaq53',
    license='',
    long_description='',
    author=author,
    maintainer=author,
    author_email=author_email,
    maintainer_email=author_email,
    install_requires=install_requires,
    python_requires=">=3.0",
    setup_requires=['online_monitor>=0.4.0<0.5'],
    packages=find_packages(),
    include_package_data=True,
    platforms='any',
    entry_points={
        'console_scripts': [
            'bdaq53 = bdaq53.bdaq53_cli:main',
            'bdaq53_monitor = bdaq53.analysis.online_monitor.start_bdaq53_monitor:main',
            'bdaq53_eudaq = bdaq53.scans.scan_eudaq:main'
        ]
    },
)

# FIXME: bad practice to put code into setup.py
# Add the online_monitor bdaq53 plugins
try:
    import os
    from online_monitor.utils import settings
    # Get the absoulte path of this package
    package_path = os.path.dirname(bdaq53.__file__)
    # Add online_monitor plugin folder to entity search paths
    settings.add_producer_sim_path(os.path.join(package_path,
                                                'analysis',
                                                'online_monitor'))
    settings.add_converter_path(os.path.join(package_path,
                                             'analysis',
                                             'online_monitor'))
    settings.add_receiver_path(os.path.join(package_path,
                                            'analysis',
                                            'online_monitor'))
except ImportError:
    pass
